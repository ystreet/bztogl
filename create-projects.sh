#!/bin/sh

GL_SERVER="https://gitlab.freedesktop.org/"
GL_TOKEN="FDO Gitlab token"

for comp in `cat gl_components`;
do
  python3 create-project.py http://127.0.0.1:1080 $GL_TOKEN gstreamer $comp
done
