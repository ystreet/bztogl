#!/usr/bin/env python

import sys
import gitlab
import time

def main(server, token, group, repo):
    gl = gitlab.Gitlab(server, private_token=token)
    try:
        proj = gl.projects.get(group + '/' + repo)
        proj.delete()
    except gitlab.exceptions.GitlabGetError as e:
        if e.response_code not in (404,):
            raise
    time.sleep(2)
    group_id = gl.groups.list(search=group)[0].id
    while True:
        try:
            gl.projects.create({'name': repo, 'namespace_id': group_id})
        except gitlab.exceptions.GitlabHttpError as a:
            if e.response_code in (400,):
                time.sleep (2)
                continue
            raise
        break

if __name__ == "__main__":
    print(sys.argv)
    if len(sys.argv) != 5:
        raise ValueError
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
