#!/bin/bash

# Be careful with ending '/' !
GL_GROUP=gstreamer
GL_TOKEN="FDO Gitlab token"
GL_SERVER="https://gitlab.freedesktop.org/"
GIT_SERVER=https://anongit.freedekstop.org/git/
BZ_SERVER=https://bugzilla.gnome.org
BZ_USER=
BZ_PASSWORD=

import() {
  component="$1"
  gl_component="$2"
  echo "importing $component into $GL_GROUP/$gl_component"
  bztogl --token $GL_TOKEN --product GStreamer --component "$component" --target-project "$GL_GROUP/$2" --git-server $GIT_SERVER --gitlab-server $GL_SERVER --bugzilla-server $BZ_SERVER --bz-user "$BZ_USER" --bz-pass "$BZ_PASSWORD" 2>gl-import-$gl_component.err
  if [ $? != 0 ]; then
    exit
  fi
}

# list taken from https://bugzilla.gnome.org/page.cgi?id=browse.html&product=GStreamer

import 'cerbero' 'cerbero'
import 'common' 'common'
import 'documentation' 'gst-docs'
import "don\'t know" "gstreamer-project"
import 'gst-build' 'gst-build'
import 'gst-devtools' 'gst-devtools'
import 'gst-editing-services' 'gst-editing-services'
import 'gst-examples' 'gst-examples'
import 'gst-libav' 'gst-libav'
import 'gst-omx' 'gst-omx'
import 'gst-plugins-bad' 'gst-plugins-bad'
import 'gst-plugins-base' 'gst-plugins-base'
import 'gst-plugins-good' 'gst-plugins-good'
import 'gst-plugins-ugly' 'gst-plugins-ugly'
import 'gst-python' 'gst-python'
import 'gst-rtsp-server' 'gst-rtsp-server'
import 'gst-sharp' 'gstreamer-sharp' # this has a different repo name?
import 'gstreamer (core)' 'gstreamer'
import 'gstreamer-vaapi' 'gstreamer-vaapi'
import 'orc' 'orc'
import 'packages' 'cerbero'
import 'www' 'www'
