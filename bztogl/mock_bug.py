from datetime import datetime, tzinfo, timedelta

timeformat = '%Y-%m-%d %H:%M:%S'


class MockBugzilla(object):
    def __init__(self):
        self.logged_in = False

class MockBug(object):
    def __init__(self):
        self.creator = ""
        self.status = ""
        self.keywords = []
        self.severity = ""
        self.target_milestone = ""
        self.summary = ""
        self.id = -1
        self.bug_id = -1
        self.assigned_to = ""
        self.cc = []
        self.bugzilla = MockBugzilla()
        self.blocks = []
        self.depends_on = []
        self.see_also = []
        self.version = ""
        self.comments = []
        self.creation_time = None
        self.last_change_time = None

    def getcomments(self):
        return self.comments

def make_bug(
        bug_id,
        creation_time=None,
        last_change_time=None,
        version=None,
        status=None,
        severity=None,
        target_milestone=None,
        keywords=None,
        creator=None,
        summary=None,
        assigned_to=None,
        cc=None,
        blocks=None,
        depends_on=None,
        see_also=None,
        comments=None):
    b = MockBug()
    b.id = bug_id
    b.bug_id = bug_id
    if creation_time: b.creation_time = creation_time
    if last_change_time: b.last_change_time = last_change_time
    if version: b.version = version
    if status: b.status = status
    if severity: b.severity = severity
    if target_milestone: b.target_milestone = target_milestone
    if keywords: b.keywords = keywords
    if creator: b.creator = creator
    if summary: b.summary = summary
    if assigned_to: b.assigned_to = assigned_to
    if cc: b.cc = cc
    if blocks: b.blocks = blocks
    if depends_on: b.depends_on = depends_on
    if see_also: b.see_also = see_also
    if comments: b.comments = comments
    return b

def make_datetime(t, tz=None):
    class AEST(tzinfo):
        def utcoffset(self, dt):
            # XXX: change for your values
            return timedelta(hours=10) + self.dst(dt)

        def dst(self, dt):
            # no DST
            return timedelta(0)

    class AEDT(tzinfo):
        def utcoffset(self, dt):
            # XXX: change for your values
            return timedelta(hours=11) + self.dst(dt)

        def dst(self, dt):
            # no DST
            return timedelta(0)

    if tz == 'AEST':
        tz = AEST()
    elif tz == 'AEDT':
        tz = AEDT()

    return datetime.strptime(t, timeformat).replace(tzinfo=tz)


def make_comment(
        author=None,
        creation_time=None,
        attachment_id=-1,
        text=None):
    ret = {}
    if text: ret.update({'text' : text})
    if author: ret.update({'author' : author, 'creator' : author})
    if creation_time: ret.update({'creation_time' : creation_time})
    if attachment_id != -1: ret.update({'attachment_id' : attachment_id})
    return ret

bug796466 = make_bug(796466,
        creation_time=make_datetime('2018-05-31 16:21:54', 'AEST'),
        last_change_time=make_datetime('2018-05-31 19:49:08', 'AEST'),
        version='1.14.0',
        status='NEEDINFO',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='lyon.wang@nxp.com',
        summary='failed to link avmux_spdif link with alsasink for ac3 passthrough',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['t.i.m@zen.co.uk'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='lyon.wang@nxp.com',
                creation_time=make_datetime('2018-05-31 16:21:54', 'AEST'),
                text="""Hi,
   I was trying to realize ac3 audio passthrough via hdmi by a certain pipeline with avmux_spdif.

   By pipeline:
"gst-launch-1.0 filesrc location= 5.1_de.ac3 ! ac3parse ! avmux_spdif ! filesink location= ac3.spdif"
  I managed to dump out the encapsulated ac3 (iec61958) format.

  Then I was trying to link avmux_spdif with alsasink

  Noticing avmux_spdif src caps is "application/x-gst-av-spdif", which alsasink not supported, so I modified avmux_spdif src caps with "audio/x-ac3, framed=true"

    However seems I still met problem when trying to output the data via alsasink

below is some log with GST_DEBUG=audiobasesink:6,2

Setting pipeline to PAUSED ...
0:00:00.182977808 [335m 3646[00m     0x3c8bb190 [33;01mWARN   [00m [00m             basesrc gstbasesrc.c:3583:gst_base_src_start_complete:<filesrc0>[00m pad not activated yet
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstAc3Parse:ac3parse0.GstPad:src: caps = audio/x-ac3, framed=(boolean)true, rate=(int)48000, channels=(int)6, alignment=(string)frame
/GstPipeline:pipeline0/avmux_spdif:avmux_spdif0.GstPad:audio_0: caps = audio/x-ac3, framed=(boolean)true, rate=(int)48000, channels=(int)6, alignment=(string)frame
0:00:00.186149288 [335m 3646[00m     0x3c867280 [32;01mFIXME  [00m [00m            basesink gstbasesink.c:3145:gst_base_sink_default_event:<alsasink0>[00m stream-start event without group-id. Consider implementing group-id handling in the upstream elements
0:00:00.186471848 [335m 3646[00m     0x3c867280 [37mDEBUG  [00m [00m       audiobasesink gstaudiobasesink.c:1197:gst_audio_base_sink_preroll:<alsasink0>[00m ringbuffer in wrong state
0:00:00.186508208 [335m 3646[00m     0x3c867280 [33;01mWARN   [00m [00m       audiobasesink gstaudiobasesink.c:1198:gst_audio_base_sink_preroll:<alsasink0>[00m error: sink not negotiated.
0:00:00.186702488 [335m 3646[00m     0x3c867280 [37mDEBUG  [00m [00m       audiobasesink gstaudiobasesink.c:1197:gst_audio_base_sink_preroll:<alsasink0>[00m ringbuffer in wrong state
ERROR: from element /GstPipeline:pipeline0/GstAlsaSink:alsasink0: The stream is in the wrong format.
0:00:00.186732608 [335m 3646[00m     0x3c867280 [33;01mWARN   [00m [00m       audiobasesink gstaudiobasesink.c:1198:gst_audio_base_sink_preroll:<alsasink0>[00m error: sink not negotiated.
Additional debug info:
../../../../git/gst-libs/gst/audio/gstaudiobasesink.c(1198): gst_audio_base_sink_preroll (): /GstPipeline:pipeline0/GstAlsaSink:alsasink0:
sink not negotiated.
ERROR: pipeline doesn't want to preroll.
Setting pipeline to NULL ...

Do anyone meet similar issue when trying to use avmux_spdif to passthrough ac3 via alsasink?

Any hint will be appreciated for this issue

Thanks
Lyon"""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2018-05-31 18:18:48', 'AEST'),
                text="""This is not how alsasink currently supports passthrough, as far as I know. avmux_spdif should not be needed, and we don't support it.

ac3parse ! alsasink  might/should work."""
            ), make_comment(
                author='lyon.wang@nxp.com',
                creation_time=make_datetime('2018-05-31 18:29:23', 'AEST'),
                text="""(In reply to Tim-Philipp Müller from comment #1)
> This is not how alsasink currently supports passthrough, as far as I know.
> avmux_spdif should not be needed, and we don't support it.
>
> ac3parse ! alsasink  might/should work.

Then who will do the encapsulating of IEC 61937 for the ac3 data?
Suppose ac3 data need to be encapsulated and then send out as passthrough data, which can be recognized and decoded at receiver side? or my understanding is not correct?"""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2018-05-31 18:35:48', 'AEST'),
                text="""It will be done by the audio sink, there are some utility functions in libgstaudio."""
            ), make_comment(
                author='lyon.wang@nxp.com',
                creation_time=make_datetime('2018-05-31 19:49:08', 'AEST'),
                text="""Hi Tim, thanks for your hint.

So I tried with command line:
gst-launch-1.0 filesrc locaiton=5.1_de.ac3 ! ac3parse ! alsasink device=hw:3 --gst-debug=5

and it will report "no such device" error:

0:00:00.142809846  4343     0x272e78f0 DEBUG                   alsa gstalsa.c:472:gst_alsa_open_iec958_pcm:<alsasink0> Generated device string "hw:3:{AES0 0x02 AES1 0x82 AES2 0x00 AES3 0x02}"
0:00:00.143829486  4343     0x272e78f0 WARN                    alsa pcm_hw.c:1713:_snd_pcm_hw_open: alsalib error: Invalid value for card
0:00:00.143927526  4343     0x272e78f0 DEBUG                   alsa gstalsa.c:478:gst_alsa_open_iec958_pcm:<alsasink0> failed opening IEC958 device: No such device

Then I modified "devstr", without adding :{AES0 0X02....}
(Are these string necessary to be added ?)

This time the device can be recognized, however, it reports device busy:
0:00:00.483679220  3462      0xec5f8f0 DEBUG                   alsa gstalsa.c:474:gst_alsa_open_iec958_pcm:<alsasink0> Generated device string "hw:3"
0:00:00.484194620  3462      0xec5f8f0 WARN                    alsa pcm_hw.c:1602:snd_pcm_hw_open: alsalib error: open '/dev/snd/pcmC3D0p' failed (-16): Device or resource busy
0:00:00.484306460  3462      0xec5f8f0 DEBUG                   alsa gstalsa.c:481:gst_alsa_open_iec958_pcm:<alsasink0> failed opening IEC958 device: Device or resource busy


Is there anything I miss or some patch we need to include?
I see in this ticket Bug 721740, similar EBUSY error return, do we miss anything or patch here (I am using GST 1.14.0) ?

Thanks
Lyon"""
            )
        ])

bug732643 = make_bug(732643,
        creation_time=make_datetime('2014-07-03 03:17:04', 'AEST'),
        last_change_time=make_datetime('2014-08-23 16:28:05', 'AEST'),
        version='1.4.0',
        status='NEW',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='stevenvandenbrandenstift@gmail.com',
        summary='dvbcam: error witing TPDU (5): Input/output error afther being connected',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['bilboed@bilboed.com', 'reynaldo@osg.samsung.com'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-03 03:17:04', 'AEST'),
                attachment_id=279792,
                text="""Created attachment 279792
gstreamer trace

when trying to use gstreamer to record video encrypted with a ci smartcard from tv  vlaanderen (dvb-s channel) the cam card gets connected and the slot gets defined , but a little later the connection gets input/output problems

dvbcam camtransport.c:243:cam_tl_connection_write_tpdu: error witing TPDU (5): Input/output error


i use a tbs 5980 with a driver that is updated to latest git code. watching without the encryption works fine.

see stack trace (short version) with is run together with the gnome-dvb-deamon gst1.0 that is still in development.

 the following error also happens at some point before it:

0:07:44.121090623  5096 0x7f41f40034f0 ERROR                 dvbcam camapplication.c:334:session_data_cb: unexpected APDU length 20 expected 26

i also noticed that it opens 3 new sessions in the run? is this normal?

greetings
Steven"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-03 03:18:20', 'AEST'),
                text="""i must add i had to increase the wait time between connecting attemps of the cam module to 1 sec because otherwise the cam module fails to start in time."""
            ), make_comment(
                author='bilboed@bilboed.com',
                creation_time=make_datetime('2014-07-03 15:49:51', 'AEST'),
                text="""alas there's no "safe" way to deal with the various CAM modules out there. Most take a very long time to respond, so the current behaviour is to retry at least 5 times at different intervals (and for the first tries you see that error/warning).

Can you try just with gst-launch ?
I'm not sure what the gnome-dvb-daemon is using/doing, but if they store/update the dvb-channels.conf, you could test it from the command line:

gst-launch-1.0 playbin uri="dvb://<channel_name>"

And provide GST_DEBUG=3,dvb*:6,cam*:6 logs for both an encrypted and unencrypted channel."""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-03 18:23:28', 'AEST'),
                text="""how to define the location of the channels file (its in DVBv5 format) so i can use one in the local folder?"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-18 23:10:51', 'AEST'),
                text="""can you please provide my the command to use to test this? and any files needed?"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-18 23:26:55', 'AEST'),
                text="""this is what i get when running the above command.
where do i have to put the DVB channel configuration file or how to find out?


gst-launch-1.0 playbin uri="dvb://SYFY"
Setting pipeline to PAUSED ...
ERROR: Pipeline doesn't want to pause.
ERROR: from element /GstURIDecodeBin:uridecodebin0: Couldn't find DVB channel configuration file
Additional debug info:
gsturidecodebin.c(1404): gen_source_element (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0:
No element accepted URI 'dvb://SYFY'
Setting pipeline to NULL ...
Freeing pipeline ..."""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-18 23:42:01', 'AEST'),
                text="""i debugged the code and found the path to put the channels file,

i have seen in the code that only zap files will work? or are dvbv5 files also supported? anyway i will test next week to see if i can get the log :p

bbasebin parsechannels.c:69:parse_channels_conf_from_file:<source> parsing '/home/steven/.config/gstreamer-1.0/dvb-channels.conf'"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 18:01:09', 'AEST'),
                attachment_id=281358,
                text="""reated attachment 281358
free to air log BVN"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 18:02:09', 'AEST'),
                attachment_id=281359,
                text="""Created attachment 281359
encrypted channel 'vier' log"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 18:46:14', 'AEST'),
                attachment_id=281366,
                text="""Created attachment 281366
encrypted channel 4 log complete"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 18:57:40', 'AEST'),
                text="""i think this might be related to the problem:

 if (ioctl (fe_fd, FE_READ_STATUS, &status) ||
+      ioctl (fe_fd, FE_READ_SIGNAL_STRENGTH, &_signal) ||
+      ioctl (fe_fd, FE_READ_SNR, &snr) ||
+      ioctl (fe_fd, FE_READ_BER, &ber) ||
+      ioctl (fe_fd, FE_READ_UNCORRECTED_BLOCKS, &uncorrected_blocks)) {
+    GST_WARNING_OBJECT (src, "Failed to get statistics from the device");
+    return;
+  }

i guess that one (or more) of these fail so it gets the warning and returns, why and how to check i do not know, is htere a way (program) to run these commands (ioctl) manually or to see the return values?"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 19:29:23', 'AEST'),
                text="""ok i adjusted the code and recompiledit

 its the ioctl (fe_fd, FE_READ_UNCORRECTED_BLOCKS, &uncorrected_blocks) that fails and that makes the method return over and over.

now i do not know where in the driver to find the problem?"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 19:40:36', 'AEST'),
                text="""or is this not required for the good working of gstreamer? then we can just ingnore the uncorrected blocks and set them to 0 for example if the FE_READ_UNCORRECTED_BLOCKS is not implemented in the driver?"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 20:52:34', 'AEST'),
                attachment_id=281377,
                text="""Created attachment 281377
vier encrypted channel log

excuse me , but the channel info was not correct now it complains only about this:

dvbcam camtransport.c:237:cam_tl_connection_write_tpdu:[00m writing TPDU 82 (CREATE_T_C) connection 1 (size:5)
dvbcam camtransport.c:243:cam_tl_connection_write_tpdu:[00m error witing TPDU (22): Ongeldig argument
dvbcam camtransport.c:370:cam_tl_create_connection:[00m Failed sending initial connection message .. but retrying"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 21:25:40', 'AEST'),
                text="""i figured out it takes more then 3 seconds for the cam to get in working state:

this was done with also raising the wait time to 1 sec , ill check if the 1 second had influence or not.

see maybe the timeout should be raised to about 5 seconds?"""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-07-22 21:38:50', 'AEST'),
                text="""it goes well with the normal retry mode but only 5 seconds total instead of 2.5

but new problem comes up:

dvbcam camapplication.c:334:session_data_cb: unexpected APDU length 20 expected 26

afther that every time this :

dvbcam camtransport.c:500:cam_tl_read_all: error reading TPDU from module: -12
 -->CAM_RETURN_TRANSPORT_TIMEOUT

raising the polling to 1 sec and the read timout to 5 sec changes nothing.

should i try lowering it?

so the error starts with a APDU thats too short? afther that every read fails."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-08-23 09:41:12', 'AEST'),
                text="""(In reply to comment #6)
> i debugged the code and found the path to put the channels file,
>
> i have seen in the code that only zap files will work? or are dvbv5 files also
> supported? anyway i will test next week to see if i can get the log :p
> [..]

The channels.conf parsing code can only deal with ZAP formated
files right now. Adding dvbv5 format support is on my TODO but
I haven't get around doing it yet. Also, watch out for GStreamer
having problems parsing your ZAP formatted conf, as there
are multiple variations among scanning tools and while the
order of the parameters seems to be usually the same for DVB-S
the value units change."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-08-23 09:43:37', 'AEST'),
                text="""(In reply to comment #5)
> this is what i get when running the above command.
> where do i have to put the DVB channel configuration file or how to find out?
> [..]

You can set the GST_DVB_CHANNELS_CONF env var at launch time
to specify where your channels.conf file is located."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-08-23 09:58:15', 'AEST'),
                text="""(In reply to comment #10)
> i think this might be related to the problem:
>
>  if (ioctl (fe_fd, FE_READ_STATUS, &status) ||
> +      ioctl (fe_fd, FE_READ_SIGNAL_STRENGTH, &_signal) ||
> +      ioctl (fe_fd, FE_READ_SNR, &snr) ||
> +      ioctl (fe_fd, FE_READ_BER, &ber) ||
> +      ioctl (fe_fd, FE_READ_UNCORRECTED_BLOCKS, &uncorrected_blocks)) {
> +    GST_WARNING_OBJECT (src, "Failed to get statistics from the device");
> +    return;
> +  }
>
> i guess that one (or more) of these fail so it gets the warning and returns,
> why and how to check i do not know, is htere a way (program) to run these
> commands (ioctl) manually or to see the return values?

Are you actually getting this warning? I don't see it in your log.

Whatever the case the block you pasted from _output_frontend_stats()
does needs some work. Those ioctl need to wrapped around LOOP_WHILE_EINTR
(as they can a do return EINTR from time to time) for example but
I'm pretty sure this is not the reason for your failure. Don't take
my word for granted though and try commenting out the calls to
gst_dvbsrc_output_frontend_stats () and see for yourself. You should
see no difference."""
            ), make_comment(
                author='stevenvandenbrandenstift@gmail.com',
                creation_time=make_datetime('2014-08-23 16:28:05', 'AEST'),
                text="""thanks for your input here but i must say the only error appearing now (afther changing to correct zap file and increasing the times trying) is this one

it goes well with the normal retry mode but only 5 seconds total instead of 2.5

but new problem comes up:

dvbcam camapplication.c:334:session_data_cb: unexpected APDU length 20 expected
26

afther that every time this :

dvbcam camtransport.c:500:cam_tl_read_all: error reading TPDU from module: -12
 -->CAM_RETURN_TRANSPORT_TIMEOUT


so the error starts with a APDU thats too short? afther that every read fails."""
            )
        ]
    )

bug783516 = make_bug(783516,
        creation_time=make_datetime('2017-06-08 01:06:24', 'AEST'),
        last_change_time=make_datetime('2017-06-08 01:06:24', 'AEST'),
        version='1.x',
        status='NEW',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='tsaunier@gnome.org',
        summary='rtsp: Racy timeout when connecting to the server running change_state_intensive validate scenario',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=[],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='tsaunier@gnome.org',
                creation_time=make_datetime('2017-06-08 01:06:24', 'AEST'),
                text="""When running the GstValidate change_state_intensive scenario on RTSP stream, we have a race where going back to PLAYING times out trying to connect to the server to retrieve the SDP.

This issue happens with any stream.

Logs example:

=================
Test name: validate.rtsp.playback.change_state_intensive.vorbis_vp8_1_webm
Command: '/home/thiblahute/devel/gstreamer/gst-build/build/subprojects/gst-devtools/validate/tools/gst-validate-1.0 playbin uri=rtsp://127.0.0.1:45897/test audio-sink=fakesink sync=true video-sink=fakesink sync=true qos=true max-lateness=20000000 --set-media-info /home/thiblahute/gst-validate/gst-integration-testsuites/medias/defaults/webm/vorbis_vp8.1.webm.media_info'
=================


=========================================
Running scenario change_state_intensive on pipeline playbin0
=========================================
Starting pipeline
Prerolling...
Pipeline started
Executing (40/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.396170150)
Executing (39/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.386018905)
Executing (38/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.370561907)
Executing (37/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.379416772)
Executing (36/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.372735544)
Executing (35/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.375469067)
Executing (34/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.372242035)
Executing (33/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.379282019)
Executing (32/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.384176014)
Executing (31/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.376992469)
Executing (30/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.380841170)
Executing (29/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.370176525)
Executing (28/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.375556683)
Executing (27/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.372499633)
Executing (26/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.384619616)
Executing (25/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.376216970)
Executing (24/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.377693217)
Executing (23/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.376284539)
Executing (22/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.374066716)
Executing (21/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.377430032)
Executing (20/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.382110615)
Executing (19/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.370325036)
Executing (18/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.379305769)
Executing (17/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.377927701)
Executing (16/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.380539763)
Executing (15/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.379554751)
Executing (14/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.374322413)
Executing (13/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
  -> Action set-state done (duration: 0:00:00.376795187)
Executing (12/40) set-state: state=null repeat=40
Executing (subaction) set-state: state=playing
0:00:30.821920623 [332m14101[00m 0x561af013d230 [31;01mERROR  [00m [00m            validate gst-validate-reporter.c:195:gst_validate_report_valist:[00m <playbin0> 2258 (critical) : runtime: We got an ERROR message on the bus : Got error: Could not read from resource. -- Debug message: ../subprojects/gst-plugins-good/gst/rtsp/gstrtspsrc.c(5449): gst_rtspsrc_try_send (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin29/GstRTSPSrc:source:
Could not receive message. (Timeout while waiting for server response)
0:00:30.822518919 [332m14101[00m 0x561af00cda40 [31;01mERROR  [00m [00;33m gstvalidatescenario gst-validate-scenario.c:2700:message_cb:[00m Actions:
                    set-state, state=(string)null, sub-action=(string)"set-state\,\ state\=playing", repeat=(int)40;
0:00:30.842309778 [332m14101[00m 0x561af00cda40 [31;01mERROR  [00m [00m            validate gst-validate-reporter.c:195:gst_validate_report_valist:[00m <change_state_intensive> 2261 (critical) : scenario: The program stopped before some actions were executed : 1 actions were not executed:
                    set-state, state=(string)null, sub-action=(string)"set-state\,\ state\=playing", repeat=(int)40;
Executing  stop:
   warning : received the same caps twice
             Detected on <rtpvp8depay0:sink>
             Detected on <vp8dec0:sink>
             Detected on <vorbisdec0:sink>
             Detected on <rtpvorbisdepay0:sink>
             Detected on <rtpvp8depay1:sink>
             Detected on <vp8dec1:sink>
             Detected on <vorbisdec1:sink>
             Detected on <rtpvorbisdepay1:sink>
             Detected on <rtpvp8depay2:sink>
             Detected on <vp8dec2:sink>
             Detected on <vorbisdec2:sink>
             Detected on <rtpvorbisdepay2:sink>
             Detected on <rtpvp8depay3:sink>
             Detected on <vp8dec3:sink>
             Detected on <vorbisdec3:sink>
             Detected on <rtpvorbisdepay3:sink>
             Detected on <rtpvp8depay4:sink>
             Detected on <vp8dec4:sink>
             Detected on <vorbisdec4:sink>
             Detected on <rtpvorbisdepay4:sink>
             Detected on <rtpvp8depay5:sink>
             Detected on <vp8dec5:sink>
             Detected on <vorbisdec5:sink>
             Detected on <rtpvorbisdepay5:sink>
             Detected on <rtpvp8depay6:sink>
             Detected on <vp8dec6:sink>
             Detected on <vorbisdec6:sink>
             Detected on <rtpvorbisdepay6:sink>
             Detected on <rtpvp8depay7:sink>
             Detected on <vp8dec7:sink>
             Detected on <vorbisdec7:sink>
             Detected on <rtpvorbisdepay7:sink>
             Detected on <rtpvp8depay8:sink>
             Detected on <vp8dec8:sink>
             Detected on <vorbisdec8:sink>
             Detected on <rtpvorbisdepay8:sink>
             Detected on <rtpvp8depay9:sink>
             Detected on <vp8dec9:sink>
             Detected on <vorbisdec9:sink>
             Detected on <rtpvorbisdepay9:sink>
             Detected on <rtpvp8depay10:sink>
             Detected on <vp8dec10:sink>
             Detected on <vorbisdec10:sink>
             Detected on <rtpvorbisdepay10:sink>
             Detected on <rtpvp8depay11:sink>
             Detected on <vp8dec11:sink>
             Detected on <vorbisdec11:sink>
             Detected on <rtpvorbisdepay11:sink>
             Detected on <rtpvp8depay12:sink>
             Detected on <vp8dec12:sink>
             Detected on <vorbisdec12:sink>
             Detected on <rtpvorbisdepay12:sink>
             Detected on <rtpvp8depay13:sink>
             Detected on <vp8dec13:sink>
             Detected on <vorbisdec13:sink>
             Detected on <rtpvorbisdepay13:sink>
             Detected on <rtpvp8depay14:sink>
             Detected on <vp8dec14:sink>
             Detected on <vorbisdec14:sink>
             Detected on <rtpvorbisdepay14:sink>
             Detected on <rtpvp8depay15:sink>
             Detected on <vp8dec15:sink>
             Detected on <vorbisdec15:sink>
             Detected on <rtpvorbisdepay15:sink>
             Detected on <rtpvp8depay16:sink>
             Detected on <vp8dec16:sink>
             Detected on <vorbisdec16:sink>
             Detected on <rtpvorbisdepay16:sink>
             Detected on <rtpvp8depay17:sink>
             Detected on <vp8dec17:sink>
             Detected on <vorbisdec17:sink>
             Detected on <rtpvorbisdepay17:sink>
             Detected on <rtpvp8depay18:sink>
             Detected on <vp8dec18:sink>
             Detected on <vorbisdec18:sink>
             Detected on <rtpvorbisdepay18:sink>
             Detected on <rtpvp8depay19:sink>
             Detected on <vp8dec19:sink>
             Detected on <vorbisdec19:sink>
             Detected on <rtpvorbisdepay19:sink>
             Detected on <rtpvp8depay20:sink>
             Detected on <vp8dec20:sink>
             Detected on <vorbisdec20:sink>
             Detected on <rtpvorbisdepay20:sink>
             Detected on <rtpvp8depay21:sink>
             Detected on <vp8dec21:sink>
             Detected on <vorbisdec21:sink>
             Detected on <rtpvorbisdepay21:sink>
             Detected on <rtpvp8depay22:sink>
             Detected on <vp8dec22:sink>
             Detected on <vorbisdec22:sink>
             Detected on <rtpvorbisdepay22:sink>
             Detected on <rtpvp8depay23:sink>
             Detected on <vp8dec23:sink>
             Detected on <vorbisdec23:sink>
             Detected on <rtpvorbisdepay23:sink>
             Detected on <rtpvp8depay24:sink>
             Detected on <vp8dec24:sink>
             Detected on <vorbisdec24:sink>
             Detected on <rtpvorbisdepay24:sink>
             Detected on <rtpvp8depay25:sink>
             Detected on <vp8dec25:sink>
             Detected on <vorbisdec25:sink>
             Detected on <rtpvorbisdepay25:sink>
             Detected on <rtpvp8depay26:sink>
             Detected on <vp8dec26:sink>
             Detected on <vorbisdec26:sink>
             Detected on <rtpvorbisdepay26:sink>
             Detected on <rtpvp8depay27:sink>
             Detected on <vp8dec27:sink>
             Detected on <vorbisdec27:sink>
             Detected on <rtpvorbisdepay27:sink>

   warning : a new segment event has different value than the received one
             Detected on <rtpvp8depay0:src>
             Detected on <rtpvorbisdepay0:src>
             Detected on <rtpvp8depay1:src>
             Detected on <rtpvorbisdepay1:src>
             Detected on <rtpvp8depay2:src>
             Detected on <rtpvorbisdepay2:src>
             Detected on <rtpvp8depay3:src>
             Detected on <rtpvorbisdepay3:src>
             Detected on <rtpvp8depay4:src>
             Detected on <rtpvorbisdepay4:src>
             Detected on <rtpvp8depay5:src>
             Detected on <rtpvorbisdepay5:src>
             Detected on <rtpvp8depay6:src>
             Detected on <rtpvorbisdepay6:src>
             Detected on <rtpvp8depay7:src>
             Detected on <rtpvorbisdepay7:src>
             Detected on <rtpvp8depay8:src>
             Detected on <rtpvorbisdepay8:src>
             Detected on <rtpvp8depay9:src>
             Detected on <rtpvorbisdepay9:src>
             Detected on <rtpvp8depay10:src>
             Detected on <rtpvorbisdepay10:src>
             Detected on <rtpvp8depay11:src>
             Detected on <rtpvorbisdepay11:src>
             Detected on <rtpvp8depay12:src>
             Detected on <rtpvorbisdepay12:src>
             Detected on <rtpvp8depay13:src>
             Detected on <rtpvorbisdepay13:src>
             Detected on <rtpvp8depay14:src>
             Detected on <rtpvorbisdepay14:src>
             Detected on <rtpvp8depay15:src>
             Detected on <rtpvorbisdepay15:src>
             Detected on <rtpvp8depay16:src>
             Detected on <rtpvorbisdepay16:src>
             Detected on <rtpvp8depay17:src>
             Detected on <rtpvorbisdepay17:src>
             Detected on <rtpvp8depay18:src>
             Detected on <rtpvorbisdepay18:src>
             Detected on <rtpvp8depay19:src>
             Detected on <rtpvorbisdepay19:src>
             Detected on <rtpvp8depay20:src>
             Detected on <rtpvorbisdepay20:src>
             Detected on <rtpvp8depay21:src>
             Detected on <rtpvorbisdepay21:src>
             Detected on <rtpvp8depay22:src>
             Detected on <rtpvorbisdepay22:src>
             Detected on <rtpvp8depay23:src>
             Detected on <rtpvorbisdepay23:src>
             Detected on <rtpvp8depay24:src>
             Detected on <rtpvorbisdepay24:src>
             Detected on <rtpvp8depay25:src>
             Detected on <rtpvorbisdepay25:src>
             Detected on <rtpvp8depay26:src>
             Detected on <rtpvorbisdepay26:src>
             Detected on <rtpvp8depay27:src>
             Detected on <rtpvorbisdepay27:src>
             Description : when receiving a new segment, an element should push an equivalentsegment downstream

  critical : We got an ERROR message on the bus
             Detected on <playbin0>
             Details : Got error: Could not read from resource. -- Debug message: ../subprojects/gst-plugins-good/gst/rtsp/gstrtspsrc.c(5449): gst_rtspsrc_try_send (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin29/GstRTSPSrc:source:
                     Could not receive message. (Timeout while waiting for server response)
             dotfile : /home/thiblahute/.cache/gstmkdump//0:00:30.802809687-validate-report-critical-on-playbin0-runtime::error-on-bus.dot
             backtrace :
               gst_debug_get_stack_trace (gstinfo.c:2787)
               gst_validate_report_new (gst-validate-report.c:710)
               gst_validate_report_valist (gst-validate-reporter.c:186)
               gst_validate_report (gst-validate-reporter.c:301)
               _bus_handler (gst-validate-pipeline-monitor.c:503)
               ffi_call_unix64 (/usr/lib/libffi.so.6.0.4:0x7fa294a0a1c4)
               ffi_call (/usr/lib/libffi.so.6.0.4:0x7fa294a09c26)
               g_cclosure_marshal_generic (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967cd7aa)
               g_closure_invoke (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967ccecb)
               ?? (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967def7e)
               g_signal_emit_valist (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967e7bd8)
               g_signal_emit (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967e7fbb)
               gst_bus_sync_signal_handler (gstbus.c:1274)
               gst_bus_post (gstbus.c:342)
               gst_element_post_message_default (gstelement.c:1789)
               gst_bin_post_message (gstbin.c:2856)
               gst_element_post_message (gstelement.c:1832)
               gst_bin_handle_message_func (gstbin.c:4107)
               gst_pipeline_handle_message (gstpipeline.c:600)
               gst_play_bin_handle_message (gstplaybin2.c:3012)
               bin_bus_handler (gstbin.c:3327)
               gst_bus_post (gstbus.c:336)
               gst_element_post_message_default (gstelement.c:1789)
               gst_bin_post_message (gstbin.c:2856)
               gst_element_post_message (gstelement.c:1832)
               gst_bin_handle_message_func (gstbin.c:4107)
               handle_message (gsturidecodebin.c:2438)
               bin_bus_handler (gstbin.c:3327)
               gst_bus_post (gstbus.c:336)
               gst_element_post_message_default (gstelement.c:1789)
               gst_bin_post_message (gstbin.c:2856)
               gst_element_post_message (gstelement.c:1832)
               gst_element_message_full_with_details (gstelement.c:1968)
               gst_element_message_full (gstelement.c:2007)
               gst_rtspsrc_try_send (gstrtspsrc.c:5448)
               gst_rtspsrc_send (gstrtspsrc.c:5520)
               gst_rtspsrc_retrieve_sdp (gstrtspsrc.c:6684)
               gst_rtspsrc_open (gstrtspsrc.c:6823)
               gst_rtspsrc_thread (gstrtspsrc.c:7616)
               gst_task_func (gsttask.c:332)
               default_func (gsttaskpool.c:69)
               ?? (/usr/lib/libglib-2.0.so.0.5200.2:0x7fa296a81c8a)
               ?? (/usr/lib/libglib-2.0.so.0.5200.2:0x7fa296a81291)
               start_thread (/usr/lib/libpthread-2.25.so:0x7fa296211293)
               __clone (/usr/lib/libc-2.25.so:0x7fa295f5225b)


  critical : The program stopped before some actions were executed
             Detected on <change_state_intensive>
             Details : 1 actions were not executed:
                                         set-state, state=(string)null, sub-action=(string)"set-state\,\ state\=playing", repeat=(int)40;
             dotfile : /home/thiblahute/.cache/gstmkdump//0:00:30.823199351-validate-report-critical-on-change_state_intensive-scenario::not-ended.dot
             backtrace :
               gst_debug_get_stack_trace (gstinfo.c:2787)
               gst_validate_report_new (gst-validate-report.c:710)
               gst_validate_report_valist (gst-validate-reporter.c:186)
               gst_validate_report (gst-validate-reporter.c:301)
               message_cb (gst-validate-scenario.c:2702)
               ffi_call_unix64 (/usr/lib/libffi.so.6.0.4:0x7fa294a0a1c4)
               ffi_call (/usr/lib/libffi.so.6.0.4:0x7fa294a09c26)
               g_cclosure_marshal_generic (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967cd7aa)
               g_closure_invoke (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967ccf71)
               ?? (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967def7e)
               g_signal_emit_valist (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967e7bd8)
               g_signal_emit (/usr/lib/libgobject-2.0.so.0.5200.2:0x7fa2967e7fbb)
               gst_bus_async_signal_func (gstbus.c:1247)
               gst_bus_source_dispatch (gstbus.c:841)
               g_main_context_dispatch (/usr/lib/libglib-2.0.so.0.5200.2:0x7fa296a59666)
               ?? (/usr/lib/libglib-2.0.so.0.5200.2:0x7fa296a59a1c)
               g_main_loop_run (/usr/lib/libglib-2.0.so.0.5200.2:0x7fa296a59d3e)
               main (gst-validate.c:509)
               __libc_start_main (/usr/lib/libc-2.25.so:0x7fa295e85436)
               _start (/home/thiblahute/devel/gstreamer/gst-build/build/subprojects/gst-devtools/validate/tools/gst-validate-1.0:0x561aee96f716)




==== Got criticals. Return value set to 18 ====
     Critical error Got error: Could not read from resource. -- Debug message: ../subprojects/gst-plugins-good/gst/rtsp/gstrtspsrc.c(5449): gst_rtspsrc_try_send (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin29/GstRTSPSrc:source:
Could not receive message. (Timeout while waiting for server response)
     Critical error 1 actions were not executed:
                    set-state, state=(string)null, sub-action=(string)"set-state\,\ state\=playing", repeat=(int)40;

Issues found: 4
Returning 18 as errors were found

=======> Test FAILED (Return value: 18)"""
            )
        ]
    )

bug788777 = make_bug(788777,
        creation_time=make_datetime('2017-10-11 01:04:39', 'AEDT'),
        last_change_time=make_datetime('2018-07-09 08:52:13', 'AEST'),
        version='1.x',
        status='NEW',
        severity='blocker',
        target_milestone='git master',
        keywords=[],
        creator='lists@svrinformatica.it',
        summary='rtpjitterbuffer/h264parse timestamp issue (regression)',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['gstreamer@pexip.com', 'havard.graff@gmail.com', 'nirbheek.chauhan@gmail.com', 'pedro@pedrocr.net', 'slomo@coaxion.net', 't.i.m@zen.co.uk'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-11 01:04:39', 'AEDT'),
                attachment_id=361248,
                text="""Created attachment 361248
logs that show the issue

In a pipeline like this:

rtspsrc ! rtph264depay ! h264parse ! ...

using rtsp over tcp can happen that rtspsrc outputs buffers with the same timestamp, when this happen h264parse outputs buffer with invalid timestamps.

Please take a look at the attached logs, you can see:

rtpjitterbuffer.c:916:rtp_jitter_buffer_calculate_pts:[00m backwards timestamps, using previous time

so different buffers with pts 0:15:23.020362975 are sended.

Not sure how to handle this case, we need to change rtpjitterbuffer or h264parse?

This problem seems to happen only using rtsp over tcp, I'm unable to reproduce it using rtsp over udp."""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-11 04:18:07', 'AEDT'),
                text="""this problem does not happen in 1.6 branch, while I can reproduce it in 1.12 and git master, the main difference seems that in 1.6 when a backward timestamp is detected a resync is scheduled, while in 1.12 the previous timestamp is used, tomorrow I'll do some other tests to better understand the different behaviour"""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2017-10-11 04:39:07', 'AEDT'),
                text="""Do you have a pcap capture or so to reproduce the issue?"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-11 06:42:17', 'AEDT'),
                text="""It seems to happen with any rtsp camera, here is a capture from an axis one (that use gst-rtsp-server)

http://94.177.162.225/temp/788777_full.pcapng

please look at the end of the capture between timestamp 664022331083 and 664048333625 and between timestamp 670648340011 and 670127771194

thanks!"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-11 22:57:25', 'AEDT'),
                text="""this regression start to happen with 1.10, I'm now trying to identify the commit"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-12 00:14:38', 'AEDT'),
                text="""this is the commit that introduce the regression

https://cgit.freedesktop.org/gstreamer/gst-plugins-good/commit/?h=1.10&id=5db8ce66d0d6d426b8406d651238381217006eda"""
            ), make_comment(
                author='havard.graff@gmail.com',
                creation_time=make_datetime('2017-10-12 05:28:22', 'AEDT'),
                text="""One thing that jumps out from the logs is the lack of dts on the buffer. pop_and_push_next:<rtpjitterbuffer0>[00m Pushing buffer 49694, dts 99:99:99.999999999, pts 0:15:23.020362975

Now, udpsrc sets dts as "arrival time" and the jitterbuffer sort of assumes this being the arrival time, and calculates the pts. Maybe there are some improvements that could be done in terms of dts being CLOCK_TIME_NONE?

Alternatively, maybe the tcpsrc should be setting dts in the same way udpsrc does it."""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2017-10-12 05:39:06', 'AEDT'),
                text="""Over TCP it works intentionally like that. TCP is not very regular with packet arrival times, things arrive in batches usually due to how TCP works and buffering everywhere along the path.

With TCP the idea is that the first buffer has timestamps, and from then on we simply interpolate based on the RTP timestamps."""
            ), make_comment(
                author='havard.graff@gmail.com',
                creation_time=make_datetime('2017-10-12 05:47:39', 'AEDT'),
                text="""Maybe we even could use the arrival time in the jitterbuffer as "dts" when dts is -1?

@Nicola: you feel like getting down and dirty with the jitterbuffer-tests? A quick scan reveals that we indeed have no tests that pass in -1 (GST_CLOCK_TIME_NONE) as dts, so you could write a simple test that:

1. Pushes some buffers with dts set to GST_CLOCK_TIME_NONE (generate_test_buffer_full)
2. Increments the time on the clock (with, say, 20ms), for each buffer pushed, simulating that the buffers are arriving at appropriate times.
3. Verifies that the output pts on the buffers are indeed incrementing (which they might not be now)"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-13 07:18:03', 'AEDT'),
                text="""(In reply to Håvard Graff (hgr) from comment #8)
> Maybe we even could use the arrival time in the jitterbuffer as "dts" when
> dts is -1?
>
> @Nicola: you feel like getting down and dirty with the jitterbuffer-tests? A
> quick scan reveals that we indeed have no tests that pass in -1
> (GST_CLOCK_TIME_NONE) as dts, so you could write a simple test that:
>
> 1. Pushes some buffers with dts set to GST_CLOCK_TIME_NONE
> (generate_test_buffer_full)
> 2. Increments the time on the clock (with, say, 20ms), for each buffer
> pushed, simulating that the buffers are arriving at appropriate times.
> 3. Verifies that the output pts on the buffers are indeed incrementing
> (which they might not be now)

Sorry for the delay, I'll try to write a test case, probably I can find some spare time the next week, but I don't think will be easy, the problem does not happen for each buffer but happens randomically (probably when the camera burst data), 10-15 buffers are sended with the same timestamp and after that the stream works normally for 10-15 minutes and so on"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2017-10-18 04:46:40', 'AEDT'),
                text="""I did some quick experiments, the point is to generate something that triggers the condition here:

https://cgit.freedesktop.org/gstreamer/gst-plugins-good/tree/gst/rtpmanager/rtpjitterbuffer.c#n907

for example:

for (b = 1; b < 5; b++) {
  gst_harness_set_time (h, 10 * GST_SECOND + 20 * GST_MSECOND * b);
  if (b < 2){
     fail_unless_equals_int (GST_FLOW_OK,
        gst_harness_push (h,
            generate_test_buffer_full (-1, b, b * TEST_RTP_TS_DURATION)));
  } else {
     fail_unless_equals_int (GST_FLOW_OK,
        gst_harness_push (h,
            generate_test_buffer_full (-1, b, (b-1) * TEST_RTP_TS_DURATION)));
  }
}

so the buffer pushed when b = 1 and b = 2 will have the same timestamp

the real logs shows that one of the other conditions was met.

I'm a bit busy these days hope this is enough"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2018-02-26 22:12:42', 'AEDT'),
                text="""Nicola, any news on this? Can you provide a patch against the existing jitterbuffer tests to show this problem? Also is it still a problem with latest GIT?

Håvard, any ideas what to do about this one? Your commit introduced it back then :)"""
            ), make_comment(
                author='havard.graff@gmail.com',
                creation_time=make_datetime('2018-02-27 19:21:29', 'AEDT'),
                text="""(In reply to Sebastian Dröge (slomo) from comment #11)
> Håvard, any ideas what to do about this one? Your commit introduced it back
> then :)

If there is a failing test-case I am happy to take a look. I guess the hardest bit here is to decide what is the correct behavior in this case, and a well written test can provide that. And for the record: My commit fixed a regression introduced in 1.x with mixing dts and pts for lost-events, back in 0.10 this was done correctly since there was only one timestamp. :)"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2018-03-13 06:04:05', 'AEDT'),
                text="""Ok so will have to wait until after 1.14 then :)"""
            ), make_comment(
                author='pedro@pedrocr.net',
                creation_time=make_datetime('2018-07-09 00:43:39', 'AEST'),
                text="""I've hit what I think is this bug trying to do rtsp streaming from an IP camera. For example:

$ gst-launch-1.0 --gst-debug-level=2 uridecodebin uri="rtsp://xxx" caps="video/x-h264" ! h264parse ! mp4mux ! filesink location=test.mp4
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Progress: (open) Opening Stream
Progress: (connect) Connecting to rtspu://10.23.67.76/user=admin&password=&channel=1&stream=0.sdp?real_stream
Progress: (open) Retrieving server options
Progress: (open) Retrieving media info
Progress: (request) SETUP stream 0
Progress: (open) Opened Stream
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Progress: (request) Sending PLAY request
Progress: (request) Sending PLAY request
Progress: (request) Sent PLAY request
0:00:02.155699947 19264 0x7f1fe4003630 WARN                   qtmux gstqtmux.c:4553:gst_qt_mux_add_buffer:<mp4mux0> error: Buffer has no PTS.
ERROR: from element /GstPipeline:pipeline0/GstMP4Mux:mp4mux0: Could not multiplex stream.
Additional debug info:
gstqtmux.c(4553): gst_qt_mux_add_buffer (): /GstPipeline:pipeline0/GstMP4Mux:mp4mux0:
Buffer has no PTS.
Execution ended after 0:00:02.061331145
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
0:00:02.156376777 19264 0x55a03c547370 WARN                 rtspsrc gstrtspsrc.c:5915:gst_rtsp_src_receive_response:<source> receive interrupted
0:00:02.156393147 19264 0x55a03c547370 WARN                 rtspsrc gstrtspsrc.c:8242:gst_rtspsrc_pause:<source> PAUSE interrupted
Setting pipeline to NULL ...
Freeing pipeline ...

Using matroskamux instead of mp4mux appears to work but only because instead of failing it creates a file without any I-frames:

$ gst-launch-1.0 --gst-debug-level=2 uridecodebin uri="rtsp://xxx" caps="video/x-h264" ! h264parse ! matroskamux ! filesink location=test.mp4
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Progress: (open) Opening Stream
Progress: (connect) Connecting to rtspu://10.23.67.76/user=admin&password=&channel=1&stream=0.sdp?real_stream
Progress: (open) Retrieving server options
Progress: (open) Retrieving media info
Progress: (request) SETUP stream 0
Progress: (open) Opened Stream
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Progress: (request) Sending PLAY request
Progress: (request) Sending PLAY request
Progress: (request) Sent PLAY request
0:00:02.176154384 19308 0x7fad98003f70 WARN             matroskamux matroska-mux.c:3685:gst_matroska_mux_write_data:<matroskamux0:video_0> Invalid buffer timestamp; dropping buffer
0:00:02.181343483 19308 0x7fad98003f70 WARN             matroskamux matroska-mux.c:3685:gst_matroska_mux_write_data:<matroskamux0:video_0> Invalid buffer timestamp; dropping buffer
0:00:04.182640326 19308 0x7fad98003f70 WARN             matroskamux matroska-mux.c:3685:gst_matroska_mux_write_data:<matroskamux0:video_0> Invalid buffer timestamp; dropping buffer
0:00:06.180916572 19308 0x7fad98003f70 WARN             matroskamux matroska-mux.c:3685:gst_matroska_mux_write_data:<matroskamux0:video_0> Invalid buffer timestamp; dropping buffer
0:00:08.181572733 19308 0x7fad98003f70 WARN             matroskamux matroska-mux.c:3685:gst_matroska_mux_write_data:<matroskamux0:video_0> Invalid buffer timestamp; dropping buffer
0:00:10.181878193 19308 0x7fad98003f70 WARN             matroskamux matroska-mux.c:3685:gst_matroska_mux_write_data:<matroskamux0:video_0> Invalid buffer timestamp; dropping buffer
(...)

I added a probe to the h264parser src and it seems that all the I-frames end up with broken pts values (each line is a buffer):

flags: DISCONT | HEADER | DELTA_UNIT pts 00:00:00.130488980 dts 00:00:00.132701175
flags: (empty) pts 99:99:99.999999999 dts 00:00:00.144599571
flags: DELTA_UNIT pts 00:00:00.170488896 dts 00:00:00.172938726
flags: DELTA_UNIT pts 00:00:00.210488559 dts 00:00:00.212886204
flags: DELTA_UNIT pts 00:00:00.250487801 dts 00:00:00.252749559
flags: DELTA_UNIT pts 00:00:00.290486458 dts 00:00:00.292753600
flags: DELTA_UNIT pts 00:00:00.330484373 dts 00:00:00.332698810
flags: DELTA_UNIT pts 00:00:00.370481400 dts 00:00:00.373815622
flags: DELTA_UNIT pts 00:00:00.410477411 dts 00:00:00.412160732
flags: DELTA_UNIT pts 00:00:00.450472303 dts 00:00:00.452277058
flags: DELTA_UNIT pts 00:00:00.490466004 dts 00:00:00.492262089
flags: DELTA_UNIT pts 00:00:00.530458479 dts 00:00:00.532317524
flags: DELTA_UNIT pts 00:00:00.570449738 dts 00:00:00.572483015
flags: DELTA_UNIT pts 00:00:00.610439839 dts 00:00:00.612719141
flags: DELTA_UNIT pts 00:00:00.650428891 dts 00:00:00.652659546
flags: DELTA_UNIT pts 00:00:00.690417052 dts 00:00:00.692286976
flags: DELTA_UNIT pts 00:00:00.730404526 dts 00:00:00.732778163
flags: DELTA_UNIT pts 00:00:00.770391557 dts 00:00:00.772393936
flags: DELTA_UNIT pts 00:00:00.810378416 dts 00:00:00.812636200
flags: DELTA_UNIT pts 00:00:00.850365386 dts 00:00:00.852443918
flags: DELTA_UNIT pts 00:00:00.890352749 dts 00:00:00.893038377
flags: DELTA_UNIT pts 00:00:00.930340769 dts 00:00:00.932568091
flags: DELTA_UNIT pts 00:00:00.970315534 dts 00:00:00.972372486
flags: DELTA_UNIT pts 00:00:01.010292724 dts 00:00:01.012433147
flags: DELTA_UNIT pts 00:00:01.050272620 dts 00:00:01.052350431
flags: DELTA_UNIT pts 00:00:01.090255361 dts 00:00:01.092344823
flags: DELTA_UNIT pts 00:00:01.130240949 dts 00:00:01.132636784
flags: DELTA_UNIT pts 00:00:01.170229258 dts 00:00:01.172454366
flags: DELTA_UNIT pts 00:00:01.210220059 dts 00:00:01.213108059
flags: DELTA_UNIT pts 00:00:01.250213051 dts 00:00:01.253091072
flags: DELTA_UNIT pts 00:00:01.290207891 dts 00:00:01.292905405
flags: DELTA_UNIT pts 00:00:01.330204227 dts 00:00:01.333063222
flags: DELTA_UNIT pts 00:00:01.370201723 dts 00:00:01.372836432
flags: DELTA_UNIT pts 00:00:01.410200080 dts 00:00:01.412488087
flags: DELTA_UNIT pts 00:00:01.450199049 dts 00:00:01.452409799
flags: DELTA_UNIT pts 00:00:01.490198431 dts 00:00:01.492800029
flags: DELTA_UNIT pts 00:00:01.530198079 dts 00:00:01.532800996
flags: DELTA_UNIT pts 00:00:01.570197889 dts 00:00:01.573130835
flags: DELTA_UNIT pts 00:00:01.610197792 dts 00:00:01.612535500
flags: DELTA_UNIT pts 00:00:01.650197746 dts 00:00:01.655823246
flags: DELTA_UNIT pts 00:00:01.690197725 dts 00:00:01.692281451
flags: DELTA_UNIT pts 00:00:01.730197717 dts 00:00:01.732738014
flags: DELTA_UNIT pts 00:00:01.770197713 dts 00:00:01.772335278
flags: DELTA_UNIT pts 00:00:01.810197712 dts 00:00:01.812399785
flags: DELTA_UNIT pts 00:00:01.850197712 dts 00:00:01.852435838
flags: DELTA_UNIT pts 00:00:01.890197712 dts 00:00:01.893547681
flags: DELTA_UNIT pts 00:00:01.930197712 dts 00:00:01.932689090
flags: DELTA_UNIT pts 00:00:01.970197712 dts 00:00:01.972465099
flags: DELTA_UNIT pts 00:00:02.010197712 dts 00:00:02.012648985
flags: DELTA_UNIT pts 00:00:02.050197712 dts 00:00:02.053152003
flags: DELTA_UNIT pts 00:00:02.090197712 dts 00:00:02.093963961
flags: HEADER | DELTA_UNIT pts 00:00:02.130197711 dts 00:00:02.130642509
flags: (empty) pts 99:99:99.999999999 dts 00:00:02.144220304
flags: DELTA_UNIT pts 00:00:02.170197711 dts 00:00:02.172837551
(...)

I found a workaround (that I haven't yet figured out how to enable in the rust bindings) which is to do set_pts_interpolation(h264parser, true). That seems to fix the issue.

I haven't bee able to grab a raw rtp or h264 stream to a file that I can then reproduce this at will from. Let me know what kind of extra information would help. If any one the gstreamer developers wants one of these cameras to test I can also have it shipped to you. There seems to be a recent wave of cheap 2-4MP cameras with h264 (and supposedly h265 as well). They're probably all using the same encoder chips so this is likely to be a common issue."""
            ), make_comment(
                author='pedro@pedrocr.net',
                creation_time=make_datetime('2018-07-09 08:40:35', 'AEST'),
                text="""Thanks to Sebastian I've been able to enable interpolation in the rust code. Here's the result:

flags: DISCONT | HEADER | DELTA_UNIT pts 00:00:00.261462431 dts 00:00:00.263152518
flags: (empty) pts 00:00:00.274878421 dts 00:00:00.274878421
flags: DELTA_UNIT pts 00:00:00.301462431 dts 00:00:00.304715236
flags: DELTA_UNIT pts 00:00:00.341462431 dts 00:00:00.343545874
flags: DELTA_UNIT pts 00:00:00.381462431 dts 00:00:00.383407293
flags: DELTA_UNIT pts 00:00:00.421462431 dts 00:00:00.423277810
flags: DELTA_UNIT pts 00:00:00.461462431 dts 00:00:00.463337810
flags: DELTA_UNIT pts 00:00:00.501462431 dts 00:00:00.503497098
flags: DELTA_UNIT pts 00:00:00.541462431 dts 00:00:00.547031050
flags: DELTA_UNIT pts 00:00:00.581462431 dts 00:00:00.583345253
flags: DELTA_UNIT pts 00:00:00.621462431 dts 00:00:00.623342326
flags: DELTA_UNIT pts 00:00:00.661462431 dts 00:00:00.663315179
flags: DELTA_UNIT pts 00:00:00.701462431 dts 00:00:00.703462505
flags: DELTA_UNIT pts 00:00:00.741462431 dts 00:00:00.743970603
flags: DELTA_UNIT pts 00:00:00.781462431 dts 00:00:00.783335348
flags: DELTA_UNIT pts 00:00:00.821462431 dts 00:00:00.823293648
flags: DELTA_UNIT pts 00:00:00.861462431 dts 00:00:00.863326481
flags: DELTA_UNIT pts 00:00:00.901462431 dts 00:00:00.903385915
flags: DELTA_UNIT pts 00:00:00.941462431 dts 00:00:00.943352480
flags: DELTA_UNIT pts 00:00:00.981462431 dts 00:00:00.983493392
flags: DELTA_UNIT pts 00:00:01.021462431 dts 00:00:01.023313135
flags: DELTA_UNIT pts 00:00:01.061462431 dts 00:00:01.063276491
flags: DELTA_UNIT pts 00:00:01.101462431 dts 00:00:01.103391895
flags: DELTA_UNIT pts 00:00:01.141462431 dts 00:00:01.143395879
flags: DELTA_UNIT pts 00:00:01.181462431 dts 00:00:01.183290100
flags: DELTA_UNIT pts 00:00:01.221462431 dts 00:00:01.223348690
flags: DELTA_UNIT pts 00:00:01.261462431 dts 00:00:01.263373463
flags: DELTA_UNIT pts 00:00:01.301462431 dts 00:00:01.304241082
flags: DELTA_UNIT pts 00:00:01.341462431 dts 00:00:01.343400175
flags: DELTA_UNIT pts 00:00:01.381462431 dts 00:00:01.383383311
flags: DELTA_UNIT pts 00:00:01.421462431 dts 00:00:01.423801744
flags: DELTA_UNIT pts 00:00:01.461462431 dts 00:00:01.463330093
flags: DELTA_UNIT pts 00:00:01.501462431 dts 00:00:01.503430315
flags: DELTA_UNIT pts 00:00:01.541462431 dts 00:00:01.543366482
flags: DELTA_UNIT pts 00:00:01.581462431 dts 00:00:01.584078245
flags: DELTA_UNIT pts 00:00:01.621462431 dts 00:00:01.623450738
flags: DELTA_UNIT pts 00:00:01.661462431 dts 00:00:01.663463921
flags: DELTA_UNIT pts 00:00:01.701462431 dts 00:00:01.703523269
flags: DELTA_UNIT pts 00:00:01.741462431 dts 00:00:01.743462150
flags: DELTA_UNIT pts 00:00:01.781462431 dts 00:00:01.783394769
flags: DELTA_UNIT pts 00:00:01.821462431 dts 00:00:01.823449308
flags: DELTA_UNIT pts 00:00:01.861462431 dts 00:00:01.863591417
flags: DELTA_UNIT pts 00:00:01.901462431 dts 00:00:01.903516378
flags: DELTA_UNIT pts 00:00:01.941462431 dts 00:00:01.943498889
flags: DELTA_UNIT pts 00:00:01.981462431 dts 00:00:01.983434396
flags: DELTA_UNIT pts 00:00:02.021462431 dts 00:00:02.023455491
flags: DELTA_UNIT pts 00:00:02.061462431 dts 00:00:02.063458245
flags: DELTA_UNIT pts 00:00:02.101462431 dts 00:00:02.103519384
flags: DELTA_UNIT pts 00:00:02.141462431 dts 00:00:02.143711913
flags: DELTA_UNIT pts 00:00:02.181462431 dts 00:00:02.183479301
flags: DELTA_UNIT pts 00:00:02.221462431 dts 00:00:02.224441922
flags: HEADER | DELTA_UNIT pts 00:00:02.261462431 dts 00:00:02.261688725
flags: (empty) pts 00:00:02.281857150 dts 00:00:02.281857150
flags: DELTA_UNIT pts 00:00:02.301463400 dts 00:00:02.305059963
flags: DELTA_UNIT pts 00:00:02.341464362 dts 00:00:02.343595702
flags: DELTA_UNIT pts 00:00:02.381465316 dts 00:00:02.383605776
flags: DELTA_UNIT pts 00:00:02.421466262 dts 00:00:02.424238691
flags: DELTA_UNIT pts 00:00:02.461467201 dts 00:00:02.463484227
flags: DELTA_UNIT pts 00:00:02.501468132 dts 00:00:02.503617245
flags: DELTA_UNIT pts 00:00:02.541469056 dts 00:00:02.543589840
flags: DELTA_UNIT pts 00:00:02.581469972 dts 00:00:02.583406674
flags: DELTA_UNIT pts 00:00:02.621470881 dts 00:00:02.623393752
flags: DELTA_UNIT pts 00:00:02.661471783 dts 00:00:02.663436755
flags: DELTA_UNIT pts 00:00:02.701472678 dts 00:00:02.703473183
flags: DELTA_UNIT pts 00:00:02.741473565 dts 00:00:02.743609305
flags: DELTA_UNIT pts 00:00:02.781474445 dts 00:00:02.783423319
flags: DELTA_UNIT pts 00:00:02.821475318 dts 00:00:02.823385178
flags: DELTA_UNIT pts 00:00:02.861476184 dts 00:00:02.863378073
flags: DELTA_UNIT pts 00:00:02.901477043 dts 00:00:02.903474673
flags: DELTA_UNIT pts 00:00:02.941477896 dts 00:00:02.943363251
flags: DELTA_UNIT pts 00:00:02.981478742 dts 00:00:02.983381692
flags: DELTA_UNIT pts 00:00:03.021479581 dts 00:00:03.023401156
flags: DELTA_UNIT pts 00:00:03.061480413 dts 00:00:03.063378192
flags: DELTA_UNIT pts 00:00:03.101481239 dts 00:00:03.103433214
flags: DELTA_UNIT pts 00:00:03.141482058 dts 00:00:03.143495895
flags: DELTA_UNIT pts 00:00:03.181482870 dts 00:00:03.183510019
flags: DELTA_UNIT pts 00:00:03.221483676 dts 00:00:03.223407398
flags: DELTA_UNIT pts 00:00:03.261484475 dts 00:00:03.263987352
flags: DELTA_UNIT pts 00:00:03.301485268 dts 00:00:03.304303148
flags: DELTA_UNIT pts 00:00:03.341486055 dts 00:00:03.343456637
flags: DELTA_UNIT pts 00:00:03.381486835 dts 00:00:03.383475190
flags: DELTA_UNIT pts 00:00:03.421487609 dts 00:00:03.423318451
flags: DELTA_UNIT pts 00:00:03.461488377 dts 00:00:03.463354330
flags: DELTA_UNIT pts 00:00:03.501489139 dts 00:00:03.503489793
flags: DELTA_UNIT pts 00:00:03.541489895 dts 00:00:03.543482092
flags: DELTA_UNIT pts 00:00:03.581490645 dts 00:00:03.583399527
flags: DELTA_UNIT pts 00:00:03.621491389 dts 00:00:03.624278414
flags: DELTA_UNIT pts 00:00:03.661492127 dts 00:00:03.663512520
flags: DELTA_UNIT pts 00:00:03.701492859 dts 00:00:03.703616479
flags: DELTA_UNIT pts 00:00:03.741493585 dts 00:00:03.743561367
flags: DELTA_UNIT pts 00:00:03.781494305 dts 00:00:03.783531420
flags: DELTA_UNIT pts 00:00:03.821495019 dts 00:00:03.823463372
flags: DELTA_UNIT pts 00:00:03.861495728 dts 00:00:03.863532045
flags: DELTA_UNIT pts 00:00:03.901496431 dts 00:00:03.903624667
flags: DELTA_UNIT pts 00:00:03.941497128 dts 00:00:03.943561047
flags: DELTA_UNIT pts 00:00:03.981497820 dts 00:00:03.983542823
flags: DELTA_UNIT pts 00:00:04.021498506 dts 00:00:04.023509947
flags: DELTA_UNIT pts 00:00:04.061499187 dts 00:00:04.063684982
flags: DELTA_UNIT pts 00:00:04.101499862 dts 00:00:04.104242408
flags: DELTA_UNIT pts 00:00:04.141500532 dts 00:00:04.143758055
flags: DELTA_UNIT pts 00:00:04.181501197 dts 00:00:04.183498000
flags: DELTA_UNIT pts 00:00:04.221501856 dts 00:00:04.224864948
flags: HEADER | DELTA_UNIT pts 00:00:04.261502510 dts 00:00:04.261748079
flags: (empty) pts 00:00:04.275363628 dts 00:00:04.275363628
(...)

Seems like interpolation just sets pts=dts. The end result is a fully playable file with apparently no issues."""
            ), make_comment(
                author='pedro@pedrocr.net',
                creation_time=make_datetime('2018-07-09 08:52:13', 'AEST'),
                text="""Although the end result is a playable file it seems there are still some issues. matroskamux generates a playable file with errors:

$ vlc test.mkv
VLC media player 3.0.2 Vetinari (revision 3.0.2-0-gd7b653cf14)
[h264 @ 0x7fd7ccc35320] no frame!
[h264 @ 0x7fd7ccc47040] no frame!
[h264 @ 0x7fd7ccc8cdc0] no frame!
[h264 @ 0x7fd7ccca8fe0] no frame!
[h264 @ 0x7fd7cccc52e0] no frame!
[h264 @ 0x7fd7ccc35320] no frame!
[h264 @ 0x7fd7ccc47040] no frame!
[h264 @ 0x7fd7ccc8cdc0] no frame!
[h264 @ 0x7fd7ccca8fe0] no frame!
(end of stream)

But with mp4mux the file is broken:

$ vlc test.mp4
VLC media player 3.0.2 Vetinari (revision 3.0.2-0-gd7b653cf14)
[mov,mp4,m4a,3gp,3g2,mj2 @ 0x7fa7b8c5ed60] moov atom not found
[mov,mp4,m4a,3gp,3g2,mj2 @ 0x7fa770c12060] moov atom not found
[00007fa770c18260] avformat demux error: Could not open /home/pedrocr/data/core/Projects/vsurv/test.mp4: Unknown error 1094995529

Let me know if there's anything else I can provide or if this is actually a different bug and I'll open a new one."""
            )
        ]
    )

bug756073 = make_bug(756073,
        creation_time=make_datetime('2015-10-05 20:04:02', 'AEDT'),
        last_change_time=make_datetime('2015-10-05 21:58:12', 'AEDT'),
        version='1.6.0',
        status='NEW',
        severity='major',
        target_milestone='git master',
        keywords=[],
        creator='radegast13@gmail.com',
        summary='vtdec: "Error decoding frame -12909" on iOS',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['radegast13@gmail.com', 'slomo@coaxion.net'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='radegast13@gmail.com',
                creation_time=make_datetime('2015-10-05 20:04:02', 'AEDT'),
                text="""I have cloned this repo http://cgit.freedesktop.org/~slomo/gst-sdk-tutorials/tree/gst-sdk/tutorials/xcode%20iOS and run Tutorial 5 in XCode 7. Wowza rtsp sample video works fine. But my IP camera shows only first frame of video and fire PAUSED state.
Similar IP camera rtsp links works fine in VLC and Android version of GStreamer.

I have implemented tutorial sample in my application and have got similar behavior.
Here is log of GStreamer http://pastebin.com/9N54vQUb captured from my application.

Logs have a lot of

"0:01:05.669427000 [335m  178[00m 0x183559e0 [32;01mFIXME  [00m [00;01m                 bin gstbin.c:4070:gboolean gst_bin_query(GstElement *, GstQuery *):[00m implement duration caching in GstBin again"

and

"733:gst_vtdec_session_output_callback:<vtdec0>[00m Error decoding frame -12909
0:00:09.393987000 [336m 5105[00m 0x159570a0 [31;01mERROR  [00m [00m"

messages.

How can I fix it?

Thanks for advance!"""
            )
        ]
    )

bug768079 = make_bug(768079,
        creation_time=make_datetime('2016-06-27 13:18:40', 'AEST'),
        last_change_time=make_datetime('2018-09-27 21:28:25', 'AEST'),
        version='git master',
        status='NEW',
        severity='enhancement',
        target_milestone='git master',
        keywords=[],
        creator='chul0812@gmail.com',
        summary='waylandsink: add support wayland presentation time interface',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['daniel@fooishbar.org', 'gkiagia@tolabaki.gr', 'jared.hu@nxp.com', 'nicolas.dufresne@usherbroke.ca', 'nicolas@ndufresne.ca', 'olivier.crete@ocrete.ca', 't.i.m@zen.co.uk'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='chul0812@gmail.com',
                creation_time=make_datetime('2016-06-27 13:18:40', 'AEST'),
                text="""I bring comments about this tasks and wrap writer's name with angle brackets, sorry for the poor readability.

Waylandsink was handled by George Kiagiadakis and he had written presentation time interface codes for the demo, but the interface has been changed and settled down as a stable protocol.

I was starting it based on George's work (http://cgit.collabora.com/git/user/gkiagia/gst-plugins-bad.git/log/?h=demo), removing presentation queue and considering display stack delay.
It was predicting latency at display stack from wl_commit/damage/attach to frame presence and Pekka Paalanen(pq) advised that would not estimate the delay from wl_surface_commit() to display.

(it's part of comments)
<pq> wonchul, if you are trying to estimate the delay from wl_surface_commit() to display, and you don't sync the time you call commit() to the incoming events, that's going to be a lot less accurate.
<pq> 11:11:07> no, I literally meant replacing the queueing protocol calls with a queue implementation in the sink, so you don't use the queueing protocol anymore, but rely only on the feedback protocol to trigger attach+commits from the queue.
<pq> 11:12:27> the queue being a timestamp-ordered list of frame, just like in the weston implementation.

So, the way estimating the delay from wayland is not much accurate.
I turned to add a queue holding buffers before doing render() in the waylandsink


<Olivier Crête>
I'm a bit concerned about adding a queue in the sink that would increase the latency unnecessarily. I wonder if this could be done while queueing around 1 buffer there in normal streaming. Are we talking about queuing the actual frames or just information about the frames?


<Wonchul Lee>
I've queued reference of frames and tried to render based on the wayland presentation clock.
It could bring some delay depending on specific contents by adding a queue in the sink, It's not clear to me what specific factor cause delay yet, but yes, it would increase the latency at the moment.

The idea was disabling clock synchronization in gstbasesink and rendering(wayland commit/damage/attach) frames based on the calibrated wayland clock. I pushed the reference of gstbuffer to the queue and set the async clock callback to request render at a right time, and then rendered or dropped it depending on the adjusted timestamp.
This changes have issues that adjusted timestamp what requested to render is getting late than expected and it could cause dropping most of the frames at some cases since the adjusted timestamp was always late.
So I'm referring audiobasesink to adjust clock synchronization for the frames with wayland clock.


<Olivier Crête>
This work has two separate goals:

When the video has a different framerate than the display framerate, it should drops frames more or less evenly, so if you need to display 4 out of 5 frames, it should be something like 1,2,3,4,6,7,8,9,11,... Or if you need to display 30/60 frames it should display 1,3,5,7,9, etc .. Currently, GstBaseSink is not very clever about that.
And we have to be careful as this can be also caused by the compositor not being able to keep up. It's not because the display can do 60fps that the compositor is actually able to produce 60 new frames, it could be limited to a lower number, so we'll also have to make sure we're protected against that.
We want to guess the latency added by the display stack. The current GStreamer video sinks more or less assume that a buffer is rendered immediately when the render() vmethod returns, but this is not really how current display hardware work. Especially when you have double or triple buffering. So we want to know how much in advance to submit the buffer, but not too early to not display it one interval too early.
I just asked @nicolas a quick question about how he though we should do this, then we spent two hours whiteboarding ideas about this and we've barely been able to define the problem.

Here are some ideas we bounced around:

After submitting one frame (the first frame? the preroll frame?), we can have an idea of the upper bound of the latency for the live pipeline case. It should be the time between the moment a frame was submitted and when it was finally rendered + the "refresh". We can probably delay sending the async-done until the presented event of the first frame has arrived.
For the non-live case, we can probably find a way to submit the frame as early as possible before the next. Finding that time is the tricky part I think
@wonchul: could you summarize the different things your tried, what were the hypothesis and what were the results? It's important to keep these kinds of records for the Tax R&D filings (and so we can keep up with your work).

@pq or @daniels:

what is the logic behind the seq field, how do you expect it can be used? Do you know any example where it is used?
I'm also not sure how we can detect the case where the compositor cannot keep up? Or is the compositor is gnome-shell and has a gc that makes it miss a couple frames for no good reason?
From the info is the presented event (or any other way), is there a way we can evaluate when is the latest we can submit a buffer to have it arrive in time for a specific refresh? Or do we have to try and then do some kind of search to find what those deadlines are in practice?


<Pekka Paalanen>
seq field of wp_presentation_feedback.presented event:

No examples of use, I don't think. I didn't originally considerer it as needed, but it was added to allow implementing GLX_OML_sync_control on top of it. I do not think we should generally depend on seq unless you specifically care about the refresh count instead of timings. My intention with the design was that new code can work better with timestamps, while old code you don't want to port to timestamps could use seq as it has always done. Timestamps are "accurate", while seq may have been estimated from a clock in the kernel and may change its rate or may not have a constant rate at all.

seq comes from a time, when display refresh was a known guaranteed constant frequency, and you could use it as a clock by simply counting cycles. I believe all timing-sensitive X11 apps have been written with this assumption. But it is no longer exactly true, it has caveats (hard to maintain across video mode switches or display suspends, lacking hardware support, etc.), and with new display tech it will become even less true (variable refresh rate, self-refresh panels, ...).

seq is not guaranteed to be provided, it may be zero depending on the graphics stack used by the compositor. I'm also not sure what it means if you don't have both VSYNC and HW_COMPLETION in flags

The timestamp OTOH is always provided, but it may have some caveats which should be indicated by unset bits in flags.

Compositor not keeping up:

Maybe you could use the tv + refresh from presented event to guess when the compositor should be presenting your frame, and compare afterwards with what actually happened?

I can't really think of a good way to know if the compositor cannot keep up or why it cannot keep up. Hickups can happen and the compositor probably won't know why either. All I can say is collect statistics and analyze then over time. This might be a topic for further investigations, but to get more information about which steps take too much time we need some kernel support (explicit fencing) that is being developed, and make the compositor use that information.

Only hand-waving, sorry.

Finding the deadline:

I don't think there is a way to know really, and also the compositor might be adjusting its own schedules, so it might be variable.

The way I imaged it is that from presented event you compute the time of the next possible presentation, and if you want to hit that, submit a frame ASAP. This should get you just below one display-frame-cycle latency in any case, if your rendering is already complete.

If we really need the deadline, that would call for extending the protocol, so that the compositor could tell you when the deadline is. The compositor chooses the deadline based on how fast it thinks it can do a composition and hit the right vblank.


<Wonchul Lee>
About the latency, I tried to get latency added by the display stack from the wl commit/damage/attach to the present frame. It's a variable delay depending on the situation as pq mentioned before and could disturb targeting next present. The way we could assume optimal latency by accumulating it and observe a gap by the presentation feedback, maybe not always reliable.

I tried to synchronize GStreamer clock time with presentation feedback to render a frame on time and added a queue in GstWaylandSink to request render on each presentation feedback if there's a frame on time, similar to what George did. It's not well fit with GstBaseSink though, and GstWaylandSink needs to disable BaseSink time synchronization and computing itself. I faced unexpected underflow (consistently increasing delay) when playing with mpegts stream, so It also needs proper QOS handling to prevent underflow.

I would be good to get reliable latency from the display stack to make use of it when synchronizing presenting time whether computing it GstWaylandSink itself or not, there's a latency what we're missing anyway, though I'm not sure it's feasible.


<Pekka Paalanen>
@wonchul btw. what do you mean when you say "synchronize GStreamer clock time with presentation feedback"?

Does it mean something else than looking at what clock is advertised by wp_presentation.clock_id and then synchronizing GStreamer clock with clock_gettime() using the given clock id? Or does synchronizing mean something else than being able to convert a timestamp from one clock domain to the other domain?


<Nicolas Dufresne>
@pq I would need some clarification about submitting frame ASAP. If we blindly do that, frames will get displayed too soon on screen (in playback, decoders are much faster then the expected render speed). In GStreamer, we have infrastructure to wait until the moment is right. The logic (simplified) is to wait for the right moment minus the "currently expected" render latency, and submit. This is in playback case of course, and is to ensure the best possible A/V sync. In that case we expect the presentation information to be helpful in constantly correcting that moment. What we miss, is some semantic, as just blindly obey to the computed render delay of last frames does not seem like best idea. We expected to be able to calculate, or estimate, a submission window that will (most of the time) hit the screen at an estimated time.

For the live case, we're still quite screwed. Nothing seems to improve our situation. We need at start to pick a latency, and if later find that latency was too small (the latency is the window in which we are able to adapt), we end-up screwing up the audio (a glitch) to increase that latency window. So again, some semantic that we could use to calculate a pessimistic latency from the first presentation report would be nice.


<Olivier Crête>
I think that in the live case you can probably keep a 1 frame queue at the sink, so when a new frame arrives, you can decide if you want to present the queued one at the next refresh or replace it with a new one. Then the thread that talks to the compositor (and gets the events, etc), can pick the buffers from the "queue" to send to the compositor.


<Nicolas Dufresne>
Ok, that make sense for non-live. Would be nice to document the intended use, that was far from obvious. We keep thinking we need to look at the number, but we don't understand at first the the moment we get called back is important. You seem to assume that we can "pick" a frame, like if the sink was pulling whatever it wants randomly, that unfortunately not how things works. We can though introduce a small queue (some late queue) so we only start blocking upstream when that queue is full. And it would help making decisions

For live it's much more complex. The entire story about declared latency is because if we don't declare any latency, that queue will always be empty. Worst case, the report will always tell use that we have displayed the frame late. I'm quite sure you told me that the render pipeline can have multiple step, where submitting frame 1 2 3 at 1 blank distance, will render on blank 3 4 5 with effectively 3 blank latency. That latency is what we need to report for proper A/V sink in live pipeline, and changing is to be done with care as it breaks the audio. That we need some ideas, cause right now we have no clue."""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2016-10-01 04:05:38', 'AEST'),
                text="""*** Bug 768080 has been marked as a duplicate of this bug. ***"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2016-10-01 06:04:49', 'AEST'),
                text="""I'm gathering code for that in a branch (careful, this branch is rebased often).

https://git.collabora.com/cgit/user/nicolas/gst-plugins-bad.git/log/?h=wayland-presentation

Arun and I have been tracing the behaviour without any custom synchronization. So far we got interesting results. Here's a very meaningful graph

  http://imgur.com/a/7VijX

On this graph, we try to play 30fps video on an output at some unprecise 60Hz. We can observe that the refresh rate is in fact slower, hence it runs later and later until the submission hits the previous vblank. So most frame are displayed on 2 blank, and once in a while it only renders on 1 blank.

This zig-zag result is exactly what we expect and the behaviour is acceptable. What is not though is the jumps you may notice close to the edges, or whenever the scheduler decides to kick at the wrong moment. This is noticeable in this graph by those vertical line that looks like glitches. For that reason, we came to the same conclusion as George had previously experiment in it's demo branch.

  https://git.collabora.com/cgit/user/gkiagia/gst-plugins-bad.git/commit/?h=demo

To ensure smoothness, we need to control the submission time to ensure we do actually chose a vblank instead of leaving it to our luck. The simplest mechanism is to make sure the presented callback keep triggering and to only draw on this callback. This callback represent the sooner point it time for which client will hit the next blank. This is though not the most efficient, so a step up would be to predict the appropriate presented callback for the current frame base on the last presented callback and the refresh (or a refresh calculated from the sequence number if you are precision hungry).

From there, we'll still be displaying the frame a bit far from the presentation time. That's because there is a certain latency possibly introduced by the compositor and/or the driver/hw. We don't know this latency in advance, so we have no choice but to figure-out at run time. I'll be experimenting few estimation method, ideally a method that gives a value out of the preroll phase, and then could suggest the app to update the latency later when a more accurate value is available. I'll also try and avoid implementing our own sync for now, but try and be smarter then just replacing GstBuffer pointer like we do.

If someone knows if there is standard for this. As you can see on the graph, it produce a zigzag when the video rate is different from the display rate. We can adjust latency to center this zig-zag around 0, or we can lower that zigzag so the smallest delay tends to zero. I'm not sure what is best, my intuition tells me that using center is more accurate even if it means that at some moment the frames are displayed too soon, and other too late."""
            ), make_comment(
                author='gkiagia@tolabaki.gr',
                creation_time=make_datetime('2017-03-10 03:45:46', 'AEDT'),
                text="""I am looking a bit into this.

First of all, I have rebased again on top of current master (conflicted with dmabuf stuff - fixed):
https://git.collabora.com/cgit/user/gkiagia/gst-plugins-bad.git/log/?h=wayland-presentation

Some comments:

* I don't particularly like doing the streaming thread throttling in prepare() (with that g_cond_wait()). It doesn't make it clear where the throttling happens, I was confused for quite some time. It will probably also make it hard to implement unlock() and waiting for preroll.

* Numbers look confusing. It looks like the render time of the next buffer is always smaller than the last presentation time by a factor of about 3 seconds in my tests:

base_time 285:57:44.954398746 prepared_buffer_pts 0:00:00.149999999 presentation_time 285:57:45.033256712 refresh_duration 0:00:00.016666666 render_time 285:57:41.282301844

It looks to me as if all buffers are actually very late, but synchronization happens nevertheless because of the g_cond_wait in prepare(). Maybe I am missing something, I'll continue looking...

* There is a g_cond_signal() in show_frame() which doesn't make any sense, as show_frame() runs on the same thread as prepare()."""
            ), make_comment(
                author='daniel@fooishbar.org',
                creation_time=make_datetime('2017-03-10 03:46:59', 'AEDT'),
                text="""(In reply to George Kiagiadakis from comment #3)
> * Numbers look confusing. It looks like the render time of the next buffer
> is always smaller than the last presentation time by a factor of about 3
> seconds in my tests:
>
> base_time 285:57:44.954398746 prepared_buffer_pts 0:00:00.149999999
> presentation_time 285:57:45.033256712 refresh_duration 0:00:00.016666666
> render_time 285:57:41.282301844

Mismatch in which clock was used perhaps?"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2017-03-10 12:45:53', 'AEDT'),
                text="""The traces are not self explanatory, hence need work indeed. Remember this is early work.

Using prepare() is very important, since the render function is synchronised. It would prevent you from doing any kind of adaptation. I'll probably be able to dive again in this work next week, and will probably provide initial support for preroll and unlock.

If I remember correctly, the sync happens on the clock thread (which is also running the WL queue, for race free handling). Prepare just wait for a slot to be free in the two buffer queue. You can unlock that easily."""
            ), make_comment(
                author='jared.hu@nxp.com',
                creation_time=make_datetime('2018-09-27 15:43:18', 'AEST'),
                text="""Hi Nicolas,

What is the progress of this enhancement? We need this feature to fix a waylandsink hang issue caused by the patch in the ticket:
https://bugzilla.gnome.org/show_bug.cgi?id=794793

best regards
Jared"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2018-09-27 21:28:25', 'AEST'),
                text="""I'm not actively working on this at the moment."""
            )
        ]
    )

bug769268 = make_bug(769268,
        creation_time=make_datetime('2016-07-28 23:24:42', 'AEST'),
        last_change_time=make_datetime('2017-04-19 22:10:17', 'AEST'),
        version='1.8.2',
        status='NEW',
        severity='major',
        target_milestone='git master',
        keywords=[],
        creator='kapsa@man.poznan.pl',
        summary='nvenc segfault after setting wrong width or height',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['slomo@coaxion.net', 't.i.m@zen.co.uk', 'vincent.penquerch@collabora.co.uk', 'ystreet00@gmail.com'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-28 23:24:42', 'AEST'),
                text="""After setting odd width or height nvenc will crash.

e.g.
set caps width: 1855, height 1060
set pipline to GST_STATE_PLAYING
nvenc will crash

change width to even value (e.g. 1854) and nvenv will work."""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2016-07-28 23:33:25', 'AEST'),
                text="""Can you provide a backtrace of all threads during the crash, a debug log and the exact versions of GStreamer, the hardware and drivers?"""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-29 17:20:50', 'AEST'),
                attachment_id=332334,
                text="""Created attachment 332334
GST_DEBUG=*:5 (nvenc segfault)

WARN nvenc gstnvh264enc.c:571:gst_nv_h264_enc_initialize_encoder:<nvh264enc0>[00m error: Failed to init encoder: 8

ERROR nvenc gstnvbaseenc.c:919:gst_nv_base_enc_set_format:<nvh264enc0>[00m Subclass failed to reconfigure encoder"""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-29 17:36:09', 'AEST'),
                text="""PC spec:
Intel® Core™ i7-4790K
GeForce GTX 750 Ti/PCIe/SSE2
7,7 GiB
SSD 256

drivers:
nvidia 361.42
gstreamer 1.8.2 from default Ubuntu 16.04 repo
nvenc builded from git://anongit.freedesktop.org/gstreamer/gst-plugins-bad (tag 1.8.2)
nvidia_video_sdk_6.0.1
nvidia cuda 7.5.18-0ubuntu1 from default Ubuntu 16.04 repo

Linux wk 4.4.0-31-generic #50-Ubuntu SMP Wed Jul 13 00:07:12 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2016-07-29 18:03:37', 'AEST'),
                text="""Please also provide a backtrace of all threads when it crashed, and a debug log with level 6"""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-29 19:15:00', 'AEST'),
                attachment_id=332337,
                text="""Created attachment 332337
GST_DEBUG=*:6 (nvenc segfault)"""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-29 19:15:46', 'AEST'),
                attachment_id=332338,
                text="""Created attachment 332338
gdb thread apply all backtrace"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2016-07-29 19:24:29', 'AEST'),
                text="""Can you get another backtrace after installing debug symbols for glib, gstreamer, gst-plugins-base, etc?"""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-29 19:34:30', 'AEST'),
                attachment_id=332339,
                text="""Created attachment 332339
gdb thread apply all backtrace  with dbg

installed symbols:
libglib2.0-0-dbg
gstreamer1.0-plugins-*-dbg"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2016-07-29 19:37:45', 'AEST'),
                text="""The debug symbols of GStreamer are still missing. On Debian based systems the package is called libgstreamer1.0-0-dbg"""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-07-29 19:41:41', 'AEST'),
                attachment_id=332340,
                text="""Created attachment 332340
gdb thread apply all backtrace with libgstreamer1.0-0-dbg"""
            ), make_comment(
                author='vincent.penquerch@collabora.co.uk',
                creation_time=make_datetime('2016-10-07 22:24:08', 'AEDT'),
                text="""Try installing all the libgst*1.0.0-dbg packages, there might be more."""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2016-10-20 22:09:19', 'AEDT'),
                text="""x264 will also fail with non-even width so we can just say it's not supported."""
            ), make_comment(
                author='kapsa@man.poznan.pl',
                creation_time=make_datetime('2016-10-26 16:40:31', 'AEST'),
                text="""Pipeline like this will crash:

gst-launch-1.0 -e videotestsrc ! video/x-raw,width=1919,height=1080,framerate=30/1 ! videoconvert ! videoscale ! nvh264enc ! h264parse ! queue ! mpegtsmux name=mux ! filesink location=test.ts
Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
Got context from element 'nvh264enc0': gst.gl.GLDisplay=context, gst.gl.GLDisplay=(GstGLDisplay)"\(GstGLDisplayX11\)\ gldisplayx11-0";
Caught SIGSEGV
Spinning.  Please run 'gdb gst-launch-1.0 2882' to continue debugging, Ctrl-C to quit, or Ctrl-\ to dump core.

x264 returns error:

gst-launch-1.0 -e videotestsrc ! video/x-raw,width=1919,height=1080,framerate=30/1 ! videoconvert ! videoscale ! x264enc ! h264parse ! queue ! mpegtsmux name=mux ! filesink location=test.ts
Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
ERROR: from element /GstPipeline:pipeline0/GstX264Enc:x264enc0: Can not initialize x264 encoder.
Additional debug info:
gstx264enc.c(1587): gst_x264_enc_init_encoder (): /GstPipeline:pipeline0/GstX264Enc:x264enc0
ERROR: pipeline doesn't want to preroll.
Setting pipeline to NULL ...
Freeing pipeline ..."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2017-04-19 22:10:17', 'AEST'),
                attachment_id=350062,
                text="""Created attachment 350062
nvenc: only accept widths and height that are a multiple of 2

Can't reproduce the crash, but we could do something like this as a quick fix.

Not sure if that's better or worse than erroring out though. It has the advantage that it's more correct, and theoretically elements before the encoder could automatically crop/scale as required. In practice I can't find any elements which actually support this yet though."""
            )
        ]
    )

bug792232 = make_bug(792232,
        creation_time=make_datetime('2018-01-05 19:11:56', 'AEDT'),
        last_change_time=make_datetime('2018-01-16 18:38:53', 'AEDT'),
        version='git master',
        status='NEW',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='jun.xie@samsung.com',
        summary='hlsdemux: setup new stream if current variant stream becomes invalid and is removed from master playlist',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=[],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='jun.xie@samsung.com',
                creation_time=make_datetime('2018-01-05 19:11:56', 'AEDT'),
                text="""During updating manifest, current variant stream is checked for validity.
If it becomes invalid, master playlist is updated and old variant streams are compared new variant stream.

Currently, only a warning log is shown.
>hlsdemux gsthlsdemux.c:1288:gst_hls_demux_update_variant_playlist:[00m Unable to match current playlist

I think we need to :
1) reset current variant,
2) setup new stream."""
            ), make_comment(
                author='jun.xie@samsung.com',
                creation_time=make_datetime('2018-01-05 20:22:03', 'AEDT'),
                attachment_id=366361,
                text="""Created attachment 366361
[1/2] reset current variant if old one becomes invalid and is removed from master playlist"""
            ), make_comment(
                author='jun.xie@samsung.com',
                creation_time=make_datetime('2018-01-05 20:24:15', 'AEDT'),
                attachment_id=366362,
                text="""Created attachment 366362
[1/2] reset current variant if old one becomes invalid and is removed from master playlist"""
            ), make_comment(
                author='jun.xie@samsung.com',
                creation_time=make_datetime('2018-01-05 20:32:26', 'AEDT'),
                attachment_id=366363,
                text="""Created attachment 366363
[2/2] setup new stream if current variant stream changed during manifest update"""
            ), make_comment(
                author='jun.xie@samsung.com',
                creation_time=make_datetime('2018-01-16 18:38:53', 'AEDT'),
                text="""please review :)"""
            )
        ]
    )
bug793383 = make_bug(793383,
        creation_time=make_datetime('2018-02-13 00:14:41', 'AEDT'),
        last_change_time=make_datetime('2018-02-14 22:20:50', 'AEDT'),
        version='1.12.3',
        status='NEEDINFO',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='lephihungch@gmail.com',
        summary='Crashing on iOS: [ALAssetRepresentation release]: message sent to deallocated instance, [GstAssetsLibrary .cxx_destruct] XCode 9.2, iOS 11.2 (15C107)',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['lephihungch@gmail.com', 'slomo@coaxion.net', 'thaytan@noraisin.net'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 00:14:41', 'AEDT'),
                text="""I'm stuck on crashing issue: message sent to deallocated instance zombie object with GStreamer tutorial project for iOS with this version of XCode, the app crashes when trying to unref the player or pipeline after playing a AVI file saved in Camera Roll using ALAsset with prefix:
assets-library://asset/asset.AVI?id=ID_OF_AVI_FILE&ext=AVI.

Crash point: typefind:sink -> [GstAssetsLibrary .cxx_destruct]

Crash callStackSymbols

<_NSCallStackArray 0x604000452180>(
0   ???                                 0x00000001226dbf1c 0x0 + 4872584988,
1   Tutorial 5                          0x0000000103211300 main + 0,
2   libobjc.A.dylib                     0x000000010bbb6912 _ZL27object_cxxDestructFromClassP11objc_objectP10objc_class + 127,
3   libobjc.A.dylib                     0x000000010bbc21a4 objc_destructInstance + 124,
4   libobjc.A.dylib                     0x000000010bbc21db object_dispose + 22,
5   AssetsLibrary                       0x00000001088a6acd -[ALAssetsLibrary dealloc] + 98,
6   libobjc.A.dylib                     0x000000010bbcca2e _ZN11objc_object17sidetable_releaseEb + 202,
7   libobjc.A.dylib                     0x000000010bbcd178 _ZN12_GLOBAL__N_119AutoreleasePoolPage3popEPv + 860,
8   libobjc.A.dylib                     0x000000010bbce31b _ZN12_GLOBAL__N_119AutoreleasePoolPage11tls_deallocEPv + 127,
9   libsystem_pthread.dylib             0x000000010ca0f27e _pthread_tsd_cleanup + 534,
10  libsystem_pthread.dylib             0x000000010ca0efbd _pthread_exit + 79,
11  libsystem_pthread.dylib             0x000000010ca0d6cc pthread_sigmask + 0,
12  libsystem_pthread.dylib             0x000000010ca0d56d _pthread_body + 0,
13  libsystem_pthread.dylib             0x000000010ca0cc5d thread_start + 13
)

>https://github.com/sdroege/gst-player
>https://github.com/GStreamer/gst-docs/tree/master/examples/tutorials/xcode%20iOS"""
            ), make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 00:41:42', 'AEDT'),
                text="""Please review the issue or let me know how to solve this issue.
Thanks"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2018-02-13 02:57:04', 'AEDT'),
                text="""Please get a stacktrace with line numbers and also a GStreamer debug log, if possible."""
            ), make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 15:54:53', 'AEDT'),
                text="""Hi,
I saw these GStreamer last lines of debug log:

gst-player gstplayer.c:942:tick_cb:<player0>[00m Position 0:00:03.063842062
0:00:17.840929000 [336m 5693[00m 0x7f83e40a6180 [37mTRACE  [00m [00m          gst-player gstplayer.c:490:gst_player_dispose:<player0>[00m Stopping main thread
0:00:17.841182000 [336m 5693[00m 0x7f83e3330320 [37mTRACE  [00m [00m          gst-player gstplayer.c:2971:gst_player_main:<player0>[00m Stopped main loop
0:00:17.848277000 [336m 5693[00m 0x7f83e3330320 [37mTRACE  [00m [00m          gst-player gstplayer.c:2999:gst_player_main:<player0>[00m Stopped main thread
0:00:17.848504000 [336m 5693[00m 0x7f83e40a6180 [37mTRACE  [00m [00m          gst-player gstplayer.c:513:gst_player_finalize:<player0>[00m Finalizing
2018-02-13 11:47:26.134074+0700 GstPlay[5693:103692] *** -[ALAssetRepresentation release]: message sent to deallocated instance 0x600000a363c0

And the stacktrace I attached in the first comment. It crash in this line after play an AVI load from iOS asset library (If we are not play the video then unref, it's ok but not if we press play):
gst_object_unref(player);

I and there are more information about crash:
GstPlay`-[GstAssetsLibrary .cxx_destruct]:
    0x10e9058ab <+0>:  pushq  %rbp
    0x10e9058ac <+1>:  movq   %rsp, %rbp
    0x10e9058af <+4>:  pushq  %rbx
    0x10e9058b0 <+5>:  pushq  %rax
    0x10e9058b1 <+6>:  movq   %rdi, %rbx
    0x10e9058b4 <+9>:  movq   0x25e3d75(%rip), %rdi     ; GstAssetsLibrary.result
    0x10e9058bb <+16>: addq   %rbx, %rdi
    0x10e9058be <+19>: xorl   %esi, %esi
    0x10e9058c0 <+21>: callq  0x110636ab8               ; symbol stub for: objc_storeStrong
//Crashing line
    0x10e9058c5 <+26>: addq   0x25e3d5c(%rip), %rbx     ; GstAssetsLibrary.asset (avidemux0:sink (14): EXC_BAD_INSTRUCTION (code=EXC_I386_INVOP, subcode=0x0)
//
    0x10e9058cc <+33>: xorl   %esi, %esi
    0x10e9058ce <+35>: movq   %rbx, %rdi
    0x10e9058d1 <+38>: addq   $0x8, %rsp
    0x10e9058d5 <+42>: popq   %rbx
    0x10e9058d6 <+43>: popq   %rbp
    0x10e9058d7 <+44>: jmp    0x110636ab8               ; symbol stub for: objc_storeStrong

(In reply to Sebastian Dröge (slomo) from comment #2)
> Please get a stacktrace with line numbers and also a GStreamer debug log, if
> possible."""
            ), make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 15:57:16', 'AEDT'),
                attachment_id=368285,
                text="""Created attachment 368285
Crashing line

I just summited an image for more information"""
            ), make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 16:01:41', 'AEDT'),
                text="""Please let me know how can I provide more information to help you re-procedure or detecting the issue. Thanks you :("""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2018-02-13 18:41:12', 'AEDT'),
                text="""Not clear what the problem here is, it does not crash directly in the GStreamer code but somewhere in a C++ (?!) destructor.

The code in question would be here in any case:
https://cgit.freedesktop.org/gstreamer/gst-plugins-bad/tree/sys/applemedia/iosassetsrc.m

The destruction of the GstAssetsLibrary seems to be the problem somehow"""
            ), make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 19:12:10', 'AEDT'),
                text="""The tutorial running well in previous version (I cannot remember which version is) but I encountered this issue in current version of XCode with GstPlayer and gst_parse_launch("playbin",NULL).

The code I'm using:

#import <gst/player/player.h>

//ViewDidLoad
render = gst_player_video_overlay_video_renderer_new ((__bridge gpointer)(video_view));
player = gst_player_new (render, NULL);
g_object_set (player, "uri", [uri UTF8String], NULL);

//viewDidDisappear
if (player)
    {
	gst_object_unref(player);
    }

Is it my fault in code flow? What I should do to run the tutorial project with current verion of XCode? Thanks for your help :(

(In reply to Sebastian Dröge (slomo) from comment #6)
> Not clear what the problem here is, it does not crash directly in the
> GStreamer code but somewhere in a C++ (?!) destructor.
>
> The code in question would be here in any case:
> https://cgit.freedesktop.org/gstreamer/gst-plugins-bad/tree/sys/applemedia/
> iosassetsrc.m
>
> The destruction of the GstAssetsLibrary seems to be the problem somehow"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2018-02-13 19:36:42', 'AEDT'),
                text="""It's not a problem in your code. But it would help a lot if you could find out which version was working, and which version did not :)"""
            ), make_comment(
                author='lephihungch@gmail.com',
                creation_time=make_datetime('2018-02-13 21:38:25', 'AEDT'),
                text="""I will try another versions and let you know as soon as possible but hope that you can detect it soon. Thanks for your help. :D."""
            ), make_comment(
                author='thaytan@noraisin.net',
                creation_time=make_datetime('2018-02-14 22:20:50', 'AEDT'),
                text="""I see the same crash with Tutorial 5 after playing a .mov file from the asset library.

The crash is indeed during freeing of the GstAssetsLibrary

* thread #20, name = 'qtdemux0:sink', stop reason = EXC_BAD_ACCESS (code=1, address=0x20)
  * frame #0: 0x00000001842057f4 libobjc.A.dylib`objc_object::release() + 16
    frame #1: 0x0000000100c9370c Tutorial 5`-[GstAssetsLibrary .cxx_destruct](self=0x0000000107838bf0, _cmd=<unavailable>) at iosassetsrc.m:441 [opt]
    frame #2: 0x00000001841e6ef4 libobjc.A.dylib`object_cxxDestructFromClass(objc_object*, objc_class*) + 148
    frame #3: 0x00000001841f4638 libobjc.A.dylib`objc_destructInstance + 88
    frame #4: 0x00000001841f4690 libobjc.A.dylib`object_dispose + 16
    frame #5: 0x00000001960ed99c AssetsLibrary`-[ALAssetsLibrary dealloc] + 100
    frame #6: 0x0000000184206138 libobjc.A.dylib`(anonymous namespace)::AutoreleasePoolPage::pop(void*) + 836
    frame #7: 0x0000000184207e2c libobjc.A.dylib`(anonymous namespace)::AutoreleasePoolPage::tls_dealloc(void*) + 144
    frame #8: 0x0000000184bc75e4 libsystem_pthread.dylib`_pthread_tsd_cleanup + 572
    frame #9: 0x0000000184bc7334 libsystem_pthread.dylib`_pthread_exit + 88
    frame #10: 0x0000000184bc82c0 libsystem_pthread.dylib`_pthread_body + 320
    frame #11: 0x0000000184bc8180 libsystem_pthread.dylib`_pthread_start + 312
    frame #12: 0x0000000184bc6b74 libsystem_pthread.dylib`thread_start + 4"""
            )
        ]
    )

bug679462 = make_bug(679462,
        creation_time=make_datetime('2012-07-06 02:54:20', 'AEST'),
        last_change_time=make_datetime('2016-08-24 21:30:16', 'AEST'),
        version='git master',
        status='NEW',
        severity='enhancement',
        target_milestone='git master',
        keywords=[],
        creator='bugzilla@hadess.net',
        summary='playbin: improve error message when plugins are missing',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['d@deutrino.net', 'slomo@coaxion.net', 't.i.m@zen.co.uk'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='bugzilla@hadess.net',
                creation_time=make_datetime('2012-07-06 02:54:20', 'AEST'),
                text="""Using totem-video-thumbnailer ported to GStreamer 1.0. I get:

TotemVideoThumbnailer-Message: Initialised libraries, about to create video widget
TotemVideoThumbnailer-Message: setting URI file:///home/hadess/example.y4m
TotemVideoThumbnailer-Message: Video widget created
TotemVideoThumbnailer-Message: About to open video file
0:00:00.128743859 [331m13692[00m       0xfe4a10 [31;01mERROR  [00m [00;04m             default totem-gst-helpers.c:61:totem_gst_message_print:[00m message = Your GStreamer installation is missing a plug-in.
0:00:00.128784255 [331m13692[00m       0xfe4a10 [31;01mERROR  [00m [00;04m             default totem-gst-helpers.c:63:totem_gst_message_print:[00m domain  = 2591 (gst-core-error-quark)
0:00:00.128799413 [331m13692[00m       0xfe4a10 [31;01mERROR  [00m [00;04m             default totem-gst-helpers.c:64:totem_gst_message_print:[00m code    = 12
0:00:00.128808122 [331m13692[00m       0xfe4a10 [31;01mERROR  [00m [00;04m             default totem-gst-helpers.c:65:totem_gst_message_print:[00m debug   = gstdecodebin2.c(3686): gst_decode_bin_expose (): /GstPlayBin:play/GstURIDecodeBin:uridecodebin0/GstDecodeBin:decodebin0:
no suitable plugins found
0:00:00.128817996 [331m13692[00m       0xfe4a10 [31;01mERROR  [00m [00;04m             default totem-gst-helpers.c:66:totem_gst_message_print:[00m source  = <decodebin0>
0:00:00.128833565 [331m13692[00m       0xfe4a10 [31;01mERROR  [00m [00;04m             default totem-gst-helpers.c:67:totem_gst_message_print:[00m uri     = (NULL)
totem-video-thumbnailer couldn't open file '/home/hadess/example.y4m'

But in the debug I can see:
0:00:00.439854033 ^[[334m13671^[[00m      0x1794990 ^[[37mDEBUG  ^[[00m ^[[00;43m             GST_BUS gstbus.c:314:gst_bus_post:<bus3>^[[00m [msg 0x179c700] posting on bus error message from element 'decodebin0': GstMessageError, gerror=(GError)NULL, debug=(string)"gstdecodebin2.c\(1726\):\ analyze_new_pad\ \(\):\ /GstPlayBin:play/GstURIDecodeBin:uridecodebin0/GstDecodeBin:decodebin0:\012No\ decoder\ to\ handle\ media\ type\ \'application/x-yuv4mpeg\'";

Which is a much better error message. How come that more helpful message isn't getting posted to the bus?

This is the debug print code:
http://git.gnome.org/browse/totem/tree/src/gst/totem-gst-helpers.c#n36

The changes for GStreamer 1.0 are minimal, see patch in bug 674078"""
            ), make_comment(
                author='bugzilla@hadess.net',
                creation_time=make_datetime('2012-07-06 02:59:43', 'AEST'),
                text="""gst-launch output isn't that much more useful.

$ gst-launch-1.0 playbin uri=file:///home/hadess/example.y4m
Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
Missing element: Y4M demuxer
WARNING: from element /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0: No decoder available for type 'application/x-yuv4mpeg'.
Additional debug info:
gsturidecodebin.c(882): unknown_type_cb (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0
ERROR: from element /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0/GstDecodeBin:decodebin0: Your GStreamer installation is missing a plug-in.
Additional debug info:
gstdecodebin2.c(3686): gst_decode_bin_expose (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0/GstDecodeBin:decodebin0:
no suitable plugins found
ERROR: pipeline doesn't want to preroll.
Setting pipeline to NULL ...
Freeing pipeline ..."""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2014-02-09 02:29:55', 'AEST'),
                text="""You get the useful information in the missing-plugin message if that helps, and as the warning message there.

The problem code-wise is that we post a warning for every type that has no usable plugin... and then if none of all the types had a usable plugin an error message is posted, just that at this point there could be a long list of missing plugins."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2016-08-14 20:17:09', 'AEST'),
                text="""playbin (or at least playbin3 at this point) should intercept any missing plugin messages that were sent, and create one error message that contains a list of missing plugins. It should get the descriptions of the missing plugins (e.g. "MP3 audio decoder") from the description API in pbutils.

Maybe it can also be done by just peeking at caps of unsupported streams in playbin3 (not sure right now how streams for which decoders are missing are exposed/handled in decodebin3/playbin3)."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2016-08-24 21:30:16', 'AEST'),
                text="""*** Bug 770186 has been marked as a duplicate of this bug. ***"""
            )
        ]
    )

bug740784 = make_bug(740784,
        creation_time=make_datetime('2014-11-27 08:11:44', 'AEDT'),
        last_change_time=make_datetime('2017-06-29 06:41:10', 'AEST'),
        version='git master',
        status='ASSIGNED',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='athoik@gmail.com',
        summary='subparse: fails to detect UTF-8 encoding',
        assigned_to='reynaldo@osg.samsung.com',
        cc=['slomo@coaxion.net', 't.i.m@zen.co.uk'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-11-27 08:11:44', 'AEDT'),
                attachment_id=291594,
                text="""Created attachment 291594
sample srt with bom and without bom

Subparse fails to detect UTF-8 encoding when file does not contain BOM.

INFO subparse gstsubparse.c:465:convert_encoding:<subparse0> invalid UTF-8!

But when we set GST_SUBTITLE_ENCODING to UTF-8 subtitles displayed correctly.

INFO subparse gstsubparse.c:465:convert_encoding:<subparse0> invalid UTF-8!
LOG subparse gstsubparse.c:495:convert_encoding:<subparse0> successfully converted 4096 characters from UTF-8 to UTF-8

The exacly same subtitle with BOM (only BOM header is the difference between those files) has no issues."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-11-29 03:35:33', 'AEDT'),
                attachment_id=291730,
                text="""Created attachment 291730
subparse: avoid false negative with g_utf8_validate

Problem is at g_utf8_validate() when passing over
a null terminated string without telling it not
to include it in the test. This patch fixes the
issue for me."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-11-29 03:42:32', 'AEDT'),
                text="""/include it/include the NULL/"""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-11-29 04:32:54', 'AEDT'),
                attachment_id=291730,
                text="""Comment on attachment 291730
subparse: avoid false negative with g_utf8_validate

Aha, as I suspected on IRC then :)

Patch looks generally fine, but I think we should trim off terminating zeroes elsewhere and in general, so maybe early in convert_encoding() or before calling convert_encoding()?

It's called a NUL in this case btw, NULL is for pointers."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-11-29 04:36:39', 'AEDT'),
                text="""And bonus points for a unit test addition to tests/check/elements/subparse.c :)"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-11-29 11:22:36', 'AEDT'),
                text="""+1 for the test addition but I don't think we should trim
NUL terminators elsewhere or at all TBH. Purpose of the
patch is just to pass g_utf8_validate() without having to
conditionally pass -1 for len to avoid the false negative.

My opinion is that changing behavior like trimming all NUL
terminators would, falls outside the scope of the small fix
and I'm actually rather unsure as to whether it wouldn't end
up breaking things elsewhere."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-11-29 11:27:12', 'AEDT'),
                text="""By 'elsewhere' I don't mean everywhere in GStreamer, I mean "a couple of lines earlier"."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-11-29 11:38:47', 'AEDT'),
                attachment_id=291758,
                text="""Created attachment 291758
subparse: avoid false negative with g_utf8_validate

Fix commit message on NULL/NUL"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-11-29 11:43:40', 'AEDT'),
                text="""(In reply to comment #6)
> By 'elsewhere' I don't mean everywhere in GStreamer, I mean "a couple of lines
> earlier".

I understand, but the current patch is not trimming any \0,
just fixing the length passed to g_utf8_validate() If you
insist I can do it though. Just wanted to state my opinion on it."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-11-29 12:04:36', 'AEDT'),
                text="""The question is "what is the correct data here, is it supposed to include a NUL terminator or not here?". The answer to this is "no", that's why I suggested you fix up the data in this location if needed. How sure are you g_convert*() will handle it fine in all circumstances and not do the same thing in some?"""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-11-30 07:10:19', 'AEDT'),
                attachment_id=291805,
                text="""Created attachment 291805
sample srt without bom that fails in the middle of the file

Hello,


Applying the proposed patched doen't fix the problem with the attached sample.

subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line '137'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line '00:13:22,771 --> 00:13:24,764'
subparse gstsubparse.c:843:parse_subrip_time: parsing timestamp '00:13:22,771'
subparse gstsubparse.c:843:parse_subrip_time: parsing timestamp '00:13:24,764'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line 'Ήδη τον ζορίζεις να'
subparse gstsubparse.c:465:convert_encoding:<subparse0> invalid UTF-8!
subparse gstsubparse.c:486:convert_encoding:<subparse0> could not convert string from 'ANSI_X3.4-1968' to UTF-8: Invalid byte sequence in conversion input
subparse gstsubparse.c:495:convert_encoding:<subparse0> successfully converted 4096 characters from ANSI_X3.4-1968 to UTF-8 , using ISO-8859-15 as fallback
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line 'τα κάνει όλα μόνος I?I?I?;'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line ''
subparse gstsubparse.c:1566:handle_buffer:<subparse0> Sending text 'Ήδη τον ζορίζεις να
τα κάνει όλα μόνος I&#x84;I?I?;', 0:13:22.771000000 + 0:00:01.993000000
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line '138'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line '00:13:25,295 --> 00:13:28,077'
subparse gstsubparse.c:843:parse_subrip_time: parsing timestamp '00:13:25,295'
subparse gstsubparse.c:843:parse_subrip_time: parsing timestamp '00:13:28,077'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line 'I€I? I»IµI?; I I?I?I±I?I?I¬I? IZIµ I¶I?II?I¶IµI? I?I?I?I?I?I±.'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line 'I?I?I¬I»IµIYIµ I?I? I±I?I?I?I?I?I?I·I?I?!'
subparse gstsubparse.c:1535:handle_buffer:<subparse0> Parsing line ''
subparse gstsubparse.c:1566:handle_buffer:<subparse0> Sending text 'I€I? I»IµI&#x82;; I&#x9f; I?I&#x80;I±I?I&#x80;I¬I&#x82; IZIµ I¶I?I&#x81;I?I¶IµI? I&#x84;I?I&#x80;I?I&#x84;I±.
I&#x94;I?I¬I»IµIYIµ I&#x84;I? I±I?I&#x84;I?I?I?I?I·I&#x84;I?!', 0:13:25.295000000 + 0:00:02.782000000


It works up to the 137 subtitle without problem."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-12-01 02:23:27', 'AEDT'),
                text="""Two more thoughts:

a) the incoming data might be UTF-16 or UTF-32. In this case trailing NULs are 2 bytes or 4 bytes, and removing a single NUL byte at the end would might a character in half. This is an argument for doing the trimming of NULs just before the gst_utf8_validate() and not earlier.

b) there could be multiple trailing zeroes

So how about a while (len > 0 && str[len-1] == '\0') --len just before the _validate()?"""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-12-01 02:46:17', 'AEDT'),
                text="""What happens when the buffer contains multiple NULs?

Maybe it's better to check up to the first occurence of NUL?

gsize check_len = strnlen(str, len);
len = check_len < len ? check_len : len;"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-02 02:12:48', 'AEDT'),
                attachment_id=291917,
                text="""Created attachment 291917
subparse: avoid false negative with g_utf8_validate

Something like this then? I understand "consumed" should still
match the size of the original run, NULs included.

Also tested this patch with a section of the newly attached srt
FILE starting around sub #135. It works."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-12-02 08:54:43', 'AEDT'),
                attachment_id=291949,
                text="""Created attachment 291949
avoid false negative with g_utf8_validate and set detected encoding

With the attached patch I have no issues with attached subtitles.

There difference is that we set detected encoding, so that next buffer to handle with so that next buffer to checked directly against gst_convert_to_utf8, instead of validating every buffer."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-12-04 01:23:54', 'AEDT'),
                attachment_id=291949,
                text="""Comment on attachment 291949
avoid false negative with g_utf8_validate and set detected encoding

Thanks for the patch! I don't think it's really right though.

>   /* Otherwise check if it's UTF8 */
>   if (self->valid_utf8) {
>+    while (len > 0 && str[len] == '\0') --len;

This is an off-by-one, no?

>     if (g_utf8_validate (str, len, NULL)) {
>       GST_LOG_OBJECT (self, "valid UTF-8, no conversion needed");
>+      self->detected_encoding = g_strdup ("UTF-8");

I don't think this is correct. Example:

First string: "Hello!"
Second: "My name is $iso_8859_15_characters"."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-04 01:32:50', 'AEDT'),
                text="""Athanasios, latest patch I attached should fix the issue.
Please double check. I already did and it works here."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-12-04 04:00:45', 'AEDT'),
                text="""Tim, the idea of setting detected_encoding was to use gst_convert_to_utf8 since we have valid UTF-8 data. Usually one srt subtitle is encoded using only one encoding, so why to validate every buffer? In case gst_convert_to_uf8 fail will go again on validate (and most probably validate will fail too!).

  /* First try any detected encoding */
  if (self->detected_encoding) {
    ret =
        gst_convert_to_utf8 (str, len, self->detected_encoding, consumed, &err);

    if (!err)
      return ret;


Reynaldo, You are right! I just tested and both samples work great!

So please push Reynaldo's patch."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-12-04 05:55:40', 'AEDT'),
                text="""OOOpppsss! Reynaldo, it still hapens! Sorry...

subparse1>[00m Parsing line '137'
subparse1>[00m Parsing line '00:13:22,771 --> 00:13:24,764'
subparse.c:848:parse_subrip_time:[00m parsing timestamp '00:13:22,771'
subparse.c:848:parse_subrip_time:[00m parsing timestamp '00:13:24,764'
subparse1>[00m Parsing line 'Ήδη τον ζορίζεις να'
subparse1>[00m invalid UTF-8!
subparse1>[00m successfully converted 4096 characters from ISO-8859-15 to UTF-8
subparse1>[00m Parsing line 'τα κάνει όλα μόνος I?I?I?;'
subparse1>[00m Parsing line ''
subparse1>[00m Sending text 'Ήδη τον ζορίζεις να
τα κάνει όλα μόνος I&#x84;I?I?;', 0:13:22.771000000 + 0:00:01.993000000
subparse.c:199:gst_sub_parse_src_query:[00m Handling duration query
subparse.c:199:gst_sub_parse_src_query:[00m Handling duration query
subparse.c:199:gst_sub_parse_src_query:[00m Handling duration query
subparse.c:199:gst_sub_parse_src_query:[00m Handling duration query
subparse.c:199:gst_sub_parse_src_query:[00m Handling duration query
subparse1>[00m Parsing line '138'
subparse1>[00m Parsing line '00:13:25,295 --> 00:13:28,077'
subparse.c:848:parse_subrip_time:[00m parsing timestamp '00:13:25,295'
subparse.c:848:parse_subrip_time:[00m parsing timestamp '00:13:28,077'
subparse1>[00m Parsing line 'I€I? I»IµI?; I I?I?I±I?I?I¬I? IZIµ I¶I?II?I¶IµI? I?I?I?I?I?I±.'
subparse1>[00m Parsing line 'I?I?I¬I»IµIYIµ I?I? I±I?I?I?I?I?I?I·I?I?!'
subparse1>[00m Parsing line ''
subparse1>[00m Sending text 'I€I? I»IµI&#x82;; I&#x9f; I?I&#x80;I±I?I&#x80;I¬I&#x82; IZIµ I¶I?I&#x81;I?I¶IµI? I&#x84;I?I&#x80;I?I&#x84;I±.
I&#x94;I?I¬I»IµIYIµ I&#x84;I? I±I?I&#x84;I?I?I?I?I·I&#x84;I?!', 0:13:25.295000000 + 0:00:02.782000000

Can we further debug it?"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-04 09:16:05', 'AEDT'),
                text="""(In reply to comment #18)
> [..]
>
> Can we further debug it?

Sure. Can you attach an small (in duration) sub file that shows
the problem with the patch applied? say a 2-3 minutes file. I'm
failing to reproduce your failure using a section of the last
file you attached."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-04 15:44:04', 'AEDT'),
                attachment_id=292114,
                text="""Created attachment 292114
subparse: avoid false negatives dealing with UTF-8

OK. Turns out we were only looking at part of the problem.

Termination NULs are an issue, Granted. And these were already
been handled by my previous patch. But there's another condition
that wasn't been considered:

It might be that only part of the available data is be valid
UTF-8. For example a byte at the end might be the start of a
valid UTF-8 run (ie: d0 / 11010000) but not be a valid UTF-8
character by itself. In this case, we should consume only the
valid portion of the run.

This new patch addresses the whole issue as I see it. Quickly
drafted solution and I'm likely fine-tuning it tomorrow but
comments & testing welcome all the same. I tried out with the
samples I have and it works."""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-04 15:48:13', 'AEDT'),
                attachment_id=291949,
                text="""Comment on attachment 291949
avoid false negative with g_utf8_validate and set detected encoding

This patch reads past the end of the sub data and
does not handle the full issue."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2014-12-05 03:49:29', 'AEDT'),
                attachment_id=292114,
                text="""Review of attachment 292114:

Now we can handle both schenarios. Great Reynaldo!

subparse gstsubparse.c:199:gst_sub_parse_src_query: Handling duration query
subparse gstsubparse.c:199:gst_sub_parse_src_query: Handling duration query
subparse gstsubparse.c:1554:handle_buffer:<subparse0> Parsing line '137'
subparse gstsubparse.c:1554:handle_buffer:<subparse0> Parsing line '00:13:22,771 --> 00:13:24,764'
subparse gstsubparse.c:862:parse_subrip_time: parsing timestamp '00:13:22,771'
subparse gstsubparse.c:862:parse_subrip_time: parsing timestamp '00:13:24,764'
subparse gstsubparse.c:1554:handle_buffer:<subparse0> Parsing line 'Ήδη τον ζορίζεις να'
subparse gstsubparse.c:479:convert_encoding:<subparse0> At least some of the data was invalid UTF-8
subparse gstsubparse.c:1554:handle_buffer:<subparse0> Parsing line 'τα κάνει όλα μόνος του;'
subparse gstsubparse.c:1554:handle_buffer:<subparse0> Parsing line ''
subparse gstsubparse.c:1585:handle_buffer:<subparse0> Sending text 'Ήδη τον ζορίζεις να
τα κάνει όλα μόνος του;', 0:13:22.771000000 + 0:00:01.993000000"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-05 06:59:10', 'AEDT'),
                text="""Cool Athanasios. Thanks for your help testing it!"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2014-12-05 08:06:55', 'AEDT'),
                text="""Also looks good to me, but I'd like to wait for Tim's opinion before merging this :)"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2014-12-15 03:22:42', 'AEDT'),
                text="""Tim?"""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2014-12-16 12:22:58', 'AEDT'),
                text="""I'm not sure if this is ok as-is. I'm fine with cutting off the text after the first invalid character for text that is definitely UTF-8, i.e. where we had a BOM, but if I'm reading the current patch correctly it would basically just cut off non-UTF8 encodings at the first non-ASCII character and treat it as UTF-8 instead of falling through to the fallbacks (current locale, environment variables, etc.)."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2015-03-21 04:42:20', 'AEDT'),
                text="""Is there any other ideas how to solve the pending issues from that bug?"""
            ), make_comment(
                author='reynaldo@osg.samsung.com',
                creation_time=make_datetime('2015-04-20 07:10:35', 'AEST'),
                attachment_id=301945,
                text="""Created attachment 301945
subparse: avoid false negatives dealing with UTF-8

Patch rebased on top of latest master.

Had completely forgot about this issue. Will take another
look and comment. My take though, was that as long as we
rely on finding one valid code point the patch should be
rather safe. I might not be remembering this clearly but
from what I know, finding a valid code point in otherwise
non UTF-8 text it's quite unlikely.

Relying on the BOM being present is not a good idea in
general, BOM presence is not required and even frowned
upon."""
            ), make_comment(
                author='t.i.m@zen.co.uk',
                creation_time=make_datetime('2015-04-20 07:33:54', 'AEST'),
                text="""My comment was about the equally valid non-UTF-8 case. Of course we should not rely on a BOM, but if a BOM is present we can make stronger assumptions about the encoding of the text."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2015-08-07 22:41:24', 'AEST'),
                attachment_id=301945,
                text="""Review of attachment 301945:

Above patch works with both srt provided."""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2015-08-07 23:50:56', 'AEST'),
                attachment_id=301945,
                text="""Comment on attachment 301945
subparse: avoid false negatives dealing with UTF-8

Please don't mark patches as accept-commit-now, that's only meant to be used by the maintainers."""
            ), make_comment(
                author='athoik@gmail.com',
                creation_time=make_datetime('2017-06-29 06:41:10', 'AEST'),
                text="""1.12.1 and patch still applies and works for me."""
            )
        ]
    )

bug776540 = make_bug(776540,
        creation_time=make_datetime('2016-12-28 21:52:48', 'AEDT'),
        last_change_time=make_datetime('2017-12-26 09:40:05', 'AEDT'),
        version='1.10.2',
        status='NEEDINFO',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='petroskataras@gmail.com',
        summary='GstGL: Occasional assertion failures on GST_IS_GL_DISPLAY and GST_IS_GL_CONTEXT',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['ystreet00@gmail.com'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2016-12-28 21:52:48', 'AEDT'),
                attachment_id=342522,
                text="""Created attachment 342522
GST_DEBUG=*gl:6

I am experiencing some weird behavior related to the GstGL elements when trying to load videos in async mode ( i.e not blocking the pipeline until its pre-rolled ).

More specifically I occasionally get :

** (<unknown>:65255): CRITICAL **: gst_gl_context_get_display: assertion 'GST_IS_GL_CONTEXT (context)' failed

** (<unknown>:65255): CRITICAL **: gst_gl_display_get_handle_type: assertion 'GST_IS_GL_DISPLAY (display)' failed

(<unknown>:65255): GStreamer-CRITICAL **: gst_object_unref: assertion 'object != NULL' failed

(<unknown>:65255): GStreamer-CRITICAL **: gst_object_unref: assertion 'object != NULL' failed

and / or

(<unknown>:65255): GStreamer-CRITICAL **: invalid unclassed object pointer for value type 'GstGLDisplay'

These assertion failures show up in a non-consistent way and although the application most of the times will still continue properly there are other times where it will just fail an abort. What is interesting is that if I export GST_DEBUG=*gl:6 to try and observe what is happening the application will fail after some reloads with:

** (<unknown>:64943): CRITICAL **: gst_gl_context_fill_info: assertion 'context->priv->active_thread == g_thread_self ()' failed
**
ERROR:gstglcontext.c:1252:gst_gl_context_create_thread: assertion failed: (error == NULL || *error != NULL)

Attached you can find the log output of such a failure configured with GST_DEBUG=*gl:6.

Again, this does not manifest if I block the pipeline until its pre-rolled .

I ve been seeing this behavior on both OS X and Linux but I m currently testing on OS X.

The player is playbin based and is located here :

https://github.com/PetrosKataras/Cinder/blob/master/src/cinder/linux/GstPlayer.cpp

with the relevant parts of initializing and setting the GL components :

https://github.com/PetrosKataras/Cinder/blob/master/src/cinder/linux/GstPlayer.cpp#L168-L193

https://github.com/PetrosKataras/Cinder/blob/master/src/cinder/linux/GstPlayer.cpp#L465-L503

https://github.com/PetrosKataras/Cinder/blob/master/src/cinder/linux/GstPlayer.cpp#L561-L585

I would really appreciate any clues on this.."""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2016-12-30 11:48:36', 'AEDT'),
                text="""G_DEBUG=fatal-warnings and a debugger are your friends here with the criticals/warnings.  You can get a backtrace when the critical occurs which may be very helpful.

One possible explanation is a race between cinder/glfw and setting up the GL display/context.  Make sure that the supporting library is set up correctly before attempting to retrieve the necessary display/context from it.

For g(st)_object_(un)ref assertions.  That usually means a refcounting bug somewhere which could be causing this whole mess.

The end of the log has activation happening in another thread than the fill_info is called which should not happen.  Determining why that happens will probably get you closer to fixing the problem:

0:00:09.850171000 0x7fef3e76c840 DEBUG  glcontext gstglcontext.c:726:gst_gl_context_activate:<glcontextcocoa6>[00m activate:1
0:00:09.850246000 0x7fef3d9380a0 DEBUG  glcontext gstglcontext.c:1250:gst_gl_context_create_thread:<glcontextcocoa6>[00m Filling info

** (<unknown>:64943): CRITICAL **: gst_gl_context_fill_info: assertion 'context->priv->active_thread == g_thread_self ()' failed
**
ERROR:gstglcontext.c:1252:gst_gl_context_create_thread: assertion failed: (error == NULL || *error != NULL)"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2016-12-30 20:36:03', 'AEDT'),
                attachment_id=342609,
                text="""Created attachment 342609
Sync mode with GST_DEBUG=*gl*:6"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2016-12-30 20:36:44', 'AEDT'),
                text="""Hi Matthew and thank you very much for your reply.

I m fairly sure that the context from Cinder's side is active and valid at the point where I am retrieving it in order to initialize the GstGL side. The context I am retrieving from Cinder is non-null and the same that I get from a straight call to CGLGetCurrentContex() when the fail happens.

Cinder is using straight Cocoa on OS X and GLFW is used only on Linuxland but I am facing the same issue on both platforms.

What makes me worry is that these issues *do not* manifest if I block the pipeline during load until its pre-rolled. For comparison I attach a log at the same level as the previous one but on sync mode this time ( i.e I m blocking until pre-roll during load ).

I ve realised that for some reason the activation happens in a different thread but I m not entirely sure what would be the best way to debug this issue and figure out why this would be happening. The OS X binaries also seem to miss debug symbols which makes things a bit harder to debug but I could also try on Linux where I m using a manually built gst-uninstalled setup."""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2016-12-30 23:29:01', 'AEDT'),
                text="""I have some more info after running on Linux.

Here is the backtrace :

#0  0x00007ffff6618a6b in g_logv () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#1  0x00007ffff6618bdf in g_log () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#2  0x00007ffff5955ccf in gst_gl_context_get_display (context=0x0) at gstglcontext.c:1490
#3  0x00007ffff5970d75 in gst_gl_handle_set_context (element=element@entry=0x1572480, context=context@entry=0x7fffa00016a0, display=display@entry=0x15726c0,
    other_context=0x15723a0) at gstglutils.c:403
#4  0x00007ffff5962140 in gst_gl_base_filter_set_context (element=0x1572480, context=0x7fffa00016a0) at gstglbasefilter.c:178
#5  0x00007ffff5970799 in _gst_context_query (element=element@entry=0x1572480, display_type=display_type@entry=0x7ffff5985d59 "gst.gl.app_context") at gstglutils.c:233
#6  0x00007ffff5970b4f in gst_gl_context_query (element=0x1572480) at gstglutils.c:282
#7  gst_gl_ensure_element_data (element=element@entry=0x1572480, display_ptr=display_ptr@entry=0x15726c0, context_ptr=0x15723a0) at gstglutils.c:346
#8  0x00007ffff596204d in gst_gl_base_filter_change_state (element=0x1572480, transition=GST_STATE_CHANGE_NULL_TO_READY) at gstglbasefilter.c:483
#9  0x00007ffff62f172e in gst_element_change_state (element=element@entry=0x1572480, transition=transition@entry=GST_STATE_CHANGE_NULL_TO_READY) at gstelement.c:2695
#10 0x00007ffff62f1ea7 in gst_element_set_state_func (element=0x1572480, state=GST_STATE_READY) at gstelement.c:2649
#11 0x00007ffff62d0295 in gst_bin_element_set_state (next=GST_STATE_READY, current=GST_STATE_NULL, start_time=0, base_time=0, element=0x1572480, bin=0x7fffcc0118a0)
    at gstbin.c:2613
#12 gst_bin_change_state_func (element=0x7fffcc0118a0, transition=GST_STATE_CHANGE_NULL_TO_READY) at gstbin.c:2955
#13 0x00007ffff62f172e in gst_element_change_state (element=element@entry=0x7fffcc0118a0, transition=transition@entry=GST_STATE_CHANGE_NULL_TO_READY) at gstelement.c:2695
#14 0x00007ffff62f1ea7 in gst_element_set_state_func (element=0x7fffcc0118a0, state=GST_STATE_READY) at gstelement.c:2649
#15 0x00007fffea81d22f in activate_sink (playbin=playbin@entry=0x127e040, sink=0x7fffcc0118a0, activated=activated@entry=0x7fffffffab48) at gstplaybin2.c:4451
#16 0x00007fffea822a7e in activate_group (target=GST_STATE_PAUSED, group=0x127e4c8, playbin=0x127e040) at gstplaybin2.c:5243
#17 setup_next_source (playbin=playbin@entry=0x127e040, target=GST_STATE_PAUSED) at gstplaybin2.c:5644
#18 0x00007fffea823be8 in gst_play_bin_change_state (element=0x127e040, transition=GST_STATE_CHANGE_READY_TO_PAUSED) at gstplaybin2.c:5774
#19 0x00007ffff62f172e in gst_element_change_state (element=element@entry=0x127e040, transition=transition@entry=GST_STATE_CHANGE_READY_TO_PAUSED) at gstelement.c:2695
#20 0x00007ffff62f1ea7 in gst_element_set_state_func (element=0x127e040, state=GST_STATE_PAUSED) at gstelement.c:2649
#21 0x00000000009c1e80 in gst::video::GstPlayer::setPipelineState (this=0x1449c00, targetState=GST_STATE_PAUSED, forceSync=false)
    at /home/petroska/Software/libs/cinder-aes/src/cinder/linux/GstPlayer.cpp:980
#22 0x00000000009c1084 in gst::video::GstPlayer::load (this=0x1449c00, path="/home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/assets/bbb.mp4",
    async=false) at /home/petroska/Software/libs/cinder-aes/src/cinder/linux/GstPlayer.cpp:659
#23 0x00000000008218db in cinder::linux::MovieBase::initFromPath (this=0x14fce00, filePath=...) at /home/petroska/Software/libs/cinder-aes/src/cinder/linux/Movie.cpp:68
#24 0x0000000000821e9e in cinder::linux::MovieGl::MovieGl (this=0x14fce00, path=...) at /home/petroska/Software/libs/cinder-aes/src/cinder/linux/Movie.cpp:247
#25 0x0000000000734784 in cinder::linux::MovieGl::create (path=...) at /home/petroska/Software/libs/cinder-aes/include/cinder/linux/Movie.h:170
#26 0x000000000073351f in GstPlayerTestApp::loadMovieFile (this=0xde0ff0, moviePath=...)
    at /home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/src/GstPlayerTestApp.cpp:293
#27 0x00000000007325c1 in GstPlayerTestApp::newLoad (this=0xde0ff0)
    at /home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/src/GstPlayerTestApp.cpp:206
#28 0x0000000000732500 in GstPlayerTestApp::testReload (this=0xde0ff0)
    at /home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/src/GstPlayerTestApp.cpp:195
#29 0x0000000000730ea7 in GstPlayerTestApp::testCurrentCase (this=0xde0ff0)
    at /home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/src/GstPlayerTestApp.cpp:125
#30 0x0000000000733051 in GstPlayerTestApp::update (this=0xde0ff0) at /home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/src/GstPlayerTestApp.cpp:244
#31 0x000000000074790d in cinder::app::AppBase::privateUpdate__ (this=0xde0ff0) at /home/petroska/Software/libs/cinder-aes/src/cinder/app/AppBase.cpp:232
#32 0x00000000008172bb in cinder::app::AppImplLinux::run (this=0xde0cf0) at /home/petroska/Software/libs/cinder-aes/src/cinder/app/linux/AppImplLinuxGlfw.cpp:332
#33 0x0000000000810bcb in cinder::app::AppLinux::launch (this=0xde0ff0) at /home/petroska/Software/libs/cinder-aes/src/cinder/app/linux/AppLinux.cpp:48
#34 0x00000000007474f3 in cinder::app::AppBase::executeLaunch (this=0xde0ff0) at /home/petroska/Software/libs/cinder-aes/src/cinder/app/AppBase.cpp:190
#35 0x0000000000735639 in cinder::app::AppLinux::main<GstPlayerTestApp>(std::shared_ptr<cinder::app::Renderer> const&, char const*, int, char* const*, std::function<void (cinder::app::AppBase::Settings*)> const&) (defaultRenderer=std::shared_ptr (count 3, weak 0) 0xddfe40, title=0xa445bf "GstPlayerTestApp", argc=1, argv=0x7fffffffb528,
    settingsFn=...) at /home/petroska/Software/libs/cinder-aes/include/cinder/app/linux/AppLinux.h:85
#36 0x0000000000733756 in main (argc=1, argv=0x7fffffffb528) at /home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/src/GstPlayerTestApp.cpp:316


It seems that context_replacement is NULL for some reason when reading the structure as I can see from the following lines but no clue yet why this is the case..

...

398         GstGLDisplay *context_display;
399         GstGLDisplay *element_display;
400
401         if (gst_structure_get (s, "context", GST_GL_TYPE_CONTEXT,
402                 &context_replacement, NULL)) {
403           context_display = gst_gl_context_get_display (context_replacement);
404           element_display = display_replacement ? display_replacement : *display;
405           if (element_display
406               && (gst_gl_display_get_handle_type (element_display) &
407                   gst_gl_display_get_handle_type (context_display)) == 0) {
(gdb) print s
$8 = (const GstStructure *) 0x148ff60
(gdb) print *s
$9 = {type = 18969104, name = 216}
(gdb) print context_replacement
$10 = (GstGLContext *) 0x0"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-02 21:20:40', 'AEDT'),
                text="""I ve been looking a bit more into this on Linux. It seems that these failures are consistently coming from the glupload element when trying to set gst.gl.app_context and the element has not reached GST_STATE_READY.

On successful runs if I manually break at the same point where it fails occasionally ( i.e inside gst_gl_handle_context ) the element has already switched to GST_STATE_READY while on failed runs it is in an async state change mode from NULL to READY ( this is visible also on the bt I previously attached ).

For comparison here is how the upload element looks on a successful run :

{object = {object = {g_type_instance = {g_class = 0x13f3cc0}, ref_count = 3, qdata = 0x0}, lock = {p = 0x0, i = {0, 0}}, name = 0x13f8340 "upload", parent = 0x13dd0e0,
    flags = 0, control_bindings = 0x0, control_rate = 100000000, last_sync = 18446744073709551615, _gst_reserved = 0x0}, state_lock = {p = 0x13f8310, i = {0, 0}},
  state_cond = {p = 0x0, i = {2, 0}}, state_cookie = 1, target_state = GST_STATE_READY, current_state = GST_STATE_READY, next_state = GST_STATE_VOID_PENDING,
  pending_state = GST_STATE_VOID_PENDING, last_return = GST_STATE_CHANGE_SUCCESS, bus = 0x1270c70, clock = 0x0, base_time = 0, start_time = 0, numpads = 2, pads = 0x13f5480,
  numsrcpads = 1, srcpads = 0x13f54a0, numsinkpads = 1, sinkpads = 0x13f5460, pads_cookie = 2, contexts = 0x1401020, _gst_reserved = {0x0, 0x0, 0x0}}


and here is how it looks on a failed run :

{object = {object = {g_type_instance = {g_class = 0x13f3cc0}, ref_count = 2, qdata = 0x0}, lock = {p = 0x0, i = {0, 0}}, name = 0x142b2a0 "upload",
    parent = 0x7fff800126a0, flags = 0, control_bindings = 0x0, control_rate = 100000000, last_sync = 18446744073709551615, _gst_reserved = 0x0}, state_lock = {
    p = 0x1465830, i = {0, 0}}, state_cond = {p = 0x0, i = {1, 0}}, state_cookie = 1, target_state = GST_STATE_READY, current_state = GST_STATE_NULL,
  next_state = GST_STATE_READY, pending_state = GST_STATE_READY, last_return = GST_STATE_CHANGE_ASYNC, bus = 0x7fffd004d310, clock = 0x0, base_time = 0, start_time = 0,
  numpads = 2, pads = 0x7fffd01bc620, numsrcpads = 1, srcpads = 0x7fff8c008900, numsinkpads = 1, sinkpads = 0x7fffdc045180, pads_cookie = 2, contexts = 0x7fffd814eba0,
  _gst_reserved = {0x0, 0x0, 0x0}}


The failure happens at the point where playbin has reached GST_STATE_READY and tries to activate the video-sink that contains the glupload element.

Here is how the video-sink looks at the point of the failure :

{object = {object = {g_type_instance = {g_class = 0x12242c0}, ref_count = 4, qdata = 0x0}, lock = {p = 0x0, i = {0, 0}}, name = 0x1457eb0 "cinder-vbin", parent = 0x0,
    flags = 32, control_bindings = 0x0, control_rate = 100000000, last_sync = 18446744073709551615, _gst_reserved = 0x0}, state_lock = {p = 0x145f510, i = {0, 0}},
  state_cond = {p = 0x0, i = {1, 0}}, state_cookie = 1, target_state = GST_STATE_READY, current_state = GST_STATE_NULL, next_state = GST_STATE_READY,
  pending_state = GST_STATE_READY, last_return = GST_STATE_CHANGE_ASYNC, bus = 0x7fffd01513b0, clock = 0x0, base_time = 0, start_time = 0, numpads = 1,
  pads = 0x7fffdc03a400, numsrcpads = 0, srcpads = 0x0, numsinkpads = 1, sinkpads = 0x7fffdc0054c0, pads_cookie = 1, contexts = 0x7fff8c005920, _gst_reserved = {0x0, 0x0,
    0x0}}

and how playbin looks at the same point :

{parent = {bin = {element = {object = {object = {g_type_instance = {g_class = 0x12971f0}, ref_count = 2, qdata = 0x2}, lock = {p = 0x0, i = {0, 0}},
          name = 0x142be00 "playbinsink", parent = 0x0, flags = 32, control_bindings = 0x0, control_rate = 100000000, last_sync = 18446744073709551615, _gst_reserved = 0x0},
        state_lock = {p = 0x14645e0, i = {0, 0}}, state_cond = {p = 0x0, i = {3, 0}}, state_cookie = 2, target_state = GST_STATE_PAUSED, current_state = GST_STATE_READY,
        next_state = GST_STATE_PAUSED, pending_state = GST_STATE_PAUSED, last_return = GST_STATE_CHANGE_ASYNC, bus = 0x7fffcc75e480, clock = 0x0, base_time = 0,
        start_time = 0, numpads = 0, pads = 0x0, numsrcpads = 0, srcpads = 0x0, numsinkpads = 0, sinkpads = 0x0, pads_cookie = 0, contexts = 0x0, _gst_reserved = {0x0, 0x0,
          0x0}}, numchildren = 1, children = 0x7fff8c018800, children_cookie = 1, child_bus = 0x7fff8c117b90, messages = 0x7fffdc0d24e0, polling = 0, state_dirty = 0,
      clock_dirty = 0, provided_clock = 0x0, clock_provider = 0x0, priv = 0x124d620, _gst_reserved = {0x0, 0x0, 0x0, 0x0}}, fixed_clock = 0x0, stream_time = 0, delay = 0,
    priv = 0x124d600, _gst_reserved = {0x0, 0x0, 0x0, 0x0}}, lock = {p = 0x14454a0, i = {0, 0}}, groups = {{playbin = 0x124d640, lock = {p = 0x0, i = {0, 0}}, valid = 0,
      active = 0, uri = 0x0, suburi = 0x0, streaminfo = 0x0, source = 0x0, video_channels = 0x7fffdc024a40, audio_channels = 0x7fff8c0030c0, text_channels = 0x7fff8c00d2e0,
      audio_sink = 0x0, video_sink = 0x0, text_sink = 0x0, uridecodebin = 0x0, suburidecodebin = 0x0, pending = 0, sub_pending = 0, have_group_id = 0, group_id = 0,
      pad_added_id = 0, pad_removed_id = 0, no_more_pads_id = 0, notify_source_id = 0, drained_id = 0, autoplug_factories_id = 0, autoplug_select_id = 0,
      autoplug_continue_id = 0, autoplug_query_id = 0, sub_pad_added_id = 0, sub_pad_removed_id = 0, sub_no_more_pads_id = 0, sub_autoplug_continue_id = 0,
      sub_autoplug_query_id = 0, block_id = 0, stream_changed_pending_lock = {p = 0x0, i = {0, 0}}, stream_changed_pending = 0, suburi_flushes_to_drop_lock = {p = 0x0, i = {
          0, 0}}, suburi_flushes_to_drop = 0x0, pending_buffering_msg = 0x0, combiner = {{media_list = {0x7fffea849a41 "audio/", 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
          get_media_caps = 0x0, type = GST_PLAY_SINK_TYPE_AUDIO, combiner = 0x0, channels = 0x7fff8c0030c0, srcpad = 0x0, sinkpad = 0x0, block_id = 0, has_active_pad = 0,
          has_always_ok = 0, has_tags = 0}, {media_list = {0x7fffea849a2e "video/", 0x7fffea84f94e "image/", 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, get_media_caps = 0x0,
          type = GST_PLAY_SINK_TYPE_VIDEO, combiner = 0x0, channels = 0x7fffdc024a40, srcpad = 0x0, sinkpad = 0x0, block_id = 0, has_active_pad = 0, has_always_ok = 0,
          has_tags = 0}, {media_list = {0x7fffea84f955 "text/", 0x7fffea850584 "application/x-subtitle", 0x7fffea85059b "application/x-ssa",
            0x7fffea8505ad "application/x-ass", 0x7fffea8505bf "subpicture/x-dvd", 0x7fffea84f95b "subpicture/", 0x7fffea8505d0 "subtitle/", 0x0},
          get_media_caps = 0x7fffea841ef0 <gst_subtitle_overlay_create_factory_caps>, type = GST_PLAY_SINK_TYPE_TEXT, combiner = 0x0, channels = 0x7fff8c00d2e0,
          srcpad = 0x0, sinkpad = 0x0, block_id = 0, has_active_pad = 0, has_always_ok = 0, has_tags = 0}}}, {playbin = 0x124d640, lock = {p = 0x1, i = {1, 0}}, valid = 1,
      active = 0, uri = 0x1246110 "file:///home/petroska/Software/libs/cinder-aes/test/Linux/GstPlayerRefactorTest/assets/bbb.mp4", suburi = 0x0, streaminfo = 0x0,
      source = 0x0, video_channels = 0x7fffdc02e2a0, audio_channels = 0x7fffd8147240, text_channels = 0x7fff8c004880, audio_sink = 0x0, video_sink = 0x13dd0e0,
      text_sink = 0x0, uridecodebin = 0x0, suburidecodebin = 0x0, pending = 0, sub_pending = 0, have_group_id = 0, group_id = 0, pad_added_id = 0, pad_removed_id = 0,
      no_more_pads_id = 0, notify_source_id = 0, drained_id = 0, autoplug_factories_id = 0, autoplug_select_id = 0, autoplug_continue_id = 0, autoplug_query_id = 0,
      sub_pad_added_id = 0, sub_pad_removed_id = 0, sub_no_more_pads_id = 0, sub_autoplug_continue_id = 0, sub_autoplug_query_id = 0, block_id = 0,
      stream_changed_pending_lock = {p = 0x0, i = {0, 0}}, stream_changed_pending = 0, suburi_flushes_to_drop_lock = {p = 0x0, i = {0, 0}}, suburi_flushes_to_drop = 0x0,
      pending_buffering_msg = 0x0, combiner = {{media_list = {0x7fffea849a41 "audio/", 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, get_media_caps = 0x0,
          type = GST_PLAY_SINK_TYPE_AUDIO, combiner = 0x0, channels = 0x7fffd8147240, srcpad = 0x0, sinkpad = 0x0, block_id = 0, has_active_pad = 0, has_always_ok = 0,
          has_tags = 0}, {media_list = {0x7fffea849a2e "video/", 0x7fffea84f94e "image/", 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, get_media_caps = 0x0,
          type = GST_PLAY_SINK_TYPE_VIDEO, combiner = 0x0, channels = 0x7fffdc02e2a0, srcpad = 0x0, sinkpad = 0x0, block_id = 0, has_active_pad = 0, has_always_ok = 0,
          has_tags = 0}, {media_list = {0x7fffea84f955 "text/", 0x7fffea850584 "application/x-subtitle", 0x7fffea85059b "application/x-ssa",
            0x7fffea8505ad "application/x-ass", 0x7fffea8505bf "subpicture/x-dvd", 0x7fffea84f95b "subpicture/", 0x7fffea8505d0 "subtitle/", 0x0},
          get_media_caps = 0x7fffea841ef0 <gst_subtitle_overlay_create_factory_caps>, type = GST_PLAY_SINK_TYPE_TEXT, combiner = 0x0, channels = 0x7fff8c004880,
          srcpad = 0x0, sinkpad = 0x0, block_id = 0, has_active_pad = 0, has_always_ok = 0, has_tags = 0}}}}, curr_group = 0x124dac8, next_group = 0x124d808,
  connection_speed = 0, current_video = -1, current_audio = -1, current_text = -1, buffer_duration = 18446744073709551615, buffer_size = 4294967295, force_aspect_ratio = 1,
  multiview_mode = GST_VIDEO_MULTIVIEW_FRAME_PACKING_NONE, multiview_flags = GST_VIDEO_MULTIVIEW_FLAGS_NONE, playsink = 0x13da5b0, source = 0x0, dyn_lock = {p = 0x0, i = {0,
      0}}, shutdown = 0, async_pending = 1, elements_lock = {p = 0x0, i = {0, 0}}, elements_cookie = 0, elements = 0x0, have_selector = 1, video_pending_flush_finish = 0,
  audio_pending_flush_finish = 0, text_pending_flush_finish = 0, audio_sink = 0x0, video_sink = 0x13dd0e0, text_sink = 0x0, audio_stream_combiner = 0x0,
  video_stream_combiner = 0x0, text_stream_combiner = 0x0, aelements = 0x0, velements = 0x0, duration = {{valid = 0, format = GST_FORMAT_UNDEFINED, duration = 0}, {
      valid = 0, format = GST_FORMAT_UNDEFINED, duration = 0}, {valid = 0, format = GST_FORMAT_UNDEFINED, duration = 0}, {valid = 0, format = GST_FORMAT_UNDEFINED,
      duration = 0}, {valid = 0, format = GST_FORMAT_UNDEFINED, duration = 0}}, ring_buffer_max_size = 0, contexts = 0x125b2e0}"""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-02 22:02:07', 'AEDT'),
                text="""This may all be because checkBusMessages() at https://github.com/PetrosKataras/Cinder/blob/master/src/cinder/linux/GstPlayer.cpp#L125 is only responding to the async bus handler.  To be able to correctly and non-racily respond to need-context messages, you need to add a sync bus handler for (at the very least) the need-context message.

The two options for that are using:
1. gst_bus_set_sync_handler(), or;
2. gst_bus_enable_sync_message_emission() and the 'sync-message' signal on the bus (https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstBus.html#GstBus-sync-message)

Does adding the synchronous bus handler for the 'need-context' message help the situation?"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-02 22:09:44', 'AEDT'),
                text="""Hi Matthew,

I tried last week to hook a sync-bus handler only for the need-context message on OS X but I was still getting some issues. It was a very quick test though and in parallel I also had the async bus handler running which I am not sure if this could cause some issues ( although I was not checking anymore inside the async handler for the need-context msg just the state change logic ) .

Do you think it would worth trying replacing the whole async bus handling with a sync version??

In any case I will give a try on Linux with a sync bus handler and report back."""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-02 22:13:07', 'AEDT'),
                text="""You need both the async and sync handlers.  The need-context should be in the sync handler, everything else there can be async."""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-02 22:43:02', 'AEDT'),
                text="""So adding gst_bus_sync_handler for handling the need-context msg definitely helps for the state change issue but I got a different one now unfortunately..

(GstPlayerTestApp:29020): GStreamer-CRITICAL **: gst_object_ref: assertion 'object != NULL' failed

Thread 4253 "vqueue:src" received signal SIGTRAP, Trace/breakpoint trap.
[Switching to Thread 0x7fffe37fe700 (LWP 816)]
0x00007ffff6618a6b in g_logv () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
(gdb) bt
#0  0x00007ffff6618a6b in g_logv () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#1  0x00007ffff6618bdf in g_log () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#2  0x00007ffff62c4d2a in gst_object_ref (object=object@entry=0x0) at gstobject.c:245
#3  0x00007ffff595e825 in gst_gl_buffer_pool_new (context=0x0) at gstglbufferpool.c:302
#4  0x00007ffff596ca23 in _gl_memory_upload_propose_allocation (impl=0x7fffdc294490, decide_query=<optimized out>, query=0x7fff6c56d1e0)
    at gstglupload.c:363
#5  0x00007ffff596d577 in gst_gl_upload_propose_allocation (upload=0x7fffa00143d0, decide_query=decide_query@entry=0x7fffc447d770,
    query=query@entry=0x7fff6c56d1e0) at gstglupload.c:1352
#6  0x00007fffe9cd2c1e in _gst_gl_upload_element_propose_allocation (bt=0x13fa7e0, decide_query=0x7fffc447d770, query=0x7fff6c56d1e0)
    at gstgluploadelement.c:181
#7  0x00007ffff606bd01 in gst_base_transform_default_query (trans=0x13fa7e0, direction=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1538
#8  0x00007ffff5962b54 in gst_gl_base_filter_query (trans=0x13fa7e0, direction=GST_PAD_SINK, query=0x7fff6c56d1e0) at gstglbasefilter.c:287
#9  0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x13eb4a0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#10 0x00007ffff630f323 in gst_pad_peer_query (pad=0x7fff8c01d900, query=query@entry=0x7fff6c56d1e0) at gstpad.c:4082
#11 0x00007ffff6068cd2 in gst_base_transform_default_propose_allocation (trans=0x7fff8c03e860, decide_query=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1438
#12 0x00007ffff606bd01 in gst_base_transform_default_query (trans=0x7fff8c03e860, direction=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1538
#13 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x7fffdc11d230, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#14 0x00007ffff630f323 in gst_pad_peer_query (pad=pad@entry=0x140d8b0, query=0x7fff6c56d1e0) at gstpad.c:4082
#15 0x00007ffff630fac3 in query_forward_func (pad=pad@entry=0x140d8b0, data=data@entry=0x7fffe37fc950) at gstpad.c:3318
#16 0x00007ffff630d92e in gst_pad_forward (pad=pad@entry=0x140a530, forward=forward@entry=0x7ffff630fa10 <query_forward_func>,
    user_data=user_data@entry=0x7fffe37fc950) at gstpad.c:2947
#17 0x00007ffff630dbb1 in gst_pad_query_default (pad=0x140a530, parent=<optimized out>, query=0x7fff6c56d1e0) at gstpad.c:3385
#18 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x140a530, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#19 0x00007ffff630f323 in gst_pad_peer_query (pad=pad@entry=0x140b640, query=0x7fff6c56d1e0) at gstpad.c:4082
#20 0x00007ffff630fac3 in query_forward_func (pad=pad@entry=0x140b640, data=data@entry=0x7fffe37fcbc0) at gstpad.c:3318
#21 0x00007ffff630d92e in gst_pad_forward (pad=pad@entry=0x7fffcc0789f0, forward=forward@entry=0x7ffff630fa10 <query_forward_func>,
    user_data=user_data@entry=0x7fffe37fcbc0) at gstpad.c:2947
#22 0x00007ffff630dbb1 in gst_pad_query_default (pad=0x7fffcc0789f0, parent=<optimized out>, query=0x7fff6c56d1e0) at gstpad.c:3385
#23 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x7fffcc0789f0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#24 0x00007ffff630f323 in gst_pad_peer_query (pad=0x7fffdc11cff0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:4082
#25 0x00007ffff6068cd2 in gst_base_transform_default_propose_allocation (trans=0x7fff6c0110d0, decide_query=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1438
#26 0x00007ffff5bc8f1a in gst_video_filter_propose_allocation (trans=0x7fff6c0110d0, decide_query=0x0, query=0x7fff6c56d1e0) at gstvideofilter.c:64
#27 0x00007ffff606bd01 in gst_base_transform_default_query (trans=0x7fff6c0110d0, direction=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1538
#28 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x7fffdc2176f0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#29 0x00007ffff630f323 in gst_pad_peer_query (pad=0x7fffdc2174b0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:4082
#30 0x00007ffff6068cd2 in gst_base_transform_default_propose_allocation (trans=0x7fff6c490350, decide_query=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1438
#31 0x00007ffff5bc8f1a in gst_video_filter_propose_allocation (trans=0x7fff6c490350, decide_query=0x0, query=0x7fff6c56d1e0) at gstvideofilter.c:64
#32 0x00007ffff606bd01 in gst_base_transform_default_query (trans=0x7fff6c490350, direction=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1538
#33 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x7fffdc11c4b0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#34 0x00007ffff630f323 in gst_pad_peer_query (pad=0x7fffcc062950, query=query@entry=0x7fff6c56d1e0) at gstpad.c:4082
#35 0x00007ffff6068cd2 in gst_base_transform_default_propose_allocation (trans=0x7fff6c4879c0, decide_query=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1438
#36 0x00007ffff5bc8f1a in gst_video_filter_propose_allocation (trans=0x7fff6c4879c0, decide_query=0x0, query=0x7fff6c56d1e0) at gstvideofilter.c:64
#37 0x00007ffff606bd01 in gst_base_transform_default_query (trans=0x7fff6c4879c0, direction=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1538
#38 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x7fff8c01c4c0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#39 0x00007ffff630f323 in gst_pad_peer_query (pad=0x13eb6e0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:4082
#40 0x00007ffff6068cd2 in gst_base_transform_default_propose_allocation (trans=0x7fff6c56dad0, decide_query=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1438
#41 0x00007ffff5bc8f1a in gst_video_filter_propose_allocation (trans=0x7fff6c56dad0, decide_query=0x0, query=0x7fff6c56d1e0) at gstvideofilter.c:64
#42 0x00007ffff606bd01 in gst_base_transform_default_query (trans=0x7fff6c56dad0, direction=<optimized out>, query=0x7fff6c56d1e0)
    at gstbasetransform.c:1538
#43 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x7fffcc0776f0, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#44 0x00007ffff630f323 in gst_pad_peer_query (pad=pad@entry=0x7fffd00912d0, query=0x7fff6c56d1e0) at gstpad.c:4082
#45 0x00007ffff630fac3 in query_forward_func (pad=pad@entry=0x7fffd00912d0, data=data@entry=0x7fffe37fdbb0) at gstpad.c:3318
#46 0x00007ffff630d92e in gst_pad_forward (pad=pad@entry=0x140bb20, forward=forward@entry=0x7ffff630fa10 <query_forward_func>,
    user_data=user_data@entry=0x7fffe37fdbb0) at gstpad.c:2947
#47 0x00007ffff630dbb1 in gst_pad_query_default (pad=pad@entry=0x140bb20, parent=<optimized out>, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3385
#48 0x00007fffea843c2e in gst_play_sink_convert_bin_query (pad=0x140bb20, parent=<optimized out>, query=0x7fff6c56d1e0) at gstplaysinkconvertbin.c:525
#49 0x00007ffff630ed58 in gst_pad_query (pad=pad@entry=0x140bb20, query=query@entry=0x7fff6c56d1e0) at gstpad.c:3950
#50 0x00007ffff630f323 in gst_pad_peer_query (pad=0x7fffcc076730, query=query@entry=0x7fff6c56d1e0) at gstpad.c:4082
#51 0x00007fffe9677e3f in gst_queue_push_one (queue=0x7fffa8765000) at gstqueue.c:1443
#52 gst_queue_loop (pad=<optimized out>) at gstqueue.c:1506
#53 0x00007ffff633c851 in gst_task_func (task=0x7fffd8023290) at gsttask.c:334
#54 0x00007ffff663955e in ?? () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#55 0x00007ffff6638bc5 in ?? () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#56 0x00007ffff55286fa in start_thread (arg=0x7fffe37fe700) at pthread_create.c:333
#57 0x00007ffff49bdb5d in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:109


Just a not that I m stretching the player to reproduce these issues ( i.e I m reloading  constantly between every 0.1 - 0.3 seconds )."""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-02 22:51:17', 'AEDT'),
                text="""That may be a race somewhere ;)

That backtrace requires more context in the form of a at least a full (GST_DEBUG=*:7) debug log of the run that failed."""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-02 23:07:00', 'AEDT'),
                text="""This seems pretty difficult to reproduce with GST_DEBUG=*:7 even if I trigger reloads evert 0.05 of a second which could I suppose indicate a racing issue that does not manifest that easily in this case due to logging delays.. ??

Any tricks/ideas to force it somehow??"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-02 23:16:30', 'AEDT'),
                attachment_id=342712,
                text="""Created attachment 342712
GST_DEBUG=:*gl*:7"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-02 23:17:02', 'AEDT'),
                text="""Managed to reproduce it with GST_DEBUG=*gl*:7"""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-05 18:07:03', 'AEDT'),
                text="""This is a refcount bug, gl*:7 isn't enough for them.  We need at least a GST_DEBUG=gl*:7, GST_REFCOUNTING:7 log

As for tricks, try different computers, add small sleeps everywhere around the problematic code (pretty much any function mentioned before everything's destroyed in the log above).

Question, does it happen if you make the time between reloads longer? i.e. a second or two.  This might be a race between startup and shutdown."""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-05 19:51:15', 'AEDT'),
                attachment_id=342928,
                text="""Created attachment 342928
GST_DEBUG=gl*:7,GST_REFCOUNTING:6"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-05 19:53:08', 'AEDT'),
                text="""It seems really difficult to reproduce it with verbose logging. So far I managed to reproduce only with GST_REFCOUNTING=6. See attachment. I will keep trying to get a more verbose output.

If I trigger reloads between 1.0 - 2.0 seconds it doesn't seem to manifest.. At least the last 20mins that I have it running.."""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-05 20:17:13', 'AEDT'),
                text="""Right, so very much pointing to a start/stop race then.

I don't think the extra GST_REFCOUNTING level is going to help much then without context of other debug categories.  We are going to to have to look at other categories.  Try with gl*:7,basetransform:5.  At this point it's very much a guess and check game trying to find a category that will reproduce the race easily enough that also provides enough information to be able to find the cause."""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-05 20:26:56', 'AEDT'),
                text="""Got something new now with just GST_DEBUG=basetransform:5

#0  0x00007ffff6618a6b in g_logv () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#1  0x00007ffff6618bdf in g_log () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#2  0x00007ffff631e5ec in gst_poll_write_control (set=<optimized out>) at gstpoll.c:1658
#3  0x00007ffff62d6d8d in gst_buffer_pool_init (pool=0x7fff71db8f20) at gstbufferpool.c:180
#4  0x00007ffff690c317 in g_type_create_instance () from /usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0
#5  0x00007ffff68ee31b in ?? () from /usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0
#6  0x00007ffff68efc01 in g_object_newv () from /usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0
#7  0x00007ffff68f0534 in g_object_new () from /usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0
#8  0x00007ffff5bcd302 in gst_video_buffer_pool_new () at gstvideopool.c:283
#9  0x00007ffff5bd2edf in gst_video_decoder_decide_allocation_default (decoder=0x7fffc0019d40, query=0x7fff3df3ec00) at gstvideodecoder.c:3601
#10 0x00007fffe194a26a in gst_ffmpegviddec_decide_allocation (decoder=0x7fffc0019d40, query=0x7fff3df3ec00) at gstavviddec.c:1789
#11 0x00007ffff5bd2647 in gst_video_decoder_negotiate_pool (decoder=0x7fffc0019d40, caps=<optimized out>) at gstvideodecoder.c:3686
#12 0x00007ffff5bd8af0 in gst_video_decoder_negotiate_unlocked (decoder=0x7fffc0019d40) at gstvideodecoder.c:3835
#13 gst_video_decoder_allocate_output_frame (decoder=decoder@entry=0x7fffc0019d40, frame=frame@entry=0x7fffce51fcd0) at gstvideodecoder.c:3979
#14 0x00007fffe194a6ff in get_output_buffer (ffmpegdec=0x7fffc0019d40, frame=0x7fffce51fcd0) at gstavviddec.c:1207
#15 0x00007fffe194b83e in gst_ffmpegviddec_video_frame (ret=0x7fffe3ffe3ec, frame=0x7fff8eb98a30, have_data=0x7fffe3ffe3e8, size=3461477728,
    data=0x7ffff65c65b8 <_gst_debug_min> "\005", ffmpegdec=0x7fffc0019d40) at gstavviddec.c:1393
#16 gst_ffmpegviddec_frame (ffmpegdec=ffmpegdec@entry=0x7fffc0019d40, data=data@entry=0x7fffd8042cb0 "", size=size@entry=2208, have_data=have_data@entry=0x7fffe3ffe3e8,
    frame=frame@entry=0x7fff8eb98a30, ret=ret@entry=0x7fffe3ffe3ec) at gstavviddec.c:1528
#17 0x00007fffe194c74a in gst_ffmpegviddec_handle_frame (decoder=0x7fffc0019d40, frame=0x7fff8eb98a30) at gstavviddec.c:1641
#18 0x00007ffff5bce186 in gst_video_decoder_decode_frame (decoder=decoder@entry=0x7fffc0019d40, frame=0x7fff8eb98a30) at gstvideodecoder.c:3389
#19 0x00007ffff5bd0c92 in gst_video_decoder_chain_forward (decoder=decoder@entry=0x7fffc0019d40, buf=buf@entry=0x7fff776c65f0, at_eos=at_eos@entry=0)
    at gstvideodecoder.c:2131
#20 0x00007ffff5bd1323 in gst_video_decoder_chain (pad=<optimized out>, parent=0x7fffc0019d40, buf=0x7fff776c65f0) at gstvideodecoder.c:2443
#21 0x00007ffff63089ff in gst_pad_chain_data_unchecked (data=0x7fff776c65f0, type=4112, pad=0x7fffd814c4b0) at gstpad.c:4205
#22 gst_pad_push_data (pad=pad@entry=0x7fffcc0146f0, type=type@entry=4112, data=data@entry=0x7fff776c65f0) at gstpad.c:4457
#23 0x00007ffff6310ad3 in gst_pad_push (pad=0x7fffcc0146f0, buffer=0x7fff776c65f0) at gstpad.c:4576
#24 0x00007ffff606730d in gst_base_transform_chain (pad=<optimized out>, parent=0x14047b0, buffer=<optimized out>) at gstbasetransform.c:2372
#25 0x00007ffff63089ff in gst_pad_chain_data_unchecked (data=0x7fff776c65f0, type=4112, pad=0x7fffcc014b70) at gstpad.c:4205
#26 gst_pad_push_data (pad=pad@entry=0x7fffcc077b70, type=type@entry=4112, data=data@entry=0x7fff776c65f0) at gstpad.c:4457
#27 0x00007ffff6310ad3 in gst_pad_push (pad=0x7fffcc077b70, buffer=buffer@entry=0x7fff776c65f0) at gstpad.c:4576
#28 0x00007ffff604b169 in gst_base_parse_push_frame (parse=parse@entry=0x7fff2abd2a60, frame=frame@entry=0x7fffd80e3de0) at gstbaseparse.c:2543
#29 0x00007ffff604f09f in gst_base_parse_handle_and_push_frame (frame=0x7fffd80e3de0, parse=0x7fff2abd2a60) at gstbaseparse.c:2360
#30 gst_base_parse_finish_frame (parse=parse@entry=0x7fff2abd2a60, frame=frame@entry=0x7fffd80e3de0, size=<optimized out>) at gstbaseparse.c:2701
#31 0x00007fffe836a6d7 in gst_h264_parse_handle_frame_packetized (frame=0x7fffd80e3de0, parse=0x7fff2abd2a60) at gsth264parse.c:1020
#32 gst_h264_parse_handle_frame (parse=0x7fff2abd2a60, frame=0x7fffd80e3de0, skipsize=<optimized out>) at gsth264parse.c:1065
#33 0x00007ffff6047500 in gst_base_parse_handle_buffer (parse=parse@entry=0x7fff2abd2a60, buffer=<optimized out>, skip=skip@entry=0x7fffe3ffebd8,
    flushed=flushed@entry=0x7fffe3ffebdc) at gstbaseparse.c:2168
#34 0x00007ffff604be7e in gst_base_parse_chain (pad=<optimized out>, parent=0x7fff2abd2a60, buffer=<optimized out>) at gstbaseparse.c:3240
#35 0x00007ffff63089ff in gst_pad_chain_data_unchecked (data=0x7ffeecc75c60, type=4112, pad=0x7fffcc0144b0) at gstpad.c:4205
#36 gst_pad_push_data (pad=pad@entry=0x7fff8c01e4b0, type=type@entry=4112, data=data@entry=0x7ffeecc75c60) at gstpad.c:4457
#37 0x00007ffff6310ad3 in gst_pad_push (pad=0x7fff8c01e4b0, buffer=buffer@entry=0x7ffeecc75c60) at gstpad.c:4576
#38 0x00007fffe96732fc in gst_single_queue_push_one (allow_drop=<synthetic pointer>, object=0x7ffeecc75c60, sq=0x7fffc00fdb10, mq=0x7fffc000f020) at gstmultiqueue.c:1611
#39 gst_multi_queue_loop (pad=<optimized out>) at gstmultiqueue.c:1923
#40 0x00007ffff633c851 in gst_task_func (task=0x7fff8c020ef0) at gsttask.c:334
#41 0x00007ffff663955e in ?? () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#42 0x00007ffff6638bc5 in ?? () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
#43 0x00007ffff55286fa in start_thread (arg=0x7fffe3fff700) at pthread_create.c:333
#44 0x00007ffff49bdb5d in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:109"""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-05 20:32:06', 'AEDT'),
                text="""That's seemingly unrelated.  Is the code valgrind clean?"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-05 20:33:36', 'AEDT'),
                text="""Can you suggest a way to run through valgrind without getting false positives?"""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-05 20:52:12', 'AEDT'),
                text="""If you just run:

valgrind cmd to run

it will output all occurrences of any read/write to invalid locations.  Chances are extremely slim that any of those are not actual bugs."""
            ), make_comment(
                author='ystreet00@gmail.com',
                creation_time=make_datetime('2017-01-05 20:52:37', 'AEDT'),
                text="""We're not interested in if memory was leaked or not here."""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-05 20:58:47', 'AEDT'),
                text="""I had a run with :

G_SLICE=always-malloc
G_DEBUG=gc-friendly

valgrind ./Application

and the only thing it reports is the following during runtime :

Pipeline state changed from : NULL to READY with pending VOID_PENDING
==9989== Thread 23 gstglcontext:
==9989== Conditional jump or move depends on uninitialised value(s)
==9989==    at 0x10DDFFBF: ??? (in /usr/lib/x86_64-linux-gnu/libnvidia-glcore.so.367.35)
==9989==    by 0x10DE0DE8: ??? (in /usr/lib/x86_64-linux-gnu/libnvidia-glcore.so.367.35)
==9989==    by 0x1092AF46: ??? (in /usr/lib/x86_64-linux-gnu/libnvidia-glcore.so.367.35)
==9989==    by 0x70AED34: gst_gl_query_end (gstglquery.c:282)
==9989==    by 0x7090DF5: gst_gl_memory_texsubimage (gstglmemory.c:541)
==9989==    by 0x7093727: _upload_pbo_memory (gstglmemorypbo.c:151)
==9989==    by 0x7093727: _gl_mem_map_gpu_access (gstglmemorypbo.c:338)
==9989==    by 0x7093727: _gl_mem_map (gstglmemorypbo.c:362)
==9989==    by 0x708E135: _map_data_gl (gstglbasememory.c:261)
==9989==    by 0x708BE17: gst_gl_context_thread_add (gstglcontext.c:1537)
==9989==    by 0x708DD7D: _mem_map_full (gstglbasememory.c:295)
==9989==    by 0x66B64DF: gst_memory_map (gstmemory.c:303)
==9989==    by 0x709E400: _do_convert_one_view (gstglcolorconvert.c:2174)
==9989==    by 0x70A0749: _do_convert (gstglcolorconvert.c:2422)
==9989==
Pipeline state changed from : READY to PAUSED with pending VOID_PENDING
Playing: 0
Pipeline state about to change from : NULL to READY
Pipeline state changed SUCCESSFULLY from : NULL to READY
Pipeline state changed from : NULL to READY with pending VOID_PENDING
Need context gst.gl.GLDisplay from element convert
Need context gst.gl.app_context from element convert
Pipeline state about to change from : READY to PAUSED
Pipeline state change will happen ASYNC from : READY to PAUSED
==9989== Conditional jump or move depends on uninitialised value(s)
==9989==    at 0x10DDFFBF: ??? (in /usr/lib/x86_64-linux-gnu/libnvidia-glcore.so.367.35)
==9989==    by 0x10DE0DE8: ??? (in /usr/lib/x86_64-linux-gnu/libnvidia-glcore.so.367.35)
==9989==    by 0x7090D85: gst_gl_memory_texsubimage (gstglmemory.c:537)
==9989==    by 0x7093727: _upload_pbo_memory (gstglmemorypbo.c:151)
==9989==    by 0x7093727: _gl_mem_map_gpu_access (gstglmemorypbo.c:338)
==9989==    by 0x7093727: _gl_mem_map (gstglmemorypbo.c:362)
==9989==    by 0x708E135: _map_data_gl (gstglbasememory.c:261)
==9989==    by 0x708BE17: gst_gl_context_thread_add (gstglcontext.c:1537)
==9989==    by 0x708DD7D: _mem_map_full (gstglbasememory.c:295)
==9989==    by 0x66B64DF: gst_memory_map (gstmemory.c:303)
==9989==    by 0x709E400: _do_convert_one_view (gstglcolorconvert.c:2174)
==9989==    by 0x70A0749: _do_convert (gstglcolorconvert.c:2422)
==9989==    by 0x70A3D22: _run_message_sync (gstglwindow.c:617)
==9989==    by 0x70A3CC1: _run_message_async (gstglwindow.c:684)
==9989=="""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-05 21:40:09', 'AEDT'),
                attachment_id=342937,
                text="""Created attachment 342937
Full valgrind log

Also a full log in case its more helpful.

Configured with:

G_SLICE=always-malloc
G_DEBUG=gc-friendly

valgrind -v --leak-check=full --suppressions=./gst.supp"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-07 20:44:15', 'AEDT'),
                attachment_id=343072,
                text="""Created attachment 343072
GST_DEBUG=gl*:7,basetransform:5

part1"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-07 20:45:33', 'AEDT'),
                attachment_id=343073,
                text="""Created attachment 343073
GST_DEBUG=gl*:7,basetransform:5

part2"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-07 20:47:34', 'AEDT'),
                attachment_id=343074,
                text="""Created attachment 343074
GST_DEBUG=gl*:7,basetransform:5

part3"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-07 20:49:25', 'AEDT'),
                attachment_id=343075,
                text="""Created attachment 343075
GST_DEBUG=gl*:7,basetransform:5

part 4"""
            ), make_comment(
                author='petroskataras@gmail.com',
                creation_time=make_datetime('2017-01-07 20:51:44', 'AEDT'),
                text="""Added some sleep and managed to hit the same issue with GST_DEBUG=gl*:7,basetransform:5.

I had to split the log since it was too big otherwise so sorry for the multiple attachments."""
            )
        ]
    )

bug797075 = make_bug(797075,
        creation_time=make_datetime('2018-09-05 06:50:33', 'AEST'),
        last_change_time=make_datetime('2018-09-10 23:39:06', 'AEST'),
        version='git master',
        status='NEEDINFO',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='lists@svrinformatica.it',
        summary='hls segfaults',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['nicolas@ndufresne.ca', 'slomo@coaxion.net'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2018-09-05 06:50:33', 'AEST'),
                text="""I see some crashs using playbin for an hls stream

- crash 1:

#0  0x00007fffef3cca26 in malloc () at /usr/lib/libc.so.6
#1  0x00007fffef453b0f in __vasprintf_chk () at /usr/lib/libc.so.6
#2  0x00007ffff7c54c5a in g_vasprintf () at /usr/lib/libglib-2.0.so.0
#3  0x00007ffff7c2e0de in g_strdup_vprintf () at /usr/lib/libglib-2.0.so.0
#4  0x00007ffff7c2e19a in g_strdup_printf () at /usr/lib/libglib-2.0.so.0
#5  0x00007fff657d504e in uri_join (uri1=<optimized out>, uri2=0x7ffed0017015 "3304.ts")
    at ../subprojects/gst-plugins-bad/ext/hls/m3u8.c:1111
#6  0x00007fff657d583d in gst_m3u8_update (self=0x7ffed0016db0, data=<optimized out>)
    at ../subprojects/gst-plugins-bad/ext/hls/m3u8.c:522
#7  0x00007fff657cf5c9 in gst_hls_demux_update_playlist (demux=demux@entry=0x7ffef00ceec0, update=update@entry=1, err=err@entry=0x0) at ../subprojects/gst-plugins-bad/ext/hls/gsthlsdemux.c:1459
#8  0x00007fff657cfcdf in gst_hls_demux_change_playlist (demux=demux@entry=0x7ffef00ceec0, max_bitrate=<optimized out>, changed=changed@entry=0x7ffee67fb2e4) at ../subprojects/gst-plugins-bad/ext/hls/gsthlsdemux.c:1599
#9  0x00007fff657d0edd in gst_hls_demux_select_bitrate (stream=<optimized out>, bitrate=<optimized out>)
    at ../subprojects/gst-plugins-bad/ext/hls/gsthlsdemux.c:1149
#10 0x00007fff657c0c99 in gst_adaptive_demux_stream_select_bitrate (bitrate=<optimized out>, stream=0x7fff1412d830, demux=0x7ffef00ceec0) at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:4280
#11 0x00007fff657c0c99 in gst_adaptive_demux_stream_advance_fragment_unlocked (duration=<optimized out>, stream=0x7fff1412d830, demux=0x7ffef00ceec0) at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:4232
#12 0x00007fff657c0c99 in gst_adaptive_demux_stream_advance_fragment (demux=demux@entry=0x7ffef00ceec0, stream=stream@entry=0x7fff1412d830, duration=<optimized out>) at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:4153
#13 0x00007fff657ce726 in gst_hls_demux_finish_fragment (demux=0x7ffef00ceec0, stream=0x7fff1412d830)
    at ../subprojects/gst-plugins-bad/ext/hls/gsthlsdemux.c:953
#14 0x00007fff657b477f in gst_adaptive_demux_eos_handling (stream=stream@entry=0x7fff1412d830)
    at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:2730
#15 0x00007fff657b4af9 in _src_event (pad=<optimized out>, parent=<optimized out>, event=0x7ffed4003d10)
    at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:2748
#16 0x00007ffff7dee277 in gst_pad_send_event_unchecked (pad=pad@entry=0x7fff1412acd0, event=event@entry=0x7ffed4003d10, type=<optimized out>, type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at ../subprojects/gstreamer/gst/gstpad.c:5757
#17 0x00007ffff7dee7c4 in gst_pad_push_event_unchecked (pad=pad@entry=0x7fff1404f5b0, event=0x7ffed4003d10, type=type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at ../subprojects/gstreamer/gst/gstpad.c:5402
#18 0x00007ffff7deec34 in push_sticky (pad=pad@entry=0x7fff1404f5b0, ev=ev@entry=0x7ffee67fb600, user_data=user_data@entry=0x7---Type <return> to continue, or q <return> to quit---
ffee67fb670) at ../subprojects/gstreamer/gst/gstevent.h:438
#19 0x00007ffff7dec548 in events_foreach (pad=pad@entry=0x7fff1404f5b0, func=func@entry=0x7ffff7deebe0 <push_sticky>, user_data=user_data@entry=0x7ffee67fb670) at ../subprojects/gstreamer/gst/gstpad.c:608
#20 0x00007ffff7df7b71 in check_sticky (event=0x7ffed4003d10, pad=0x7fff1404f5b0)
    at ../subprojects/gstreamer/gst/gstpad.c:3977
#21 0x00007ffff7df7b71 in gst_pad_push_event (pad=pad@entry=0x7fff1404f5b0, event=0x7ffed4003d10)
    at ../subprojects/gstreamer/gst/gstpad.c:5533
#22 0x00007ffff7df80f4 in event_forward_func (pad=pad@entry=0x7fff1404f5b0, data=data@entry=0x7ffee67fb770)
    at ../subprojects/gstreamer/gst/gstevent.h:438
#23 0x00007ffff7df446e in gst_pad_forward (pad=pad@entry=0x555556151d00, forward=forward@entry=0x7ffff7df8030 <event_forward_func>, user_data=user_data@entry=0x7ffee67fb770) at ../subprojects/gstreamer/gst/gstpad.c:3004
#24 0x00007ffff7df45b5 in gst_pad_event_default (pad=0x555556151d00, parent=<optimized out>, event=0x7ffed4003d10)
    at ../subprojects/gstreamer/gst/gstpad.c:3101
#25 0x00007ffff7dee277 in gst_pad_send_event_unchecked (pad=pad@entry=0x555556151d00, event=event@entry=0x7ffed4003d10, type=<optimized out>, type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at ../subprojects/gstreamer/gst/gstpad.c:5757
#26 0x00007ffff7dee7c4 in gst_pad_push_event_unchecked (pad=pad@entry=0x7fff1412a830, event=0x7ffed4003d10, type=type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at ../subprojects/gstreamer/gst/gstpad.c:5402
#27 0x00007ffff7deec34 in push_sticky (pad=pad@entry=0x7fff1412a830, ev=ev@entry=0x7ffee67fb970, user_data=user_data@entry=0x7ffee67fb9e0) at ../subprojects/gstreamer/gst/gstevent.h:438
#28 0x00007ffff7dec548 in events_foreach (pad=pad@entry=0x7fff1412a830, func=func@entry=0x7ffff7deebe0 <push_sticky>, user_data=user_data@entry=0x7ffee67fb9e0) at ../subprojects/gstreamer/gst/gstpad.c:608
#29 0x00007ffff7df7b71 in check_sticky (event=0x7ffed4003d10, pad=0x7fff1412a830)
    at ../subprojects/gstreamer/gst/gstpad.c:3977
#30 0x00007ffff7df7b71 in gst_pad_push_event (pad=0x7fff1412a830, event=event@entry=0x7ffed4003d10)
    at ../subprojects/gstreamer/gst/gstpad.c:5533
#31 0x00007fff8806626b in gst_queue_push_one (queue=0x7ffee0016010)
    at ../subprojects/gstreamer/plugins/elements/gstqueue.c:1455
#32 0x00007fff8806626b in gst_queue_loop (pad=<optimized out>) at ../subprojects/gstreamer/plugins/elements/gstqueue.c:1537
#33 0x00007ffff7e24751 in gst_task_func (task=0x7fff1404c710) at ../subprojects/gstreamer/gst/gsttask.c:328
#34 0x00007ffff7c37bf6 in  () at /usr/lib/libglib-2.0.so.0
#35 0x00007ffff7c371ea in  () at /usr/lib/libglib-2.0.so.0
#36 0x00007fffef840a9d in start_thread () at /usr/lib/libpthread.so.0


crash 2:

#0  0x00007fffef4a6667 in __strlen_avx2 () at /usr/lib/libc.so.6
#1  0x00007ffff7c2df54 in g_strdup () at /usr/lib/libglib-2.0.so.0
#2  0x00007fff7d7cff10 in gst_hls_demux_update_fragment_info (stream=0x7ffed000e420)
    at ../subprojects/gst-plugins-bad/ext/hls/gsthlsdemux.c:1102
#3  0x00007fff7d7b3ba1 in gst_adaptive_demux_stream_update_fragment_info (demux=demux@entry=0x7fff141aa1f0, stream=stream@entry=0x7ffed000e420) at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:4303
#4  0x00007fff7d7bde74 in gst_adaptive_demux_stream_download_loop (stream=0x7ffed000e420)
    at ../subprojects/gst-plugins-bad/gst-libs/gst/adaptivedemux/gstadaptivedemux.c:3716
#5  0x00007ffff7e24751 in gst_task_func (task=0x7ffee80c1dd0) at ../subprojects/gstreamer/gst/gsttask.c:328
#6  0x00007ffff7c37bf6 in  () at /usr/lib/libglib-2.0.so.0
#7  0x00007ffff7c371ea in  () at /usr/lib/libglib-2.0.so.0
#8  0x00007fffef840a9d in start_thread () at /usr/lib/libpthread.so.0
#9  0x00007fffef442a43 in clone () at /usr/lib/libc.so.6


the same stream works with no crash in ffplay"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2018-09-05 07:48:28', 'AEST'),
                text="""Do you have an uri to share so we can test ? Or the m3u8 file ?"""
            ), make_comment(
                author='slomo@coaxion.net',
                creation_time=make_datetime('2018-09-05 17:29:23', 'AEST'),
                text="""Or alternatively backtraces without optimizations, or you'll have to debug yourself first why those string functions are crashing (which of the pointers is wrong, and why)."""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2018-09-07 17:51:59', 'AEST'),
                text="""the problem does not happen anymore so I cannot reproduce now, I'll have to wait for few days ... it happens periodically

I have no control on the server, anyway when this edge case happen then GStreamer crashs every few minutes so basically is unable to play the hls stream while ffmpeg and flowplayer work.

Probably the m3u8 file is corrupted and other players redownload it, I remember some 404 and consecutive m3u8 download in the browser developer console.

For your info GStreamer has more delay than flowplayer and ffmpeg too: the server store 2 segments, I suppose GStreamer starts to play from the first one, in ffmpeg you can set live_start_index to start from the last one, this help a lot if hls is a "live" streaming"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2018-09-08 23:58:57', 'AEST'),
                text="""it probably happens because sometime the m3u8 is a binary file,

for example

curl "<url to m3u8>"

returns

Warning: Binary output can mess up your terminal. Use "--output -" to tell
Warning: curl to output it to your terminal anyway, or consider "--output
Warning: <FILE>" to save to a file.

this is obvious a server error, but if you try to download the m3u8 again after few milliseconds it works as expected.

Another strage behaviour of this broken server:

it returns something like this as m3u8

#EXTM3U
#EXT-X-VERSION:3
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=420000
live2_lq/index.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=670000
live2_mq/index.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=1100000
live2_hq/index.m3u8

sometime the links to live2_lq/index.m3u8 and live2_mq/index.m3u8 return 404 http error while the hq link is ok"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2018-09-09 01:28:16', 'AEST'),
                text="""It's not calling into the binary case. gst_hls_src_buf_to_utf8_playlist() runs g_utf8_validate() which would detect this case.

After that, we clearly have gone passed g_str_has_prefix (data, "#EXTM3U")) and g_strrstr (data, "\n#EXT-X-STREAM-INF:") validation, so the file is probably not that corrupted.

So I can only blame the hand written parser that follow this code. If you manage to reproduce, got to frame 6, and and print "data". Attach the output here. We can then plug that data into our unit test and easily reproduce (and fix) that bug.

For the refefence:
 gdb) f 6
 gdb) printf "%s",data"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2018-09-09 01:36:56', 'AEST'),
                text="""Another note, at the line of the crash, gst_uri_is_valid() returns success, so the data does looks like a valid URI. The code also thinks we have a relative URI. As we crash in malloc, the size passed to malloc should be traced. It also possible that it's some other memory corruption that lead to that ..."""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2018-09-10 21:47:16', 'AEST'),
                text="""here is another crash with hopefully the needed info

#0  0x00007ffff7355a26 in malloc () at /usr/lib/libc.so.6
#1  0x00007ffff73dcb0f in __vasprintf_chk () at /usr/lib/libc.so.6
#2  0x00007ffff74e32da in g_vasprintf () at /usr/lib/libglib-2.0.so.0
#3  0x00007ffff75060d6 in g_string_append_vprintf () at /usr/lib/libglib-2.0.so.0
#4  0x00007ffff7506225 in g_string_append_printf () at /usr/lib/libglib-2.0.so.0
#5  0x00007ffff751e94c in g_log_writer_format_fields () at /usr/lib/libglib-2.0.so.0
#6  0x00007ffff75248b6 in g_log_writer_standard_streams () at /usr/lib/libglib-2.0.so.0
#7  0x00007ffff75249c2 in g_log_writer_default () at /usr/lib/libglib-2.0.so.0
#8  0x00007ffff751eaa9 in g_log_structured_array () at /usr/lib/libglib-2.0.so.0
#9  0x00007ffff751eb9e in g_log_default_handler () at /usr/lib/libglib-2.0.so.0
#10 0x00007ffff751f341 in g_logv () at /usr/lib/libglib-2.0.so.0
#11 0x00007ffff751f560 in g_log () at /usr/lib/libglib-2.0.so.0
#12 0x00007ffff7f29b39 in gst_structure_set_field (structure=0x7fffb4005700, field=0x7fffe0ba0450)
    at gststructure.c:801
#13 0x00007ffff7f29ded in gst_structure_set_valist_internal (structure=<optimized out>, fieldname=<optimized out>, varargs=0x7fffe0ba0520) at gststructure.c:612
#14 0x00007ffff7f2b155 in gst_structure_new_valist (name=<optimized out>, firstfield=0x7ffff0400180 "manifest-uri", varargs=varargs@entry=0x7fffe0ba0520) at gststructure.c:286
#15 0x00007ffff7f2b1f4 in gst_structure_new (name=name@entry=0x7ffff040018d "adaptive-streaming-statistics", firstfield=firstfield@entry=0x7ffff0400180 "manifest-uri") at gststructure.c:255
#16 0x00007ffff03fa226 in gst_hls_demux_change_playlist (demux=demux@entry=0x7fffec590130, max_bitrate=<optimized out>, changed=changed@entry=0x7fffe0ba0694) at gsthlsdemux.c:1605
#17 0x00007ffff03fb2dd in gst_hls_demux_select_bitrate (stream=<optimized out>, bitrate=<optimized out>)
    at gsthlsdemux.c:1149
#18 0x00007ffff005ac99 in gst_adaptive_demux_stream_select_bitrate (bitrate=<optimized out>, stream=0x7fffc800f9e0, demux=0x7fffec590130) at gstadaptivedemux.c:4280
#19 0x00007ffff005ac99 in gst_adaptive_demux_stream_advance_fragment_unlocked (duration=<optimized out>, stream=0x7fffc800f9e0, demux=0x7fffec590130) at gstadaptivedemux.c:4232
#20 0x00007ffff005ac99 in gst_adaptive_demux_stream_advance_fragment (demux=demux@entry=0x7fffec590130, stream=stream@entry=0x7fffc800f9e0, duration=<optimized out>) at gstadaptivedemux.c:4153
#21 0x00007ffff03f8b26 in gst_hls_demux_finish_fragment (demux=0x7fffec590130, stream=0x7fffc800f9e0)
    at gsthlsdemux.c:953
---Type <return> to continue, or q <return> to quit---
#22 0x00007ffff004e77f in gst_adaptive_demux_eos_handling (stream=stream@entry=0x7fffc800f9e0)
    at gstadaptivedemux.c:2730
#23 0x00007ffff004eaf9 in _src_event (pad=<optimized out>, parent=<optimized out>, event=0x7fffc0004b30)
    at gstadaptivedemux.c:2748
#24 0x00007ffff7f004b7 in gst_pad_send_event_unchecked (pad=pad@entry=0x7fffb801d560, event=event@entry=0x7fffc0004b30, type=<optimized out>, type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at gstpad.c:5757
#25 0x00007ffff7f00a04 in gst_pad_push_event_unchecked (pad=pad@entry=0x7fffec4ab5b0, event=0x7fffc0004b30, type=type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at gstpad.c:5402
#26 0x00007ffff7f00e74 in push_sticky (pad=pad@entry=0x7fffec4ab5b0, ev=ev@entry=0x7fffe0ba09b0, user_data=user_data@entry=0x7fffe0ba0a20) at ../gst/gstevent.h:438
#27 0x00007ffff7efe788 in events_foreach (pad=pad@entry=0x7fffec4ab5b0, func=func@entry=0x7ffff7f00e20 <push_sticky>, user_data=user_data@entry=0x7fffe0ba0a20) at gstpad.c:608
#28 0x00007ffff7f09db1 in check_sticky (event=0x7fffc0004b30, pad=0x7fffec4ab5b0) at gstpad.c:3977
#29 0x00007ffff7f09db1 in gst_pad_push_event (pad=pad@entry=0x7fffec4ab5b0, event=0x7fffc0004b30)
    at gstpad.c:5533
#30 0x00007ffff7f0a334 in event_forward_func (pad=pad@entry=0x7fffec4ab5b0, data=data@entry=0x7fffe0ba0b20)
    at ../gst/gstevent.h:438
#31 0x00007ffff7f066ae in gst_pad_forward (pad=pad@entry=0x7fffb803a7a0, forward=forward@entry=0x7ffff7f0a270 <event_forward_func>, user_data=user_data@entry=0x7fffe0ba0b20) at gstpad.c:3004
#32 0x00007ffff7f067f5 in gst_pad_event_default (pad=0x7fffb803a7a0, parent=<optimized out>, event=0x7fffc0004b30) at gstpad.c:3101
#33 0x00007ffff7f004b7 in gst_pad_send_event_unchecked (pad=pad@entry=0x7fffb803a7a0, event=event@entry=0x7fffc0004b30, type=<optimized out>, type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at gstpad.c:5757
#34 0x00007ffff7f00a04 in gst_pad_push_event_unchecked (pad=pad@entry=0x7fffb801d0c0, event=0x7fffc0004b30, type=type@entry=GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) at gstpad.c:5402
#35 0x00007ffff7f00e74 in push_sticky (pad=pad@entry=0x7fffb801d0c0, ev=ev@entry=0x7fffe0ba0d20, user_data=user_data@entry=0x7fffe0ba0d90) at ../gst/gstevent.h:438
#36 0x00007ffff7efe788 in events_foreach (pad=pad@entry=0x7fffb801d0c0, func=func@entry=0x7ffff7f00e20 <push_sticky>, user_data=user_data@entry=0x7fffe0ba0d90) at gstpad.c:608
#37 0x00007ffff7f09db1 in check_sticky (event=0x7fffc0004b30, pad=0x7fffb801d0c0) at gstpad.c:3977
#38 0x00007ffff7f09db1 in gst_pad_push_event (pad=0x7fffb801d0c0, event=event@entry=0x7fffc0004b30)
    at gstpad.c:5533
---Type <return> to continue, or q <return> to quit---
#39 0x00007ffff2a8570b in gst_queue_push_one (queue=0x7fffd000cc60) at gstqueue.c:1455
#40 0x00007ffff2a8570b in gst_queue_loop (pad=<optimized out>) at gstqueue.c:1537
#41 0x00007ffff7f36a61 in gst_task_func (task=0x7fffd000fcb0) at gsttask.c:328
#42 0x00007ffff7501bd6 in  () at /usr/lib/libglib-2.0.so.0
#43 0x00007ffff74fa3eb in  () at /usr/lib/libglib-2.0.so.0
#44 0x00007ffff749ba9d in start_thread () at /usr/lib/libpthread.so.0
#45 0x00007ffff73cba43 in clone () at /usr/lib/libc.so.6
(gdb) f 16
#16 0x00007ffff03fa226 in gst_hls_demux_change_playlist (demux=demux@entry=0x7fffec590130, max_bitrate=<optimized out>,
    changed=changed@entry=0x7fffe0ba0694) at gsthlsdemux.c:1605
1605        gst_element_post_message (GST_ELEMENT_CAST (demux)
printf "%s",uri
���(gdb)printf "%x",uri
d800c8c0(gdb) f 12
#12 0x00007ffff7f29b39 in gst_structure_set_field (structure=0x7fffb4005700, field=0x7fffe0ba0450) at gststructure.c:801
801       g_warning ("Trying to set string on %s field '%s', but string is not "
(gdb) printf "%s",field

g(gdb) printf "%x",field
e0ba0450(gdb)"""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2018-09-10 21:51:23', 'AEST'),
                text="""f 16
#16 0x00007ffff03fa226 in gst_hls_demux_change_playlist (demux=demux@entry=0x7fffec590130, max_bitrate=<optimized out>,
    changed=changed@entry=0x7fffe0ba0694) at gsthlsdemux.c:1605
1605        gst_element_post_message (GST_ELEMENT_CAST (demux)

printf "%s",uri
���

printf "%x",uri
d800c8c0

f 12
#12 0x00007ffff7f29b39 in gst_structure_set_field (structure=0x7fffb4005700, field=0x7fffe0ba0450) at gststructure.c:801
801       g_warning ("Trying to set string on %s field '%s', but string is not "
printf "%s",field
g
printf "%x",field
e0ba0450(gdb)"""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2018-09-10 23:23:02', 'AEST'),
                text="""It's starting to look bad, the only thing these two have in common is malloc. Looks like memory corruption. We need to produce in valgrind (or similar) to figure out."""
            ), make_comment(
                author='lists@svrinformatica.it',
                creation_time=make_datetime('2018-09-10 23:39:06', 'AEST'),
                text="""Sorry I gave up, I switched to ffmpeg this weekend and now I use GStreamer only for audio and video sinks,

I'll try to find some spare time to provide the needed info but it is quite difficult since the stream is available only during working hours and most of the time hlsdemux hangs (see #797101) so it is a very time consuming task.

As soon as I have some spare time I would like to summarize the steps and the workarounds needed to correctly handle this hls "live" stream in my ffmpeg + gst app, maybe this will help to improve the hls support in GStreamer"""
            )
        ]
    )

bug797312 = make_bug(797312,
        creation_time=make_datetime('2018-10-21 02:57:51', 'AEDT'),
        last_change_time=make_datetime('2018-10-26 19:42:15', 'AEDT'),
        version='1.14.x',
        status='NEW',
        severity='normal',
        target_milestone='git master',
        keywords=[],
        creator='kreckel@ginac.de',
        summary='vaapih265dec: [radeon] GStreamer fails to play HEVC video',
        assigned_to='gstreamer-bugs@lists.freedesktop.org',
        cc=['bsreerenj@gmail.com', 'julien.isorce@gmail.com', 'nicolas@ndufresne.ca', 'vjaquez@igalia.com'],
        blocks=[],
        depends_on=[],
        see_also=[],
        comments=[
            make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-21 02:57:51', 'AEDT'),
                attachment_id=373979,
                text="""Created attachment 373979
Matroska file with x265 video where gst-launch-1.0 fails

The attached 1s video plays fine with other players.

$ gst-launch-1.0 playbin uri=file:///data/scratch/das_boot.mkv
Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
Got context from element 'vaapisink0': gst.vaapi.Display=context, gst.vaapi.Display=(GstVaapiDisplay)"\(GstVaapiDisplayGLX\)\ vaapidisplayglx0";
Redistribute latency...
Got context from element 'playsink': gst.vaapi.Display=context, gst.vaapi.Display=(GstVaapiDisplay)"\(GstVaapiDisplayGLX\)\ vaapidisplayglx0";
Redistribute latency...
ERROR: from element /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0/GstDecodeBin:decodebin0/GstMultiQueue:multiqueue0: Internal data stream error.
Additional debug info:
gstmultiqueue.c(2069): gst_multi_queue_loop (): /GstPlayBin:playbin0/GstURIDecodeBin:uridecodebin0/GstDecodeBin:decodebin0/GstMultiQueue:multiqueue0:
streaming stopped, reason error (-5)
ERROR: pipeline doesn't want to preroll.
Setting pipeline to NULL ...
Freeing pipeline ..."""
            ), make_comment(
                author='nicolas@ndufresne.ca',
                creation_time=make_datetime('2018-10-21 03:34:06', 'AEDT'),
                text="""I have tested this file withough vaapi support, and it works, so I'm moving the ticket component to gstreamer-vaapi. Then I have tested on 1.14.1 as shipped by Fedora (Intel i965 driver for Intel(R) Skylake - 2.1.0) and it also worked.

Then I tested on HW without HEVC support (Intel i965 driver for Intel(R) Ivybridge Mobile - 2.1.0) and the software fallback (FFMPEG) did work as expected. Which means I cannot reproduce the error you are seeing.

Please provide additional information such as VAAPI support details, as reported by vainfo utility, and confirm which version of GStreamer you are testing. Ideally, test against newer libva and vaapi driver, as the issue may already been resolved."""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-21 05:33:13', 'AEDT'),
                text="""(In reply to Nicolas Dufresne (ndufresne) from comment #1)
> Please provide additional information such as VAAPI support details, as
> reported by vainfo utility, and confirm which version of GStreamer you are
> testing. Ideally, test against newer libva and vaapi driver, as the issue
> may already been resolved.

This is libva-2.3.0 and GStreamer 1.14.4, both from Debian/testing:

$ gst-launch-1.0 --version
gst-launch-1.0 version 1.14.4
GStreamer 1.14.4
http://packages.qa.debian.org/gstreamer1.0
$ vainfo
libva info: VA-API version 1.3.0
libva info: va_getDriverName() returns 0
libva info: Trying to open /usr/lib/x86_64-linux-gnu/dri/radeonsi_drv_video.so
libva info: Found init function __vaDriverInit_1_2
libva info: va_openDriver() returns 0
vainfo: VA-API version: 1.3 (libva 2.2.0)
vainfo: Driver version: Mesa Gallium driver 18.1.7 for Radeon RX 560 Series (POLARIS11, DRM 3.26.0, 4.18.0-2-amd64, LLVM 6.0.1)
vainfo: Supported profile and entrypoints
      VAProfileMPEG2Simple            : VAEntrypointVLD
      VAProfileMPEG2Main              : VAEntrypointVLD
      VAProfileVC1Simple              : VAEntrypointVLD
      VAProfileVC1Main                : VAEntrypointVLD
      VAProfileVC1Advanced            : VAEntrypointVLD
      VAProfileH264ConstrainedBaseline: VAEntrypointVLD
      VAProfileH264ConstrainedBaseline: VAEntrypointEncSlice
      VAProfileH264Main               : VAEntrypointVLD
      VAProfileH264Main               : VAEntrypointEncSlice
      VAProfileH264High               : VAEntrypointVLD
      VAProfileH264High               : VAEntrypointEncSlice
      VAProfileHEVCMain               : VAEntrypointVLD
      VAProfileHEVCMain               : VAEntrypointEncSlice
      VAProfileHEVCMain10             : VAEntrypointVLD
      VAProfileJPEGBaseline           : VAEntrypointVLD
      VAProfileNone                   : VAEntrypointVideoProc"""
            ), make_comment(
                author='vjaquez@igalia.com',
                creation_time=make_datetime('2018-10-21 08:49:44', 'AEDT'),
                text="""As Nicolas said, the stream plays with my kabylake using intel-vaapi-driver and this is the first time I heard of someone playing HEVC with the gallium state tracker for radeon.

Please upload the log with GST_DEBUG=*:4,vaapi*:5"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-21 09:03:23', 'AEDT'),
                attachment_id=373980,
                text="""Created attachment 373980
gst-launch debug output with GST_DEBUG=*:4,vaapi*:5"""
            ), make_comment(
                author='vjaquez@igalia.com',
                creation_time=make_datetime('2018-10-21 18:27:31', 'AEDT'),
                attachment_id=373980,
                text="""Comment on attachment 373980
gst-launch debug output with GST_DEBUG=*:4,vaapi*:5

vaapi logs didn't show up. I guess the correct debug format is GST_DEBUG="vaapi*:5,*:4\""""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-21 20:27:20', 'AEDT'),
                text="""(In reply to Víctor Manuel Jáquez Leal from comment #5)
> vaapi logs didn't show up. I guess the correct debug format is
> GST_DEBUG="vaapi*:5,*:4"

Uhm, that didn't change the log.
I'm going to upload the log with GST_DEBUG="*:5" now."""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-21 20:30:59', 'AEDT'),
                attachment_id=373982,
                text="""Created attachment 373982
gst-launch debug output with GST_DEBUG=*:5"""
            ), make_comment(
                author='julien.isorce@gmail.com',
                creation_time=make_datetime('2018-10-23 17:34:18', 'AEDT'),
                text="""Can you try with GST_DEBUG=*vaapi*:6 ?"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-24 04:13:48', 'AEDT'),
                attachment_id=374014,
                text="""Created attachment 374014
gst-launch debug output with GST_DEBUG=*vaapi*:6

Setting GST_DEBUG to *vaapi*:6 doesn't produce more lines of debug output."""
            ), make_comment(
                author='julien.isorce@gmail.com',
                creation_time=make_datetime('2018-10-24 14:40:56', 'AEDT'),
                text="""Looks like a problem with gstvaapisink which does not restrict the caps to what it really supports.

Try playbin video-sink="videoconvert ! xvimagesink"

Try also try gst-launch-1.0 filesrc location=file:///data/scratch/das_boot.mkv ! matroskademux ! h265parse ! vaapidecode ! vaapipostproc ! "video/x-raw(memory:VASurface), format=(string)RGBA" ! vaapisink"""
            ), make_comment(
                author='vjaquez@igalia.com',
                creation_time=make_datetime('2018-10-24 21:45:20', 'AEDT'),
                text="""0:00:00.456349780 [335m28462[00m 0x7fe30803ea30 [37mDEBUG  [00m [00m               vaapi gstvaapisurface.c:376:gst_vaapi_surface_new_full:[00m size 992x574, format NV12, flags 0x00000000
0:00:00.456630547 [335m28462[00m 0x7fe30803ea30 [37mDEBUG  [00m [00m               vaapi gstvaapisurface.c:213:gst_vaapi_surface_create_full:[00m surface 0xe
0:00:00.456646808 [335m28462[00m 0x7fe30803ea30 [37mDEBUG  [00m [00m               vaapi gstvaapiutils.c:130:vaapi_check_status:[00m vaRenderPicture(): invalid VASurfaceID
0:00:00.456653059 [335m28462[00m 0x7fe30803ea30 [31;01mERROR  [00m [00m       vaapipostproc gstvaapipostproc.c:872:gst_vaapipostproc_process_vpp:<vaapipostproc0>[00m failed to apply VPP filters (error 2)

IIUC the problem is in vaapipostroc: it is unable to apply a filter in a surface.

Another test would be to export the environment variable GST_VAAPI_DISABLE_VPP=1"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-25 17:22:46', 'AEDT'),
                text="""(In reply to Julien Isorce from comment #10)
> Try also try gst-launch-1.0 filesrc
> location=file:///data/scratch/das_boot.mkv ! matroskademux ! h265parse !
> vaapidecode ! vaapipostproc ! "video/x-raw(memory:VASurface),
> format=(string)RGBA" ! vaapisink

No success.
WARNING: erroneous pipeline: no element "vaapidecode\""""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-25 17:26:37', 'AEDT'),
                attachment_id=374034,
                text="""Created attachment 374034
gst-launch playbin video-sink="videoconvert ! xvimagesink" debug output with GST_DEBUG=vaapi*:5,*:4

That didn't work either."""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-25 17:30:18', 'AEDT'),
                text="""(In reply to Víctor Manuel Jáquez Leal from comment #11)
> Another test would be to export the environment variabl GST_VAAPI_DISABLE_VPP=1

Success: That does play it!"""
            ), make_comment(
                author='vjaquez@igalia.com',
                creation_time=make_datetime('2018-10-26 02:39:27', 'AEDT'),
                text="""We would need to debug what's happening with the posprocessor.

One last log (without GST_VAAPI_DISABLE_VPP!) exporting the variable LIBVA_TRACE=~/vpp-failure

With it we would get the logs from VAAPI (but not for the driver, sadly)

And post it here :)

Thanks!"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-26 04:43:27', 'AEDT'),
                attachment_id=374037,
                text="""Created attachment 374037
vpp-failure.174006.thd-0x00001980"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-26 04:43:57', 'AEDT'),
                attachment_id=374038,
                text="""Created attachment 374038
/home/rbk/vpp-failure.174006.thd-0x00001983"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-26 04:44:33', 'AEDT'),
                attachment_id=374039,
                text="""Created attachment 374039
vpp-failure.174006.thd-0x00001984"""
            ), make_comment(
                author='kreckel@ginac.de',
                creation_time=make_datetime('2018-10-26 04:44:52', 'AEDT'),
                attachment_id=374040,
                text="""Created attachment 374040
vpp-failure.174006.thd-0x0000199b"""
            )
        ]
    )



# Mocked bugs to avoid https://bugzilla.redhat.com/show_bug.cgi?id=650114
MOCKED = {
    # gst-libav
    796466 : bug796466,
    # gst-plugins-good
    783516 : bug783516,
    788777 : bug788777,
    # gst-plugins-bad
    732643 : bug732643,
    756073 : bug756073,
    768079 : bug768079,
    769268 : bug769268,
    792232 : bug792232,
    793383 : bug793383,
    797075 : bug797075,
    # gst-plugins-base
    679462 : bug679462,
    740784 : bug740784,
    776540 : bug776540,
    # gstreamer-vaapi
    797312 : bug797312,
}
