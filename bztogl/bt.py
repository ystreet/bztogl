import re

# Regexes taken from:
# http://cpansearch.perl.org/src/MKANAT/Parse-StackTrace-0.08/lib/Parse/
# This could be expanded to handle Python backtraces using code from the same
# library

_SPECIAL_LINES = (
    re.compile(r"""
        ^\#\d+\s+                             # #1
        (?:
            (?:0x[A-Fa-f0-9]{4,}\s+in\b)      # 0xdeadbeef in
            |
            (?:[A-Za-z_\*]\S+\s+\()           # some_function_name
            |
            (?:<signal \s handler \s called>)
        )
        """, re.VERBOSE | re.MULTILINE),
    re.compile(r'^\(gdb\) '),
    re.compile(r'^Thread \d+ \(.*\):\s*$'),
    re.compile(r'^\[Switching to Thread .+ \(.+\)\]\s*$'),
    re.compile(r'^Program received signal SIG[A-Z]+,'),
    re.compile(r'^Breakpoint \d+, [A-Za-z_\*]\S+\s+\('),
    # lldb backtraces
    re.compile(r"""^\s*\*?\s+thread\s+\#\d+(?:,|:)          # * thread #10
    (?:\s+
        (?:
        name\s=\s\'.*\'                                     # name = 'thread1'
        |
        stop\sreason\s=\s(?:signal\sSIG[A-Z]{3,4}|EXC_.+)   # stop reason =
        |
        queue\s=\s\'.*\'                                    # queue = 'org.reverse.dns'
        |
        tid\s=\s0x[A-Fa-f0-9]{4,}                           # tid = 0xdeadbeef
        |
        0x[A-Fa-f0-9]{4,}                                   # 0xdeadbeef
        |
        .*\s\+\s\d+                                         # lib.dylib`some_function + 100
        ),?
    ){1,6}.*$""", re.VERBOSE),
    re.compile(r'^\s*\*?\s+frame\s\#\d+:\s+(?:0x[A-Fa-f0-9]{4,}\s+).*\s*$'),
    re.compile(r'^\d+.+(?:0x[A-Fa-f0-9]{4,})\s+.+\s\+\s\d+,?\s*$'),
    # lldb disassembly
    re.compile(r'^    (?:0x[A-Fa-f0-9]{4,})\s+\<\+\d+\>:\s+.+(\%|\$|0x).+$'), # 0x10e9058ab <+0>:
    # valgrind output
    re.compile(r'==\d+== (?:Thread \d+|Conditional|  at|  by).*$')
)

_IGNORE_LINES = (
    'No symbol table info available.',
    'No locals.',
    '---Type <return> to continue, or q <return> to quit---',
    ')'
)


def _is_special(line):
    if line in _IGNORE_LINES:
        return True
    return any(map(lambda r: r.search(line), _SPECIAL_LINES))


def _quote_stack_trace(lines):
    start = end = -1
    in_backtrace = False
    possible_end = False
    for ix, l in enumerate(lines):
        if not in_backtrace:
            if _is_special(l):
                start = ix
                in_backtrace = True
                possible_end = False
            continue

        if _is_special(l):
            possible_end = False
            continue

        if not l or l.isspace():
            possible_end = True
            continue

        # If this is a non-special, non-blank line following a number of
        # blank lines, then we're done
        if possible_end:
            end = ix - 1
            break
    else:
        if not in_backtrace:
            return lines
        end = ix + 1

    lines.insert(end, '```')
    lines.insert(start, '```')

    lines[end + 3:] = _quote_stack_trace(lines[end + 3:])
    return lines


def quote_stack_traces(text):
    """Looks for possible GDB backtraces in @text, and surrounds them with
    triple backticks in order to quote them for Markdown."""

    for line in _SPECIAL_LINES:
        multi_expr = re.compile (line.pattern, line.flags | re.MULTILINE)
        if multi_expr.search(text):
            return '\n'.join(_quote_stack_trace(re.split(r'\r?\n', text)))

    return text

